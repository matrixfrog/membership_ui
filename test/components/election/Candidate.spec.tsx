import React from 'react'
import { expect } from 'chai'
import renderer from 'react-test-renderer'

import Candidate from '../../../src/js/components/election/Candidate'

describe('<Candidate />', () => {
  it('renders a name', done => {
    const tree = renderer.create(<Candidate name="a spooky spectre" />).toJSON()
    expect(tree).to.matchSnapshot()
    done()
  })

  it('renders a name and image', done => {
    const tree = renderer
      .create(
        <Candidate
          name="Gritty"
          imageUrl="https://en.wikipedia.org/wiki/Gritty_(mascot)"
        />
      )
      .toJSON()
    expect(tree).to.matchSnapshot()
    done()
  })

  it('renders a name, image, and height', done => {
    const tree = renderer
      .create(
        <Candidate
          name="eight"
          imageUrl="https://en.wikipedia.org/wiki/8"
          height={8}
        />
      )
      .toJSON()
    expect(tree).to.matchSnapshot()
    done()
  })
})
