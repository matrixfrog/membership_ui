import React from 'react'
import { expect } from 'chai'
import renderer from 'react-test-renderer'

import PageHeading from '../../../src/js/components/common/PageHeading'

describe('<PageHeading />', () => {
  it('renders h1 by default', done => {
    const tree = renderer.create(<PageHeading>foo</PageHeading>).toJSON()
    expect(tree).to.matchSnapshot()
    done()
  })

  it('renders h1', done => {
    const tree = renderer
      .create(<PageHeading level={1}>bar</PageHeading>)
      .toJSON()
    expect(tree).to.matchSnapshot()
    done()
  })

  it('renders h6', done => {
    const tree = renderer
      .create(<PageHeading level={6}>baz</PageHeading>)
      .toJSON()
    expect(tree).to.matchSnapshot()
    done()
  })
})
