import React from 'react'
import { expect } from 'chai'
import renderer from 'react-test-renderer'

import SkipLink from '../../../src/js/components/common/SkipLink'

describe('<SkipLink />', () => {
  it('renders with a child', done => {
    const tree = renderer
      .create(
        <SkipLink>
          <span>hi</span>
        </SkipLink>
      )
      .toJSON()
    expect(tree).to.matchSnapshot()
    done()
  })
  it('renders with a targetId', done => {
    const tree = renderer
      .create(
        <SkipLink targetId="foo">
          <p>bye</p>
        </SkipLink>
      )
      .toJSON()
    expect(tree).to.matchSnapshot()
    done()
  })
  it('renders with an event handler', done => {
    const tree = renderer
      .create(
        <SkipLink onClick={() => alert('foo')}>
          <strong>bar</strong>
        </SkipLink>
      )
      .toJSON()
    expect(tree).to.matchSnapshot()
    done()
  })
  it('renders with a passthrough property', done => {
    const tree = renderer
      .create(
        <SkipLink
          onClick={() => alert('foo')}
          role="button"
          aria-pressed="false"
        >
          <strong>bar</strong>
        </SkipLink>
      )
      .toJSON()
    expect(tree).to.matchSnapshot()
    done()
  })
})
