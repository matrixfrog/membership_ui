import React from 'react'
import { expect } from 'chai'
import renderer from 'react-test-renderer'

import VoteEligiblePopup from '../../../src/js/components/meeting/VoteEligiblePopup'

describe('<VoteEligiblePopup />', () => {
  it('renders an eligible (numVotes > 0) invisible popup', done => {
    const tree = renderer
      .create(<VoteEligiblePopup numVotes={1} visible={false} />)
      .toJSON()
    expect(tree).to.matchSnapshot()
    done()
  })

  it('renders an eligible (numVotes > 0) visible popup', done => {
    const tree = renderer
      .create(<VoteEligiblePopup numVotes={1} visible={true} />)
      .toJSON()
    expect(tree).to.matchSnapshot()
    done()
  })

  it('renders an ineligible (numVotes == 0) invisible popup', done => {
    const tree = renderer
      .create(<VoteEligiblePopup numVotes={0} visible={false} />)
      .toJSON()
    expect(tree).to.matchSnapshot()
    done()
  })

  it('renders an ineligible (numVotes == 0) visible popup', done => {
    const tree = renderer
      .create(<VoteEligiblePopup numVotes={0} visible={true} />)
      .toJSON()
    expect(tree).to.matchSnapshot()
    done()
  })
})
