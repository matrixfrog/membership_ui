import { expect } from 'chai'
import { fromJS, Map, List } from 'immutable'
import {
  isCommitteeAdmin,
  getRelevantCommittees
} from '../../src/js/services/members'

function createMember(roles = []) {
  return fromJS({
    user: {
      data: {
        roles
      }
    }
  })
}

function createRole(name, committee) {
  const committee_name = committee == null ? 'general' : committee
  return {
    role: name,
    committee: committee_name,
    committee_name
  }
}

describe('members.isCommitteeAdmin', () => {
  it('returns true when given a general admin and no committee provided', () => {
    const member = createMember([createRole('admin')])
    const result = isCommitteeAdmin(member)
    expect(result).to.equal(true)
  })

  it('returns true when given an admin of any committee and no committee provided', () => {
    const member = createMember([createRole('admin', 'tech')])
    const result = isCommitteeAdmin(member)
    expect(result).to.equal(true)
  })

  it('returns true when given a general admin and committee is "any"', () => {
    const member = createMember([createRole('admin')])
    const result = isCommitteeAdmin(member, 'any')
    expect(result).to.equal(true)
  })

  it('returns true when given an admin of any committee and committee is "any"', () => {
    const member = createMember([createRole('admin', 'tech')])
    const result = isCommitteeAdmin(member, 'any')
    expect(result).to.equal(true)
  })

  it('returns false when given an admin of any committee and committee is null', () => {
    const member = createMember([createRole('admin', 'tech')])
    const result = isCommitteeAdmin(member, null)
    expect(result).to.equal(false)
  })

  it('returns false when given an admin of any committee and committee is an empty array', () => {
    const member = createMember([createRole('admin', 'tech')])
    const result = isCommitteeAdmin(member, [])
    expect(result).to.equal(false)
  })

  it('returns false when given an admin of any committee and committee is an empty string', () => {
    const member = createMember([createRole('admin', 'tech')])
    const result = isCommitteeAdmin(member, '')
    expect(result).to.equal(false)
  })

  it('returns true when given the committee for which they are an admin', () => {
    const member = createMember([createRole('admin', 'tech')])
    const result = isCommitteeAdmin(member, 'tech')
    expect(result).to.equal(true)
  })

  it('returns false when given a committee for which they are NOT an admin', () => {
    const member = createMember([createRole('admin', 'tech')])
    const result = isCommitteeAdmin(member, 'healthcare')
    expect(result).to.equal(false)
  })

  it('returns true when given one of the committees for which they are an admin', () => {
    const member = createMember([
      createRole('admin', 'tech'),
      createRole('admin', 'healthcare')
    ])
    const result = isCommitteeAdmin(member, 'healthcare')
    expect(result).to.equal(true)
  })

  it('returns true when given a list of committees that contains one for which they are an admin', () => {
    const member = createMember([createRole('admin', 'tech')])
    const result = isCommitteeAdmin(member, ['healthcare', 'tech'])
    expect(result).to.equal(true)
  })

  it('returns false when given a list of committees and a general admin who is not an admin of any of the given committees', () => {
    const member = createMember([createRole('admin')])
    const result = isCommitteeAdmin(member, ['healthcare', 'tech'])
    expect(result).to.equal(false)
  })
})

describe('members.getRelevantCommittees', () => {
  it('returns the list of committees that the user admins or is active in', () => {
    const member = createMember([
      createRole('admin', 'committee 1'),
      createRole('active', 'committee 2'),
      createRole('who cares', 'committee 3')
    ])
    const result = getRelevantCommittees(member)
    expect(result.toJS()).to.eql(
      List([
        Map({
          role: 'admin',
          committee: 'committee 1',
          committee_name: 'committee 1'
        }),
        Map({
          role: 'active',
          committee: 'committee 2',
          committee_name: 'committee 2'
        })
      ]).toJS()
    )
  })
})
