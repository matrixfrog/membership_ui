## Smoke testing areas

### General

- visit the index page at https://localhost:3000/
  - if auth0 is enabled:
    - the sign in page should redirect to the auth0 sign-in screen
    - the callback page at `/process` should redirect back to home properly after sign in
    - admin accounts should show an admin item in the navbar
    - regular accounts should not show an admin item
  - if auth0 is disabled
    - the sign-in page should not show
    - the user should land on the home page with the My <X> icons

### Meetings

- visit My meetings page
- click "see all meetings"
- click "Create meeting"
  - the committee list should show a list of available committees, defaulting to "General"

### Members

- visit memberships page
  - check for no errors in the console
- Try searching for yourself in the admin > member list
  - you should get a result containing your user

### Elections

- visit elections page

  - check for no errors in the console

- visit admin page to create an election
  - the elections create link should show up in the admin page
- click on the link to create an election
  - clicking on the start or end time should show a date and time picker
  - changing the date or time in the date picker should reflect in the appropriate field
  - the start or end time fields should match each other
  - the start or end time fields should show the date and time in a human friendly way
- click on "create election"
  - the create election page should redirect to the election list page
  - the election list page should show the newly created election
- click on the link for the newly created election
  - the election details page should not be blank
  - the page should have the following information:
    - title
    - description
    - election status
    - voter status
    - number of positions
    - list of candidates
    - management buttons
