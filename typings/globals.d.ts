// from https://github.com/piotrwitek/react-redux-typescript-guide/tree/master/playground/typings

declare interface Window {
  __REDUX_DEVTOOLS_EXTENSION__: any
  __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any
}

declare interface NodeModule {
  hot?: { accept: (path: string, callback: () => void) => void }
}

declare interface System {
  import<T = any>(module: string): Promise<T>
}
declare var System: System

declare module '*.svg' {
  const value: any
  export = value
}

declare module '*.png' {
  const value: any
  export = value
}
