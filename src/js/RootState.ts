interface RootState {
  committees: CommitteeMap
  elections: ElectionMap
  emails: EmailMap
  meetings: MeetingMap
  members: MemberMap
  me: MeState
}

interface CommitteeMap {
  [id: number]: CommitteeState
}

interface CommitteeState {
  id: number
  name: string
}

interface ElectionMap {
  [id: number]: ElectionState
}

interface ElectionState {
  id: number
  name: string
  candidates: Candidate[]
  number_winners: number
  status: string
  transitions: string[]
  voting_begins?: Date
  voting_ends?: Date
}

interface EmailMap {
  [id: number]: EmailForwardRule
}

interface EmailForwardRule {
  id: number
  email_address: string
  forwarding_addresses: string[]
}

interface Candidate {
  id: number
  name: string
  image_url?: string
}

interface MeetingMap {
  [id: number]: MeetingState
}

interface MeetingState {
  id: number
  name: string
  code?: string
  committee_id?: string
  start_time?: Date
  end_time?: Date
  landing_url?: string
}

interface MemberMap {
  [id: number]: Member
}

interface Member {
  id: number
  name: string
  email: string
  eligibility: EligibleState
}

interface EligibleState {
  is_eligible: boolean
  message: string
}

interface MeState {
  id: number
  committees: Ref[]
  info: UserInfo
  meetings: Ref[]
  roles: Role[]
  votes: EligibleVote[]
}

interface UserInfo {
  biography?: string
  email_address: string // rename to email?
  first_name: string // necessary?
  last_name: string // necessary?
  name: string
}

interface Role {
  committee_id?: string
  name: string
}

interface EligibleVote {
  election_id: number
  election_name: string
  election_status: string
  voted: boolean
}

/**
 * A reference to an object with display metadata
 */
interface Ref {
  id: number
  name: string
}
