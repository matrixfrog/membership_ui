import request, {
  SuperAgentRequest,
  ResponseError,
  SuperAgent
} from 'superagent'
import { USE_AUTH } from '../config'

import {
  HTTP_GET,
  HTTP_POST,
  HTTP_PUT,
  HTTP_PATCH,
  HTTP_DELETE,
  HttpMethod
} from '../util/util'
import { addAuthSessionRetry } from 'src/js/client/ApiClient'

/**
 *
 * @deprecated use or build a client API connector in /client
 */
export default async function api(
  method: HttpMethod,
  url: string,
  params: {} | null = null
) {
  let apiRequest: SuperAgentRequest

  switch (method) {
    case HTTP_GET:
      apiRequest = request.get(url)
      if (params !== null) {
        apiRequest.query(params)
      }
      break

    case HTTP_POST:
      apiRequest = request.post(url)
      if (params !== null) {
        apiRequest.send(params)
      }
      break

    case HTTP_PUT:
      apiRequest = request.put(url)
      if (params !== null) {
        apiRequest.send(params)
      }
      break

    case HTTP_PATCH:
      apiRequest = request.patch(url)
      if (params !== null) {
        apiRequest.send(params)
      }
      break

    case HTTP_DELETE:
      apiRequest = request.delete(url)
      if (params !== null) {
        apiRequest.send(params)
      }
      break

    default:
      console.error(`Unexpected method type to api call: ${method}`)
      throw new Error(`Unexpected method type ${method}`)
  }

  if (USE_AUTH) {
    apiRequest = addAuthSessionRetry(addAuthHeaders(apiRequest))
  }

  try {
    const result = await apiRequest
    return result.body
  } catch (err) {
    if ((err as ResponseError).status !== 401) {
      throw err
    } else {
      return null
    }
  }
}

// Add basic auth headers to API request based on redux store
function addAuthHeaders(
  apiRequest: request.SuperAgentRequest
): SuperAgentRequest {
  const token = localStorage.getItem('id_token')
  if (token) {
    return apiRequest.set('Authorization', `Bearer ${token}`)
  }
  return apiRequest
}
