import { ImElectionDetailsResponse } from '../client/ElectionClient'
import { ImMemberVote } from '../client/MemberClient'

export type EligibleVoteStatus =
  | 'ineligible'
  | 'already voted'
  | 'eligible'
  | 'not found'

export function eligibleVoteStatus(
  election: ImElectionDetailsResponse,
  vote: ImMemberVote
): EligibleVoteStatus {
  if (!vote) {
    return 'ineligible'
  }
  const electionId = election.get('id')
  if (
    electionId &&
    vote.get('election_id').toString() === electionId.toString()
  ) {
    if (Object.is(vote.get('voted'), true)) {
      return 'already voted'
    } else {
      return 'eligible'
    }
  } else {
    return 'ineligible'
  }
}

export function electionStatus(
  election: ImElectionDetailsResponse,
  now = new Date()
) {
  const electionState = election.get('status')
  if (electionState !== 'published') {
    return electionState
  }
  const beginning = new Date(election.get('voting_begins_epoch_millis'))
  const ending = new Date(election.get('voting_ends_epoch_millis'))

  if (beginning && beginning > now) {
    return 'polls not open'
  } else if (ending && ending < now) {
    return 'polls closed'
  }
  return 'polls open'
}
