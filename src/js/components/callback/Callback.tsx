import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from '../../redux/actions/callback'
import { clearUserSession } from '../../redux/actions/auth'
import { logError } from '../../util/util'

import PropTypes from 'prop-types'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { bindActionCreators, Dispatch } from 'redux'
import { RouteComponentProps } from 'react-router'

interface CallbackParamProps {}

interface CallbackRouteParamProps {}

type CallbackOtherProps = RouteComponentProps<
  CallbackParamProps,
  CallbackRouteParamProps
>
type CallbackProps = CallbackStateProps &
  CallbackDispatchProps &
  CallbackOtherProps

class Callback extends Component<CallbackProps> {
  constructor(props: CallbackProps) {
    super(props)
  }

  componentDidMount() {
    this.props.loading()
    this.props.clearUserSession()
    if (/access_token|id_token|error/.test(this.props.location.hash)) {
      this.props
        .handleAuthentication()
        .then(result => {
          // Check local storage for a nonce and redirect
          const storedState = localStorage.getItem(result.state)
          if (storedState) {
            try {
              const { redirectPath } = JSON.parse(storedState)
              localStorage.removeItem(result.state)
              return this.props.router.push(redirectPath)
            } catch (err) {
              // If json parse fails, just fall through.
              logError('Parsing error during post login redirect', err)
            }
          }
          return this.props.router.push('/home')
        })
        .catch(() => {
          this.props.router.push('/')
        })
    } else {
      this.props.router.push('/home')
    }
  }

  render() {
    return <div />
  }
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators({ ...actions, clearUserSession }, dispatch)

type CallbackStateProps = RootReducer
type CallbackDispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<
  CallbackStateProps,
  CallbackDispatchProps,
  CallbackOtherProps,
  RootReducer
>(
  state => state,
  mapDispatchToProps
)(Callback)
