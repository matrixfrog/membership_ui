import cx from 'classnames'
import React, { useMemo, useState } from 'react'
import {
  Button,
  Col,
  ListGroup,
  ListGroupItem,
  OverlayTrigger,
  Row,
  Tooltip
} from 'react-bootstrap'
import { FiPlus, FiTrash } from 'react-icons/fi'
import {
  ImMemberPhoneNumberList,
  MemberPhoneNumber
} from 'src/js/client/MemberClient'
import FieldGroup, { VALID_US_PHONE } from 'src/js/components/common/FieldGroup'
import { ProvisionalMemberPhoneNumber } from 'src/js/components/membership/EditMemberModal'

const BlankPhoneNumber: Partial<MemberPhoneNumber> = {
  phone_number: '',
  name: ''
}

interface AddlPhoneNumberFormProps {
  existingNumbers: ImMemberPhoneNumberList | undefined
  changes: ProvisionalMemberPhoneNumber[]
  onAddPhone(newAddress: Partial<MemberPhoneNumber>): void
  onDeletePhone(phone: string): void
  onUndoChange(phone: string): void
}

export const AdditionalPhoneNumberForm: React.FC<AddlPhoneNumberFormProps> = ({
  existingNumbers,
  changes,
  onAddPhone,
  onDeletePhone,
  onUndoChange
}) => {
  const [newPhone, setNewPhone] = useState<Partial<MemberPhoneNumber>>(
    BlankPhoneNumber
  )

  const handleChangePhone = <T extends keyof MemberPhoneNumber>(
    formKey: T,
    value: MemberPhoneNumber[T]
  ) => {
    setNewPhone({ ...newPhone, [formKey]: value })
  }

  const handleAddPhone: React.MouseEventHandler<HTMLButtonElement> = e => {
    onAddPhone(newPhone)
    setNewPhone(BlankPhoneNumber)
  }

  const adds: ProvisionalMemberPhoneNumber[] = useMemo(
    () => changes.filter(change => change.action === 'add'),
    [changes]
  )

  const deletes: ProvisionalMemberPhoneNumber[] = useMemo(
    () => changes.filter(change => change.action === 'delete'),
    [changes]
  )

  const existing: MemberPhoneNumber[] = useMemo(
    () => (existingNumbers?.toJS() as MemberPhoneNumber[]) ?? [],
    [existingNumbers]
  )

  const newPhoneValidationError: string | null = useMemo(() => {
    const phone = newPhone.phone_number
    const addSet = new Set(adds.map(change => change.phoneNumber.phone_number))
    const deleteSet = new Set(
      deletes.map(change => change.phoneNumber.phone_number)
    )
    const existingSet = new Set(existing.map(addr => addr.phone_number))

    if (phone == null || phone.length === 0) {
      return "Phone can't be blank"
    }

    if (!VALID_US_PHONE.test(phone)) {
      return "Phone doesn't look valid"
    }

    if (deleteSet.has(phone)) {
      return "Can't add an phone marked for deletion. Undo the deletion instead"
    }

    if (addSet.has(phone) || existingSet.has(phone)) {
      return "Can't add the same phone more than once"
    }

    return null
  }, [newPhone, adds])

  const combinedAddresses: ProvisionalMemberPhoneNumber[] = useMemo(() => {
    const deleteSet = new Set(
      deletes.map(change => change.phoneNumber.phone_number)
    )
    const existingWithDeletes: ProvisionalMemberPhoneNumber[] = existing.map(
      number => ({
        phoneNumber: number,
        action: deleteSet.has(number.phone_number)
          ? ('delete' as const)
          : undefined
      })
    )
    return [...existingWithDeletes, ...adds]
  }, [existingNumbers, changes])

  return (
    <section className="phonenum-container">
      {combinedAddresses.length === 0 && (
        <div className="phonenum-empty">
          <p>No phone numbers</p>
        </div>
      )}
      {combinedAddresses.length > 0 && <header>Phone numbers</header>}
      <ListGroup className="phonenum-list">
        {combinedAddresses.map(({ phoneNumber: number, action }) => (
          <ListGroupItem
            key={number.phone_number}
            className={cx(
              'phonenum-entry',
              'modifylist-entry',
              action != null
                ? `phonenum-action-${action} modifylist-action-${action}`
                : null
            )}
          >
            <Row>
              <Col md={5}>
                <span className="phonenum-number">{number.phone_number}</span>
              </Col>
              <Col md={5}>
                <span className="phonenum-name">{number.name}</span>
              </Col>
              <Col md={2}>
                {action == null && (
                  <Button
                    onClick={() => onDeletePhone(number.phone_number!)}
                    disabled={!number.phone_number}
                    className="delete-phone-button"
                    block
                  >
                    <FiTrash />
                  </Button>
                )}
                {action != null && (
                  <Button
                    onClick={() => onUndoChange(number.phone_number!)}
                    block
                  >
                    Undo
                  </Button>
                )}
              </Col>
            </Row>
          </ListGroupItem>
        ))}
        <AdditionalPhoneNumberComposer
          value={newPhone}
          disableSave={newPhoneValidationError != null}
          validationError={newPhoneValidationError}
          onChange={handleChangePhone}
          onSave={handleAddPhone}
        />
      </ListGroup>
    </section>
  )
}

interface AddlPhoneNumberComposerProps {
  value: Partial<MemberPhoneNumber>
  disableSave: boolean
  validationError: string | null
  onChange<T extends keyof MemberPhoneNumber>(
    formKey: T,
    value: MemberPhoneNumber[T]
  ): void
  onSave: React.MouseEventHandler<HTMLButtonElement>
}

const AdditionalPhoneNumberComposer: React.FC<AddlPhoneNumberComposerProps> = ({
  value,
  disableSave,
  validationError,
  onChange,
  onSave
}) => {
  return (
    <ListGroupItem>
      <Row>
        <Col md={5}>
          <FieldGroup
            layout="row"
            formKey="phone_number"
            componentClass="input"
            label="Phone number"
            onFormValueChange={onChange}
            type="tel"
            placeholder="415-555-1234"
            value={value.phone_number}
          />
        </Col>
        <Col md={5}>
          <FieldGroup
            layout="row"
            formKey="name"
            componentClass="input"
            label="Name (optional)"
            onFormValueChange={onChange}
            type="text"
            placeholder="my super secure phone"
            value={value.name}
          />
        </Col>
        <OverlayTrigger
          overlay={
            value.phone_number != null &&
            value.phone_number.length > 0 &&
            validationError != null ? (
              <Tooltip id="new-phone-validation-error">
                {validationError}
              </Tooltip>
            ) : (
              <span />
            )
          }
          placement="bottom"
        >
          <Col
            md={2}
            className="add-phone-button-container add-entry-button-container"
          >
            <Button
              onClick={onSave}
              disabled={disableSave}
              className="add-phone-button"
              block
            >
              <FiPlus />
            </Button>
          </Col>
        </OverlayTrigger>
      </Row>
    </ListGroupItem>
  )
}
