import dateFormat from 'dateformat'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Container } from 'react-bootstrap'
import { eligibilityEmail } from '../../services/emails'
import { isMember, isMemberLoaded } from '../../services/members'
import Loading from '../common/Loading'
import PageHeading from '../common/PageHeading'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { c } from 'src/js/config'
import { Helmet } from 'react-helmet'

dateFormat.masks.dsaMembership = 'mmmm d, yyyy'

type MyMembershipStateProps = RootReducer
type MyMembershipProps = MyMembershipStateProps

class MyMembership extends Component<MyMembershipProps> {
  render() {
    if (!isMemberLoaded(this.props.member)) {
      return <Loading />
    }

    let body
    if (isMember(this.props.member)) {
      const memberData = this.props.member.getIn(['user', 'data'])

      const duesPaidUntil = memberData.getIn(['membership', 'dues_paid_until'])
      const expirationDate = duesPaidUntil ? new Date(duesPaidUntil) : null

      body = this.renderMember(expirationDate)
    } else {
      body = this.renderNonMember()
    }

    return (
      <Container>
        <Helmet>
          <title>My Membership</title>
        </Helmet>
        <PageHeading level={1}>My Membership</PageHeading>
        {body}
      </Container>
    )
  }

  renderNonMember() {
    return (
      <div>
        <h3>Am I a registered member?</h3>
        <p>
          <strong>No</strong>, you are currently not listed as a member of the
          {c('CHAPTER_NAME')} chapter.
        </p>
        <p>
          If you'd like to sign up, visit{' '}
          <a href={c('URL_CHAPTER_JOIN')}>our signup page</a> and forward your
          confirmation email to {eligibilityEmail()}.
        </p>
      </div>
    )
  }

  renderMember(expirationDate: Date | null) {
    let expirationMessage: JSX.Element
    if (expirationDate === null) {
      expirationMessage = (
        <div>
          <p>We don't have any information on when your membership expires.</p>
          <p>
            This most commonly happens for new members or people who live
            outside of {c('CHAPTER_AREA_NAME')}. If your address has changed,
            notify National DSA at{' '}
            <a href="https://act.dsausa.org/survey/mailingaddr">
              act.dsausa.org/survey/mailingaddr
            </a>
            , and they'll update us in the next few weeks.
          </p>
        </div>
      )
    } else if (
      expirationDate.getFullYear() < 2000 ||
      expirationDate.getFullYear() > new Date().getFullYear() + 50
    ) {
      expirationMessage = <p>You are a lifetime member of DSA.</p>
    } else if (expirationDate > new Date()) {
      expirationMessage = (
        <p>
          Your membership will expire on{' '}
          <strong>{dateFormat(expirationDate, 'dsaMembership')}</strong>. If you
          are paying monthly dues to National, your membership should auto-renew
          at that time.
        </p>
      )
    } else {
      expirationMessage = (
        <p>
          Your membership may be expiring soon. Make sure to{' '}
          <a href={c('URL_CHAPTER_JOIN')}>renew soon</a>. DSA now offers monthly
          dues that auto-renew.
        </p>
      )
    }

    return (
      <div>
        <h3>Am I a registered member?</h3>
        <p>
          <strong>Yes!</strong> You are a registered member of the{' '}
          {c('CHAPTER_NAME')} chapter.
        </p>
        <h3>When does my registration expire?</h3>
        {expirationMessage}
      </div>
    )
  }
}

export default connect<MyMembershipStateProps, null, null, RootReducer>(
  state => state
)(MyMembership)
