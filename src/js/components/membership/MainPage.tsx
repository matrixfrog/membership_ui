import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import { Col, Container, Row } from 'react-bootstrap'
import { isMemberLoaded } from '../../services/members'
import mymembership from '../../../../images/mymembership.png'
import myelections from '../../../../images/myelections.png'
import mycommittees from '../../../../images/mycommittees.png'
import mymeetings from '../../../../images/mymeetings.png'
import mycontactinfo from '../../../../images/mycontactinfo.png'
import myresources from '../../../../images/myresources.png'
import PageHeading from '../common/PageHeading'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { c, USE_ELECTIONS } from 'src/js/config'
import { capitalize } from 'lodash/fp'

type MainPageStateProps = RootReducer
type MainPageProps = MainPageStateProps

class MainPage extends Component<MainPageProps> {
  render() {
    let welcomeMessage = <PageHeading level={2}>Welcome!</PageHeading>
    if (isMemberLoaded(this.props.member)) {
      const memberInfo = this.props.member.getIn(['user', 'data', 'info'])
      const fullName =
        memberInfo.get('first_name') + ' ' + memberInfo.get('last_name')

      welcomeMessage = <PageHeading level={1}>Welcome, {fullName}!</PageHeading>
    }

    return (
      <Container>
        <Row>
          <Col>{welcomeMessage}</Col>
        </Row>
        <Row className="justify-content-md-center">
          <Col sm={6} md={4} lg={3}>
            {this.renderPanel('My Membership', '/my-membership', mymembership)}
          </Col>
          {USE_ELECTIONS && (
            <Col sm={6} md={4} lg={3}>
              {this.renderPanel('My Elections', '/my-elections', myelections)}
            </Col>
          )}
          <Col sm={6} md={4} lg={3}>
            {this.renderPanel(
              `My ${capitalize(c('GROUP_NAME_PLURAL'))}`,
              '/my-committees',
              mycommittees
            )}
          </Col>
          <Col sm={6} md={4} lg={3}>
            {this.renderPanel('My Meetings', '/my-meetings', mymeetings)}
          </Col>
          <Col sm={6} md={4} lg={3}>
            {this.renderPanel(
              'My Contact Info',
              '/my-contact-info',
              mycontactinfo
            )}
          </Col>
          <Col sm={6} md={4} lg={3}>
            {this.renderPanel('My Resources', '/my-resources', myresources)}
          </Col>
        </Row>
      </Container>
    )
  }

  renderPanel(title, path, imageUrl) {
    return (
      <Link to={path} className="wrapper-link">
        <div className="panel portal-panel">
          <div className="panel-heading text-center">
            <h3>{title}</h3>
          </div>
          <div className="panel-body">
            <img src={imageUrl} alt={title} />
          </div>
        </div>
      </Link>
    )
  }
}

export default connect<MainPageStateProps, null, null, RootReducer>(
  state => state
)(MainPage)
