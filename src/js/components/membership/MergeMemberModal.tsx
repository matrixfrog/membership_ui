import React, { useState } from 'react'
import { Button, Modal } from 'react-bootstrap'
import { Api } from 'src/js/client/ApiClient'
import {
  EligibleMemberListEntry,
  ImMemberDetailed,
  MemberClient
} from 'src/js/client/MemberClient'
import MemberSearchField from '../common/MemberSearchField'

interface MergeMemberModalProps {
  show: boolean
  member: ImMemberDetailed
  onHide(): void
  onSaveComplete(member: ImMemberDetailed): void
}

const MergeMemberModal: React.FC<MergeMemberModalProps> = ({
  show,
  member,
  onHide,
  onSaveComplete
}) => {
  const [
    memberToRemove,
    setMemberToRemove
  ] = useState<EligibleMemberListEntry | null>(null)

  const onMemberSelected = member => {
    setMemberToRemove(member)
  }

  const onMerge = async () => {
    const memberClient = new MemberClient(Api)
    if (memberToRemove !== null) {
      if (member.get('id') == memberToRemove.id) {
        return
      }
      const result = await memberClient.merge(
        member.get('id'),
        memberToRemove.id
      )
      if (result) {
        onSaveComplete(member)
        onHide()
      }
    }
  }

  return (
    <>
      <Modal show={show} onHide={onHide} className="info-modal" size="lg">
        <Modal.Header closeButton>
          <Modal.Title>Merge with another member record</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>
            Search for another member record to merge into this one. Data such
            as meeting attendance, votes, etc will be combined, and the searched
            for record will be deleted.
          </p>
          {memberToRemove !== null ? (
            <>
              {memberToRemove.id == member.get('id') ? (
                <p>
                  This is the same member record as the one you wish to merge
                  into. Please search again.
                </p>
              ) : (
                <p>
                  Please confirm the member record you wish to merge and delete:
                </p>
              )}
              <p className="pl-5">
                <b>{memberToRemove.name}</b>
                <br />
                {memberToRemove.email}
              </p>
              <p className="pl-5">
                <Button
                  onClick={() => setMemberToRemove(null)}
                  size="sm"
                  variant="outline-secondary"
                >
                  Search again?
                </Button>
              </p>
            </>
          ) : (
            <MemberSearchField onMemberSelected={onMemberSelected} />
          )}
        </Modal.Body>
        <Modal.Footer>
          <Button
            onClick={onMerge}
            disabled={
              memberToRemove === null ||
              (memberToRemove !== null &&
                memberToRemove.id === member.get('id'))
            }
          >
            Merge
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  )
}

export default MergeMemberModal
