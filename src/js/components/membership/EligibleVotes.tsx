import React, { Component } from 'react'
import { Link } from 'react-router'
import { List } from 'immutable'
import { electionStatus } from '../../services/elections'
import { ImMemberVoteList } from 'src/js/client/MemberClient'
import { ImElectionById } from 'src/js/redux/reducers/electionReducers'

interface EligibleVotesProps {
  votes: ImMemberVoteList
  elections: ImElectionById
}

export default class EligibleVotes extends Component<EligibleVotesProps> {
  render() {
    const votes = this.props.votes.sortBy(vote => -vote.get('election_id', 0))
    if (votes.size === 0 || !this.props.elections) {
      return <div />
    }

    const eligibleVotes = votes.flatMap((vote, index) => {
      const electionId = vote.get('election_id')
      if (!this.props.elections.has(electionId)) {
        return List()
      }
      const election = this.props.elections.get(electionId)
      return List([
        <div key={`vote-${index}`}>
          {this.renderElectionDescription(election)}
        </div>
      ])
    })

    return (
      <div>
        <h2>Eligible Votes</h2>
        {eligibleVotes}
      </div>
    )
  }

  renderElectionDescription(election) {
    return (
      <span>
        <Link to={`/elections/${election.get('id')}/`}>
          {election.get('name')}
        </Link>{' '}
        ({electionStatus(election)})
      </span>
    )
  }
}
