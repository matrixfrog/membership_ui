import React, { Component } from 'react'
import { connect } from 'react-redux'
import { USE_AUTH } from '../../config'
import Auth from '../../redux/actions/auth/universalAuth'
import { RouteComponentProps } from 'react-router'

export default function(ComposedComponent) {
  const auth = new Auth()
  class Authentication extends Component<RouteComponentProps<{}, {}>> {
    constructor(props) {
      super(props)
    }

    checkAuth = () => {
      if (USE_AUTH && (!auth.isAuthenticated() || localStorage.user == null)) {
        this.props.router.push({
          pathname: '/login',
          query: {
            redirect: window.location.pathname
          }
        })
      }
    }

    componentWillMount() {
      this.checkAuth()
    }

    componentWillUpdate(nextProps) {
      this.checkAuth()
    }

    render() {
      return <ComposedComponent {...this.props} />
    }
  }

  const mapStateToProps = state => state

  return connect(mapStateToProps)(Authentication)
}
