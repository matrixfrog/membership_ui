import React, { useState } from 'react'
import { Modal, Form } from 'react-bootstrap'
import CommitteeLeadership, {
  ImCommitteeLeader,
  ReplaceLeaderArgs
} from 'src/js/client/CommitteeLeadersClient'
import { ImCommitteeMember } from 'src/js/client/CommitteeClient'
import FieldGroup from '../common/FieldGroup'
import { List, Map } from 'immutable'
import { useMutation } from 'react-query'

interface ReplaceLeaderModalProps {
  committeeId: number
  roles: List<ImCommitteeLeader>
  members: List<ImCommitteeMember>
  onComplete: (ImCommitteeLeader) => void
  onCancel: () => void
}

const ReplaceLeaderModal: React.FC<ReplaceLeaderModalProps> = ({
  committeeId,
  roles,
  members,
  onComplete,
  onCancel
}) => {
  const { mutate: submitInner } = useMutation<
    ImCommitteeLeader,
    Error | null,
    ReplaceLeaderArgs
  >(args => CommitteeLeadership.replaceLeader(args))
  const roleOptions = Map<string, string>(
    roles.map(role => [
      `${role.get('id')}`,
      `${role.get('role_title')} (${role.get('member_name') ?? 'Unfilled'})`
    ])
  ).toJS() as { [key: string]: string }
  const [selectedRole, setSelectedRole] = useState('')
  const selectedRoleMemberId = selectedRole
    ? roles.find(role => `${role.get('id')}` === selectedRole)?.get('member_id')
    : null
  const memberOptions = Map<string, string>(
    members
      .filter(member => member.get('id') !== selectedRoleMemberId)
      .map(member => [`${member.get('id')}`, member.get('name')])
      .unshift(['null', '(Remove without replacement)']) as Iterable<
      [string, string]
    >
  ).toJS() as { [key: string]: string }
  const [selectedMember, setSelectedMember] = useState('')
  const submitEnabled = selectedRole && selectedMember
  const submit = async () => {
    const result = await submitInner({
      committeeId,
      leaderId: parseInt(selectedRole, 10),
      memberId: selectedMember === 'null' ? null : parseInt(selectedMember, 10)
    })
    onComplete(result)
  }

  return (
    <Modal.Body>
      <p>Replace which member holds a leadership&nbsp;role</p>
      <Form onSubmit={e => e.preventDefault()}>
        <FieldGroup
          formKey="leadership_role_id"
          label="Role"
          componentClass="select"
          options={roleOptions}
          placeholder="Select a role"
          value={selectedRole}
          onFormValueChange={(k, v) => setSelectedRole(v)}
          required
        />
        <FieldGroup
          formKey="member_id"
          label="Member"
          componentClass="select"
          options={memberOptions}
          placeholder="Select a member"
          value={selectedMember}
          onFormValueChange={(k, v) => setSelectedMember(v)}
          required
        />
        <div>
          <button type="button" onClick={submit} disabled={!submitEnabled}>
            Submit
          </button>{' '}
          <button type="button" onClick={onCancel}>
            Cancel
          </button>
        </div>
      </Form>
    </Modal.Body>
  )
}

export default ReplaceLeaderModal
