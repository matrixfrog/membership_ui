import React, { Component } from 'react'
import { List, Map, fromJS } from 'immutable'
import { connect } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'
import {
  fetchCommittee,
  markMemberInactive,
  addAdmin,
  removeAdmin,
  requestCommitteeMembership
} from '../../redux/actions/committeeActions'
import { Members, EligibleMemberListEntry } from '../../client/MemberClient'
import MemberSearchField from '../common/MemberSearchField'
import FieldGroup from '../common/FieldGroup'
import { isAdmin, isCommitteeAdmin } from '../../services/members'
import PageHeading from '../common/PageHeading'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { RouteComponentProps } from 'react-router'
import { FiMinusCircle } from 'react-icons/fi'
import {
  Committees,
  CommitteesById,
  ImCommitteeDetailed
} from 'src/js/client/CommitteeClient'
import { c } from 'src/js/config'
import Loading from 'src/js/components/common/Loading'
import { Container, Modal, Table } from 'react-bootstrap'
import { Helmet } from 'react-helmet'
import CommitteeLeadership, {
  ImCommitteeLeader
} from 'src/js/client/CommitteeLeadersClient'
import AddLeadershipRoleModal from './AddLeadershipRoleModal'
import ReplaceLeaderModal from './ReplaceLeaderModal'

interface CommitteeParamProps {
  committeeId: string
}
interface CommitteeState {
  inactive: boolean | null
  leaders: List<ImCommitteeLeader> | null
  showAddRoleModal: boolean
  showReplaceLeaderModal: boolean
}
interface CommitteeRouteParamProps {}
type CommitteeOtherProps = RouteComponentProps<
  CommitteeParamProps,
  CommitteeRouteParamProps
>
type CommitteeProps = CommitteeStateProps &
  CommitteeDispatchProps &
  CommitteeOtherProps

class Committee extends Component<CommitteeProps, CommitteeState> {
  constructor(props) {
    super(props)
    this.state = {
      inactive: null,
      leaders: null,
      showAddRoleModal: false,
      showReplaceLeaderModal: false
    }
  }

  static getDerivedStateFromProps(
    nextProps: CommitteeProps,
    nextState: CommitteeState
  ) {
    if (nextState.inactive === null) {
      const committee:
        | ImCommitteeDetailed
        | undefined = nextProps.committees.getIn([
        'byId',
        parseInt(nextProps.params.committeeId, 10)
      ])
      if (committee) {
        return {
          inactive: committee.get('inactive', false)
        }
      }
    }
    return nextState
  }

  componentDidMount() {
    this.props.fetchCommittee(this.getCommitteeId())
    this.fetchCommitteeLeaders(this.getCommitteeId())
  }

  async fetchCommitteeLeaders(committeeId: number) {
    const leaders = await CommitteeLeadership.list(committeeId)
    this.setState({ leaders })
  }

  getCommitteeId(): number {
    return parseInt(this.props.params.committeeId, 10)
  }

  addRoleComplete = (newRole: ImCommitteeLeader) => {
    this.setState(
      (state, props) => {
        if (!state?.leaders) {
          return null
        }
        return { leaders: state.leaders.push(newRole) }
      },
      () => this.setState({ showAddRoleModal: false })
    )
  }

  replaceLeaderComplete = (modifiedRole: ImCommitteeLeader) => {
    this.setState(
      (state, props) => {
        if (!state?.leaders) {
          return null
        }
        const replacedIndex = state.leaders.findIndex(
          leader => leader.get('id') === modifiedRole.get('id')
        )
        if (replacedIndex === -1) {
          return { leaders: state.leaders.push(modifiedRole) }
        }
        return {
          leaders: state.leaders.update(replacedIndex, val => modifiedRole)
        }
      },
      () => this.setState({ showReplaceLeaderModal: false })
    )
  }

  render() {
    // WARNING: THIS TYPE (ImCommitteeDetailed) IS SOMEWHAT INCORRECT
    // SEE CommitteeClient for more information (reducer hydration issues)
    const committee:
      | ImCommitteeDetailed
      | undefined = this.props.committees.getIn(['byId', this.getCommitteeId()])

    if (committee == null) {
      return <Loading />
    }

    const canEdit =
      isAdmin(this.props.member) ||
      isCommitteeAdmin(this.props.member, committee.get('name'))

    const leaders = this.state.leaders || List()

    const adminMembers = committee
      .get('admins', List())
      .sortBy(member => member.get('name'))
    const admins = adminMembers.map(member => (
      <div
        key={`committee-admin-${member.get('id')}`}
        className="committee-member"
      >
        {canEdit && (
          <>
            <button
              onClick={e =>
                this.props.removeAdmin(member.get('id'), committee.get('id'))
              }
              aria-label={`Remove admin role for ${c(
                'GROUP_NAME_SINGULAR'
              )} ${committee.get('name')} from member ${member.get('name')}`}
            >
              <FiMinusCircle />
            </button>{' '}
          </>
        )}
        {member.get('name')}
      </div>
    ))

    const viewerId = this.props.member.getIn(['user', 'data', 'id'])
    const activeMembers = committee.get('active_members', List())
    const viewerIsMember = activeMembers.find(
      member => member.get('id') === viewerId
    )
    const activeMemberList = activeMembers
      .sortBy(member => member.get('name'))
      .map(member => (
        <div
          key={`committee-active-${member.get('id')}`}
          className="committee-member"
        >
          {canEdit ? (
            <>
              <button
                onClick={e =>
                  this.props.markMemberInactive(
                    member.get('id'),
                    committee.get('id')
                  )
                }
                aria-label={`Mark member ${member.get(
                  'name'
                )} as in active in ${c('GROUP_NAME_SINGULAR')} ${committee.get(
                  'name'
                )} from member `}
              >
                <FiMinusCircle />
              </button>{' '}
            </>
          ) : null}
          {member.get('name')}
        </div>
      ))

    return (
      <Container>
        <Helmet>
          <title>{committee.get('name')}</title>
        </Helmet>
        <PageHeading level={1}>{committee.get('name')}</PageHeading>
        {committee.get('email') ? (
          <div style={{ marginBottom: '2rem' }}>
            Contact Leadership:{' '}
            <a href={`mailto:${committee.get('email')}`}>
              {committee.get('email')}
            </a>
          </div>
        ) : null}
        {/* TODO: "connect on Slack"; needs slack channel in db & api */}
        <div style={{ marginBottom: '2rem' }}>
          <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            <h3>Elected Leadership</h3>
            {canEdit ? (
              <div>
                <button
                  type="button"
                  onClick={() => this.setState({ showAddRoleModal: true })}
                >
                  Add Role
                </button>{' '}
                <button
                  type="button"
                  onClick={() =>
                    this.setState({ showReplaceLeaderModal: true })
                  }
                >
                  {/* TODO: allow nulling out */}
                  Replace Leader
                </button>
              </div>
            ) : null}
          </div>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>Name</th>
                <th>Role</th>
                <th>Term Start</th>
                <th>Term End</th>
              </tr>
            </thead>
            <tbody>
              {leaders.map(leader => (
                <tr key={leader.get('id')}>
                  <td
                    style={{
                      color: leader.get('member_name') ? undefined : 'red'
                    }}
                  >
                    {leader.get('member_name')
                      ? leader.get('member_name')
                      : 'Unfilled'}
                  </td>
                  <td>{leader.get('role_title')}</td>
                  <td>
                    {leader.get('term_start_date')
                      ? new Date(
                          leader.get('term_start_date')
                        ).toLocaleDateString()
                      : 'N/A'}
                  </td>
                  <td>
                    {leader.get('term_end_date')
                      ? new Date(
                          leader.get('term_end_date')
                        ).toLocaleDateString()
                      : 'N/A'}
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
          <Modal
            show={this.state.showAddRoleModal}
            centered
            size="lg"
            backdrop="static"
          >
            <AddLeadershipRoleModal
              committeeId={committee.get('id')}
              onComplete={this.addRoleComplete}
              onCancel={() => this.setState({ showAddRoleModal: false })}
            />
          </Modal>
          <Modal
            show={this.state.showReplaceLeaderModal}
            centered
            size="lg"
            backdrop="static"
          >
            <ReplaceLeaderModal
              committeeId={committee.get('id')}
              roles={leaders}
              members={committee.get('active_members', List())}
              onComplete={this.replaceLeaderComplete}
              onCancel={() => this.setState({ showReplaceLeaderModal: false })}
            />
          </Modal>
        </div>
        <div style={{ marginBottom: '2rem' }}>
          <h3>Portal Admins</h3>
          <p>
            These individuals were appointed by {c('GROUP_NAME_SINGULAR')}{' '}
            leaders to help manage the active member list, send{' '}
            {c('GROUP_NAME_SINGULAR')} emails, create meetings, and run
            elections.
          </p>
          {admins}
          {canEdit ? (
            <div style={{ marginTop: '1rem' }}>
              <MemberSearchField
                prompt="Add member:"
                onMemberSelected={(member: EligibleMemberListEntry) => {
                  this.props.addAdmin(member.id, committee.get('id'))
                }}
                id="member-search-add-admin"
              />
            </div>
          ) : null}
        </div>
        {/*
          TODO: email rules;
          reuse most of EmailRules page,
          update GET API to take committee_id filter
        */}
        <div style={{ marginBottom: '2rem' }}>
          <h3>Active Members</h3>
          <p>
            All dues-paying active members are eligible to vote in chapter-wide
            elections, regardless of their general meeting attendance record.
            Being active may also impact a member's ability to vote in{' '}
            {c('GROUP_NAME_SINGULAR')} elections. Each{' '}
            {c('GROUP_NAME_SINGULAR')} defines their own criteria for being
            "active", so be sure to reach out to your committee about the rules.
          </p>
          {canEdit ? null : (
            <p>
              If you've been active in this {c('GROUP_NAME_SINGULAR')} and
              aren't listed, send a request to {c('GROUP_NAME_SINGULAR')}{' '}
              leadership to have them confirm{' '}
              <button
                type="button"
                onClick={() =>
                  this.props.requestCommitteeMembership(committee.get('id'))
                }
                disabled={!committee?.get('id')}
              >
                Request confirmation
              </button>
            </p>
          )}
          {viewerIsMember || canEdit ? activeMemberList : null}
          {canEdit ? (
            <div style={{ marginTop: '2rem' }}>
              <MemberSearchField
                prompt="Add member:"
                onMemberSelected={this.onMemberSelected}
                id="member-search-add-member"
              />
            </div>
          ) : null}
        </div>
        {canEdit ? (
          <div style={{ marginBottom: '2rem' }}>
            <h3>Edit</h3>
            <FieldGroup
              componentClass="checkbox"
              formKey="inactive"
              label="Mark inactive?"
              value={this.state.inactive || false}
              onFormValueChange={(_, inactive) => {
                if (
                  inactive &&
                  !confirm(
                    'Are you sure you want to mark the committee as inactive?'
                  )
                ) {
                  return
                }
                this.editInactiveStatus(inactive)
              }}
            />
          </div>
        ) : null}
      </Container>
    )
  }

  onMemberSelected = async (member: EligibleMemberListEntry) => {
    const memberId = member.id
    const committeeId = this.getCommitteeId()

    const committee:
      | ImCommitteeDetailed
      | undefined = this.props.committees
      .get('byId', fromJS<CommitteesById>({}))
      .get(committeeId)

    if (committee != null) {
      const existingMember = committee
        .get('members', List())
        .find(member => member.get('id') === memberId)
      if (existingMember === null) {
        await Members.addRole(memberId, 'member', committeeId)
      }
    }
    await Members.addRole(memberId, 'active', committeeId)

    this.props.fetchCommittee(committeeId)
  }

  async editInactiveStatus(inactive: boolean) {
    try {
      const committeeId = this.getCommitteeId()
      await Committees.editInactiveStatus(committeeId, inactive)
      this.setState({ inactive })
    } catch (err) {
      console.error('Failed to edit committee inactive status')
    }
  }
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      fetchCommittee,
      markMemberInactive,
      addAdmin,
      removeAdmin,
      requestCommitteeMembership
    },
    dispatch
  )

type CommitteeStateProps = RootReducer
type CommitteeDispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<
  CommitteeStateProps,
  CommitteeDispatchProps,
  {},
  RootReducer
>(
  state => state,
  mapDispatchToProps
)(Committee)
