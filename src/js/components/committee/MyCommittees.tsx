import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Col, Container, Table } from 'react-bootstrap'
import { Link, RouteComponentProps } from 'react-router'
import { fromJS, Seq } from 'immutable'
import * as actions from '../../redux/actions/committeeActions'
import {
  isAdmin,
  isCommitteeAdmin,
  isMemberLoaded
} from '../../services/members'
import Loading from '../common/Loading'
import ConfirmCommittee from '../committee/ConfirmCommittee'
import PageHeading from '../common/PageHeading'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { bindActionCreators, Dispatch } from 'redux'
import { ImCommittee, CommitteesById } from 'src/js/client/CommitteeClient'
import { c } from 'src/js/config'
import { capitalize } from 'lodash/fp'
import { Helmet } from 'react-helmet'

interface MyCommitteesParamProps {}
interface MyCommitteesRouteParamProps {}

type MyCommitteesOtherProps = RouteComponentProps<
  MyCommitteesParamProps,
  MyCommitteesRouteParamProps
>
type MyCommitteesProps = MyCommitteesStateProps &
  MyCommitteesDispatchProps &
  MyCommitteesOtherProps

class MyCommittees extends Component<MyCommitteesProps> {
  componentDidMount() {
    this.props.fetchCommittees()
  }

  render() {
    if (!isMemberLoaded(this.props.member)) {
      return <Loading />
    }

    const adminView =
      isAdmin(this.props.member) || isCommitteeAdmin(this.props.member)

    const pageTitle = `My ${capitalize(c('GROUP_NAME_PLURAL'))}`

    return (
      <Container>
        <Helmet>
          <title>{pageTitle}</title>
        </Helmet>
        <PageHeading level={1}>{pageTitle}</PageHeading>
        {this.renderActiveCommittees()}
        <h3>Other committees</h3>
        {this.renderAllCommittees()}
      </Container>
    )
  }

  getCommittees(isActive: boolean): Seq.Indexed<ImCommittee> {
    const activeCommitteeIds = this.props.member
      .getIn(['user', 'data', 'roles'])
      .filter(
        role =>
          role.get('role') === 'active' &&
          role.get('committee_name') !== 'general'
      )
      .map(role => role.get('committee_id'))
    return this.props.committees
      .get('byId', fromJS<CommitteesById>({}))
      .valueSeq()
      .filter(
        (committee: ImCommittee) =>
          (isActive && activeCommitteeIds.contains(committee.get('id'))) ||
          (!isActive && !activeCommitteeIds.contains(committee.get('id')))
      )
      .sortBy((committee: ImCommittee) => committee.get('name')) as Seq.Indexed<
      ImCommittee
    >
  }

  renderActiveCommittees(): JSX.Element {
    const committees = this.getCommittees(true)
    const currentlyActive =
      committees.size === 0 ? (
        <p>You're currently not active in any {c('GROUP_NAME_PLURAL')}.</p>
      ) : (
        this.renderCommittees(committees)
      )

    return (
      <div style={{ marginBottom: '3rem' }}>
        {currentlyActive}
        <p>
          If you've been active in a {c('GROUP_NAME_SINGULAR')} but don't see it
          listed, send a request to the co-chairs to have them confirm:
        </p>
        <Container>
          <Col xs={12} sm={8} md={6} lg={4}>
            <ConfirmCommittee />
          </Col>
        </Container>
      </div>
    )
  }

  renderAllCommittees(): JSX.Element {
    const committees = this.getCommittees(false)
    return (
      <div>
        <p>
          Our chapter has numerous {c('GROUP_NAME_PLURAL')} focused on various
          types of work, both internal and external. To find out more about what
          they do,{' '}
          <a href={c('URL_CHAPTER_GROUP_INFO')}>click here for more info</a>.
        </p>
        <p>
          If you'd like to get in touch with the leadership for a{' '}
          {c('GROUP_NAME_SINGULAR')}, use the list below:
        </p>
        {this.renderCommittees(committees)}
      </div>
    )
  }

  renderCommittees(committees: Seq.Indexed<ImCommittee>) {
    return (
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>{capitalize(c('GROUP_NAME_SINGULAR'))}</th>
            <th>Leadership Email</th>
          </tr>
        </thead>
        <tbody>
          {committees.map(committee => (
            <tr key={committee.get('id')}>
              <td>
                <Link to={`/committees/${committee.get('id')}`}>
                  {committee.get('name')}
                </Link>
              </td>
              <td>
                <a href={`mailto:${committee.get('email')}`}>
                  {committee.get('email')}
                </a>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    )
  }
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(actions, dispatch)

type MyCommitteesStateProps = RootReducer
type MyCommitteesDispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<
  MyCommitteesStateProps,
  MyCommitteesDispatchProps,
  {},
  RootReducer
>(
  state => state,
  mapDispatchToProps
)(MyCommittees)
