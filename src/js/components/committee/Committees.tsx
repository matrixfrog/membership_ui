import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Col, Form, Container, Row } from 'react-bootstrap'
import { Link, RouteComponentProps } from 'react-router'
import * as actions from '../../redux/actions/committeeActions'
import FieldGroup from '../common/FieldGroup'
import { Map, fromJS } from 'immutable'
import { isAdmin } from '../../services/members'
import PageHeading from '../common/PageHeading'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { bindActionCreators, Dispatch } from 'redux'
import { ImCommittee } from 'src/js/redux/reducers/committeeReducers'
import { CommitteesById } from 'src/js/client/CommitteeClient'
import { c } from 'src/js/config'
import { capitalize } from 'lodash/fp'
import { Helmet } from 'react-helmet'

interface CreateCommitteeForm {
  name: string

  /**
   * Admin emails (comma-separated)
   */
  admin_list: string
  provisional: boolean
}

interface CommitteesState {
  committee: CreateCommitteeForm
}

interface CommitteesParamProps {}
interface CommitteesRouteParamProps {}

type CommitteesOtherProps = RouteComponentProps<
  CommitteesParamProps,
  CommitteesRouteParamProps
>
type CommitteesProps = CommitteesStateProps &
  CommitteesDispatchProps &
  CommitteesOtherProps

class Committees extends Component<CommitteesProps, CommitteesState> {
  constructor(props) {
    super(props)
    this.state = {
      committee: { name: '', admin_list: '', provisional: false }
    }
  }

  componentDidMount() {
    this.props.fetchCommittees()
  }

  updateCreateCommitteeForm = <K extends keyof CreateCommitteeForm>(
    formKey: K,
    value: CreateCommitteeForm[K]
  ) => {
    if (
      this.props.committees.getIn(['form', 'create', 'inSubmission'], false)
    ) {
      return
    }
    this.setState({ committee: { ...this.state.committee, [formKey]: value } })
  }

  renderCommittee(committee: ImCommittee) {
    const id = committee.get('id')
    const name = committee.get('name')
    const inactive = committee.get('inactive')

    return (
      <div key={`committee-${id}`}>
        <Link to={`/committees/${id}`}>{name}</Link>
        {inactive ? ' (Inactive)' : null}
      </div>
    )
  }

  createCommitteeFromState(committee: CreateCommitteeForm) {
    const admin_list = (committee.admin_list || '').split(',').filter(v => v)
    this.props.createCommittee({
      name: committee.name,
      admin_list,
      provisional: committee.provisional
    })
  }

  render() {
    if (!isAdmin(this.props.member)) {
      return null
    }

    const committees = this.props.committees
      .get('byId', fromJS<CommitteesById>({}))
      .valueSeq()
      .sortBy((committee: ImCommittee) => committee.get('name'))
      .map((committee: ImCommittee) => this.renderCommittee(committee))
    return (
      <Container>
        <Helmet>
          <title>{c('GROUP_NAME_PLURAL')}</title>
        </Helmet>
        <Row>
          <Col sm={4}>
            <PageHeading level={1}>
              {capitalize(c('GROUP_NAME_PLURAL'))}
            </PageHeading>
            <h2>Add {c('GROUP_NAME_SINGULAR')}</h2>
            <Form onSubmit={e => e.preventDefault()}>
              <FieldGroup
                formKey="name"
                componentClass="input"
                type="text"
                label={`${capitalize(c('GROUP_NAME_SINGULAR'))} Name`}
                value={this.state.committee.name}
                onFormValueChange={this.updateCreateCommitteeForm}
                required
              />
              <FieldGroup
                formKey="admin_list"
                componentClass="input"
                type="text"
                label="Admin Emails (comma-separated)"
                value={this.state.committee.admin_list}
                onFormValueChange={this.updateCreateCommitteeForm}
                required
              />
              <FieldGroup
                formKey="provisional"
                componentClass="checkbox"
                label="Provisional?"
                value={this.state.committee.provisional}
                onFormValueChange={this.updateCreateCommitteeForm}
              />
              <button
                type="submit"
                onClick={e => {
                  e.preventDefault()
                  this.createCommitteeFromState(this.state.committee)
                }}
              >
                Add {c('GROUP_NAME_SINGULAR')}
              </button>
            </Form>
          </Col>
        </Row>
        <Row>
          <Col>
            <h2>{capitalize(c('GROUP_NAME_PLURAL'))}</h2>
            {committees}
          </Col>
        </Row>
      </Container>
    )
  }
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(actions, dispatch)

type CommitteesStateProps = RootReducer
type CommitteesDispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<
  CommitteesStateProps,
  CommitteesDispatchProps,
  {},
  RootReducer
>(
  state => state,
  mapDispatchToProps
)(Committees)
