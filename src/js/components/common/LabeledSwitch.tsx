import React from 'react'
import { ToggleButtonGroup, ToggleButton } from 'react-bootstrap'

interface LabeledSwitchProps {
  value: boolean
  trueLabel: string
  falseLabel: string
  onChange(value: boolean): void
  variant?:
    | 'primary'
    | 'secondary'
    | 'success'
    | 'danger'
    | 'warning'
    | 'info'
    | 'dark'
    | 'light'
}

const LabeledSwitch: React.FC<LabeledSwitchProps> = ({
  value,
  trueLabel,
  falseLabel,
  onChange,
  variant = 'primary'
}) => (
  <ToggleButtonGroup
    type="radio"
    name="do-not-email"
    value={value}
    onChange={onChange}
  >
    <ToggleButton
      value={true}
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      variant={value ? variant : (`outline-${variant}` as any)}
    >
      {trueLabel}
    </ToggleButton>
    <ToggleButton
      value={false}
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      variant={value ? (`outline-${variant}` as any) : variant}
    >
      {falseLabel}
    </ToggleButton>
  </ToggleButtonGroup>
)

export default LabeledSwitch
