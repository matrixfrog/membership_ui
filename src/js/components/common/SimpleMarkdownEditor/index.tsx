import React, { useState, ReactNode } from 'react'
import MarkdownIt from 'markdown-it'
import MdEditor from 'react-markdown-editor-lite'
import 'react-markdown-editor-lite/lib/index.css'

const mdParser = new MarkdownIt({ typographer: true, linkify: true })

interface SimpleMarkdownEditorOwnProps {
  initialValue?: string
  renderFooter?: () => ReactNode
  onChange?: (event?: React.ChangeEvent<HTMLTextAreaElement>) => void
  onClickSave?: (value: string) => void
  buttonDisabled?: boolean
}
const SimpleMarkdownEditor = (props: SimpleMarkdownEditorOwnProps) => {
  const [value, setValue] = useState(props.initialValue || '')

  return (
    <div>
      <MdEditor
        value={props.initialValue}
        plugins={['header', 'fonts', 'link', 'mode-toggle', 'full-screen']}
        style={{ minHeight: '320px' }}
        renderHTML={text => mdParser.render(text)}
        onChange={({ text }, event) => {
          if (props.onChange) {
            props.onChange(event)
          }
          setValue(text)
        }}
      />
      {props.renderFooter ? (
        props.renderFooter()
      ) : (
        <div style={{ margin: '1rem 0' }}>
          <button
            onClick={() =>
              props.onClickSave ? props.onClickSave(value) : null
            }
            disabled={props.buttonDisabled}
          >
            Save
          </button>
        </div>
      )}
    </div>
  )
}

export default SimpleMarkdownEditor
