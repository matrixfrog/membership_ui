import React from 'react'
import { Helmet } from 'react-helmet'
import { Container } from 'react-bootstrap'
import PageHeading from 'src/js/components/common/PageHeading'

const NotFound: React.FC = () => (
  <Container>
    <Helmet>
      <title>Page not found</title>
    </Helmet>
    <PageHeading level={1}>Page Not Found</PageHeading>
  </Container>
)

export default NotFound
