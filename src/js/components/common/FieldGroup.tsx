import React, { Component, RefObject } from 'react'
import { Col, Form, Row, OverlayTrigger, Popover } from 'react-bootstrap'
import DateTime from 'react-datetime'
import { compact, isArray, map } from 'lodash'
import moment, { Moment } from 'moment-timezone'
import { FiHelpCircle } from 'react-icons/fi'

/**
 * Need this line to make sure we don't tree-shake out the moment-timezone
 * import. Otherwise, a FieldGroup test will fail since it won't load the
 * timezone package that overrides the `moment` prototype.
 */
const KEEP_ME_TO_DEFEAT_TREESHAKING = moment.tz('America/Los_Angeles')

type ComponentClass = 'select' | 'labels' | 'checkbox' | 'datetime'

interface FieldGroupCommon<FormKey extends string> {
  formKey: FormKey
  label?: string
  id?: string
  disabled?: boolean
  required?: boolean
  componentClass: string
  autoFocus?: boolean
  title?: string
  helptext?: string | JSX.Element
  layout?: 'column' | 'row'
}

type FormOptions =
  | (string | number)[]
  | { [key: string]: string | number | null }

interface FieldGroupSelectProps<FormKey> {
  componentClass: 'select'
  onFormValueChange: (formKey: FormKey, value: string) => void
  value?: string | number | null
  options: FormOptions
  placeholder?: string
}

interface FieldGroupCheckboxProps<FormKey> {
  componentClass: 'checkbox'
  onFormValueChange: (formKey: FormKey, value: boolean) => void
  value: boolean
}

interface FieldGroupDatetimeProps<FormKey> {
  componentClass: 'datetime'
  onFormValueChange: (formKey: FormKey, value: Date) => void
  value: Date | null
  timezone?: string
}

interface FieldGroupInputTextProps<FormKey> {
  componentClass: 'input'
  type: 'text' | 'email' | 'tel'
  onFormValueChange: (formKey: FormKey, value: string) => void
  value?: string
  placeholder?: string
  pattern?: string
  rows?: number
  maxLength?: number
}

interface FieldGroupInputNumberProps<FormKey> {
  componentClass: 'input'
  type: 'number'
  onFormValueChange: (formKey: FormKey, value: number) => void
  step?: number
  min?: number
  max?: number
  value: number
}

type FieldGroupInputProps<FormKey> =
  | FieldGroupInputTextProps<FormKey>
  | FieldGroupInputNumberProps<FormKey>

type FieldGroupProps<FormKey extends string> = FieldGroupCommon<FormKey> &
  (
    | FieldGroupSelectProps<FormKey>
    | FieldGroupCheckboxProps<FormKey>
    | FieldGroupDatetimeProps<FormKey>
    | FieldGroupInputProps<FormKey>
  )

interface FieldGroupState {}

export const VALID_EMAIL = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/
export const VALID_US_PHONE = /^\D*\d{3}\D*\d{3}\D*\d{4}\D*$/

export default class FieldGroup<FormKey extends string> extends Component<
  FieldGroupProps<FormKey>,
  FieldGroupState
> {
  render() {
    const childComponent = this.renderChildComponent()
    const { formKey, label, helptext, layout } = this.props
    // TYPEHACK: (derrickliu) react-bootstrap actually defines two different Form.Label
    // objects with two different boolean literals: one with column: true and one with column?: false
    // TypeScript can't match a regular boolean type to both literals so it fails here.
    // The `any` type here is a hack to work around this issue.
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const useColumnLayout = (layout != null ? layout === 'column' : true) as any

    const labelComponent = (
      <Form.Label column={useColumnLayout} htmlFor={formKey}>
        {label}
        {helptext != null && (
          <OverlayTrigger
            placement="right"
            overlay={
              <Popover id={`fieldgroup-${label}-helptext`}>
                <Popover.Title>{label}</Popover.Title>
                <Popover.Content>{helptext}</Popover.Content>
              </Popover>
            }
          >
            <span className="help-bubble">
              {' '}
              <FiHelpCircle />
            </span>
          </OverlayTrigger>
        )}
      </Form.Label>
    )

    if (useColumnLayout) {
      return (
        <Form.Group as={Row}>
          {label && labelComponent}
          <Col sm={6}>{childComponent}</Col>
        </Form.Group>
      )
    } else {
      return (
        <Form.Group>
          {label && labelComponent}
          {childComponent}
        </Form.Group>
      )
    }
  }

  renderChildComponent(): JSX.Element {
    const {
      id,
      formKey,
      disabled,
      required,
      autoFocus
      // don't put `onFormValueChange` or `value` here; the following conditionals discriminate the type
    } = this.props
    switch (this.props.componentClass) {
      case 'checkbox': {
        const { value, onFormValueChange } = this.props

        return (
          <Form.Check
            id={formKey}
            onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
              onFormValueChange(formKey, e.currentTarget.checked)
            }
            checked={value}
            disabled={disabled}
            required={required}
          />
        )
      }
      case 'datetime': {
        // DateTime passes in the Date object, rather than a standard Event
        const { value, onFormValueChange, timezone } = this.props
        const now = moment().minute(0)
        return (
          <DateTime
            onChange={(date: Moment) => {
              // react-datetime will pass a string to onChange if the date
              // is invalid. We don't want to store these.
              if (typeof date !== 'string') {
                onFormValueChange(formKey, date.toDate())
              }
            }}
            value={value || now}
            displayTimeZone={timezone}
            inputProps={{
              disabled,
              id: id || formKey
            }}
            timeConstraints={{
              minutes: { min: 0, max: 59, step: 15 }
            }}
          />
        )
      }
      case 'select': {
        const { options, onFormValueChange, placeholder, value } = this.props
        const optionsChildren = this.populateOptionsChildren(
          options,
          formKey,
          placeholder
        )
        const coercedValue = this.coerceValueToString(value)
        return (
          <Form.Control
            id={formKey}
            as="select"
            onChange={e => onFormValueChange(formKey, e.currentTarget.value)}
            value={coercedValue} // If value is null, use the placeholder
            disabled={disabled}
            required={required}
          >
            {optionsChildren}
          </Form.Control>
        )
      }
      case 'input': {
        const { type } = this.props

        switch (type) {
          case 'text':
          case 'email': {
            const {
              value,
              onFormValueChange,
              maxLength,
              pattern,
              title,
              placeholder
            } = this.props as FieldGroupCommon<FormKey> &
              FieldGroupInputTextProps<FormKey>
            const patternOrDefault =
              pattern || (type === 'email' ? VALID_EMAIL.source : null)
            return (
              <Form.Control
                id={formKey}
                as="input"
                type={type}
                onChange={e =>
                  onFormValueChange(formKey, e.currentTarget.value)
                }
                placeholder={placeholder}
                value={value}
                pattern={patternOrDefault || undefined}
                maxLength={maxLength}
                disabled={disabled}
                required={required}
                autoFocus={autoFocus}
                title={title}
              />
            )
          }
          case 'tel': {
            const {
              value,
              onFormValueChange,
              maxLength,
              pattern,
              title,
              placeholder
            } = this.props as FieldGroupCommon<FormKey> &
              FieldGroupInputTextProps<FormKey>
            return (
              <Form.Control
                id={formKey}
                as="input"
                type={type}
                onChange={e =>
                  onFormValueChange(formKey, e.currentTarget.value)
                }
                value={value}
                pattern={pattern || VALID_US_PHONE.source}
                maxLength={maxLength}
                disabled={disabled}
                required={required}
                autoFocus={autoFocus}
                placeholder={placeholder}
                title={title}
              />
            )
          }
          case 'number': {
            const { value, onFormValueChange, max, min, step, title } = this
              .props as FieldGroupCommon<FormKey> &
              FieldGroupInputNumberProps<FormKey>
            return (
              <Form.Control
                id={formKey}
                as="input"
                type={type}
                onChange={e =>
                  onFormValueChange(formKey, parseInt(e.currentTarget.value))
                }
                value={value.toString()}
                max={max}
                min={min ?? 1}
                step={step}
                disabled={disabled}
                required={required}
                autoFocus={autoFocus}
                title={title}
              />
            )
          }
        }
      }
      default:
        return <span>Invalid FieldGroup use</span>
    }
  }

  populateOptionsChildren(
    options: FormOptions,
    formKey: FormKey,
    placeholder?: string
  ): JSX.Element[] {
    const isOptionArray = isArray(options)
    const optionsList = map(options, (value: string | number | null, index) => {
      const key = isOptionArray ? value ?? index : index
      return (
        <option key={key} value={key}>
          {value ?? ''}
        </option>
      )
    })

    return compact([
      placeholder && (
        <option key={formKey + 'placeholder'} value="" disabled>
          {placeholder}
        </option>
      ),
      ...optionsList
    ])
  }

  coerceValueToString = (value: string | number | undefined | null): string => {
    if (value != null) {
      if (typeof value === 'number') {
        return value.toString()
      } else {
        return value
      }
    } else {
      return ''
    }
  }
}
