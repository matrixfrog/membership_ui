import React from 'react'

interface SkipLinkProps
  extends React.DetailedHTMLProps<
      React.AnchorHTMLAttributes<HTMLAnchorElement>,
      HTMLAnchorElement
    > {
  targetId: string
  onClick?: React.MouseEventHandler<HTMLAnchorElement>
}

/**
 * Link to a section of the page. Visually hidden by default, but appears when
 * the user focuses it.
 *
 * @param {Object}                            params
 * @param {String}                            params.targetId
 * @param {JSX.Element|JSX.Element[]|String}  params.children
 * @param {Function}                          params.onClick
 * @returns {JSX.Element}
 */
const SkipLink: React.FC<SkipLinkProps> = ({
  targetId = '',
  children,
  onClick = null,
  ...props
}) => {
  function handleClick(event) {
    event.preventDefault()

    const selected = document.getElementById(targetId)

    if (selected != null) {
      selected.focus()
    }

    if ('history' in window) {
      history.pushState({}, document.title, `#${targetId}`)
    }

    if (onClick != null) {
      onClick(event)
    }
  }

  return (
    <a
      className="skip-link"
      href={`#${targetId}`}
      onClick={handleClick}
      {...props}
    >
      {children}
    </a>
  )
}

export default SkipLink
