import React, { Component } from 'react'
import pluralize from 'pluralize'

export class EmptyEligibilityRow extends Component {
  render() {
    return null
  }
}

interface AttendeeEligibilityRowProps {
  isPersonallyEligible: boolean
  message?: string
  numVotes?: number
}

interface AERStyles {
  color: string
}

export class AttendeeEligibilityRow extends Component<
  AttendeeEligibilityRowProps
> {
  setStyle(canVote) {
    const styles: Partial<AERStyles> = {}

    if (!canVote) {
      styles.color = 'red'
    }

    return styles
  }

  numVotesString(isPersonallyEligible, numVotes) {
    if (isPersonallyEligible) {
      if (numVotes > 1) {
        return `eligible, and also has been chosen as a proxy voter for this meeting by ${pluralize(
          'person',
          numVotes - 1,
          true
        )} (${numVotes} total ${pluralize('vote', numVotes)})`
      } else if (numVotes === 1) {
        return 'eligible, and only has one vote'
      } else {
        return 'eligible, but has given their vote to a proxy, so cannot vote at this meeting'
      }
    } else {
      if (numVotes > 0) {
        return `not eligible, but has been chosen as a proxy voter for this meeting by ${pluralize(
          'person',
          numVotes,
          true
        )}`
      } else {
        return 'not eligible, and is not a proxy'
      }
    }
  }

  render() {
    const { isPersonallyEligible, message, numVotes } = this.props

    const canVote = numVotes !== 0
    const eligibilityText =
      message || this.numVotesString(isPersonallyEligible, numVotes)

    return <span style={this.setStyle(canVote)}>{eligibilityText}</span>
  }
}
