import React from 'react'
import { Link } from 'react-router'
import { Col, Container, Row } from 'react-bootstrap'
import PageHeading from '../common/PageHeading'
import { c, USE_ELECTIONS, PAGE_TITLE_TEMPLATE } from 'src/js/config'
import { capitalize, compact } from 'lodash/fp'
import {
  FiUsers,
  FiUserPlus,
  FiList,
  FiPackage,
  FiCheckSquare,
  FiPlusCircle,
  FiCalendar,
  FiFilePlus,
  FiMail,
  FiShuffle
} from 'react-icons/fi'
import { IoIosBonfire } from 'react-icons/io'
import { Helmet } from 'react-helmet'

export interface NavLinkEntry {
  label: string
  link?: string
  children?: NavLinkEntry[]
  icon?: JSX.Element
}

export const AdminComponents: NavLinkEntry[] = compact([
  {
    label: 'Members',
    icon: <FiUsers />,
    children: [
      {
        link: '/members',
        label: 'Manage member roster',
        icon: <FiList />
      },
      {
        link: '/members/create',
        label: 'Add new member',
        icon: <FiUserPlus />
      },
      {
        link: '/import',
        label: 'Import from National DSA roster',
        icon: <FiPackage />
      }
    ]
  },
  USE_ELECTIONS && {
    label: 'Elections',
    icon: <FiCheckSquare />,
    children: [
      {
        link: '/elections',
        label: 'Manage elections',
        icon: <FiList />
      },
      {
        link: '/elections/create',
        label: 'Create new election',
        icon: <FiPlusCircle />
      }
    ]
  },
  {
    label: 'Meetings',
    icon: <FiCalendar />,
    children: [
      {
        link: '/meetings',
        label: 'Manage meetings',
        icon: <FiList />
      },
      {
        link: '/meetings/create',
        label: 'Create new meeting',
        icon: <FiFilePlus />
      }
    ]
  },
  {
    label: capitalize(c('GROUP_NAME_PLURAL')),
    icon: <IoIosBonfire />,
    children: [
      {
        link: '/committees',
        label: `Add or manage ${c('GROUP_NAME_PLURAL')}`,
        icon: <FiPlusCircle />
      }
    ]
  },
  {
    label: 'Emails',
    icon: <FiMail />,
    children: [
      {
        link: '/email/',
        label: 'Create or manage templates',
        icon: <FiFilePlus />
      },
      {
        link: '/email/rules',
        label: 'Create or edit forwarding rules',
        icon: <FiShuffle />
      }
    ]
  }
])

const Admin: React.FC = () => (
  <Container className="admin-screen">
    {/* Needs the title template too, because the Admin page doesn't get the nav shell */}
    <Helmet titleTemplate={`🛡️ ${PAGE_TITLE_TEMPLATE}`}>
      <title>Admin Panel</title>
    </Helmet>
    <Row>
      <Col>
        <PageHeading level={1}>🛡️ Admin Tools</PageHeading>
      </Col>
    </Row>
    <Row>
      {AdminComponents.map(parent => (
        <Col key={parent.label} md={6}>
          <header>
            <PageHeading level={2} className="admin-parent-heading">
              <span className="admin-icon">{parent.icon}</span>
              {parent.label}
            </PageHeading>
          </header>
          <ul className="admin-children">
            {parent.children?.map(child => (
              <li key={child.label} className="admin-child">
                {child.link != null ? (
                  <Link
                    to={child.link}
                    className="admin-child admin-child-link"
                  >
                    <span className="admin-icon">{child.icon}</span>
                    {child.label}
                  </Link>
                ) : (
                  <span className="admin-child">
                    <span className="admin-icon">{child.icon}</span>
                    {child.label}
                  </span>
                )}
              </li>
            ))}
          </ul>
        </Col>
      ))}
    </Row>
  </Container>
)

export default Admin
