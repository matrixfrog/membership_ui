import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'
import { membershipApi } from '../../services/membership'
import FieldGroup from '../common/FieldGroup'
import { HTTP_POST, logError } from '../../util/util'
import { Button, Col, Form, Row } from 'react-bootstrap'
import { Map, fromJS } from 'immutable'
import { TypedMap, FromJS } from '../../util/typedMap'
import { fetchElections } from '../../redux/actions/electionActions'
import { ImElectionsState } from '../../redux/reducers/electionReducers'
import { RootReducer } from '../../redux/reducers/rootReducer'
import { ImElectionResponse } from 'src/js/client/ElectionClient'

interface Voter {
  member_id: number
  election_id: number
}

interface AddEligibleVoterOwnProps {
  memberId: number
  refresh: Function
}

interface AddEligibleVoterState {
  inSubmission: boolean
  eligibleVoter: FromJS<Voter>
}

type AddEligibleVoterProps = AddEligibleVoterOwnProps &
  AddEligibleVoterDispatchProps &
  AddEligibleVoterStateProps

class AddEligibleVoter extends Component<
  AddEligibleVoterProps,
  AddEligibleVoterState
> {
  constructor(props) {
    super(props)
    this.state = {
      eligibleVoter: fromJS({
        member_id: this.props.memberId,
        election_id: -1
      }),
      inSubmission: false
    }
  }

  static getDerivedStateFromProps(
    nextProps: AddEligibleVoterProps,
    nextState: AddEligibleVoterState
  ) {
    if (
      nextState.eligibleVoter.get('election_id') === -1 &&
      nextProps.elections.get('byId').size > 0
    ) {
      const election = (nextProps.elections
        .get('byId')
        .first() as unknown) as ImElectionResponse
      return {
        ...nextState,
        eligibleVoter: nextState.eligibleVoter.set(
          'election_id',
          election.get('id')
        )
      }
    }
    return nextState
  }

  updateElectionId = <K extends keyof Voter>(formKey: K, value: Voter[K]) => {
    if (this.state.inSubmission) {
      return
    }

    this.setState({
      eligibleVoter: this.state.eligibleVoter.set(formKey, value)
    })
  }

  render() {
    const electionOptions = this.props.elections
      .get('byId')
      .map((election: ImElectionResponse) => election.get('name'))
    const disabled =
      electionOptions.size === 0 ||
      this.state.eligibleVoter.get('election_id') === -1

    return (
      <div>
        <Row>
          <Col sm={4}>
            <Form onSubmit={e => e.preventDefault()}>
              <h3>Add Eligible Voter</h3>
              <FieldGroup
                formKey="election_id"
                componentClass="select"
                label="Election"
                options={electionOptions.toObject()}
                placeholder="Select an election"
                value={this.state.eligibleVoter.get('election_id').toString()}
                onFormValueChange={(formKey, value) =>
                  this.updateElectionId(formKey, parseInt(value))
                }
                required
              />
              <Button
                type="submit"
                onClick={e =>
                  this.submitForm(e, 'eligibleVoter', '/election/voter')
                }
                disabled={disabled}
              >
                Add Voter
              </Button>
            </Form>
          </Col>
        </Row>
      </div>
    )
  }

  async submitForm(e, name, endpoint) {
    e.preventDefault()
    if (this.state.inSubmission) {
      return
    }
    this.setState({ inSubmission: true })
    try {
      await membershipApi(HTTP_POST, endpoint, this.state[name].toJS())
      this.props.refresh()
    } catch (err) {
      return logError('Error loading test', err)
    } finally {
      this.setState({ inSubmission: false })
    }
  }
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators({ fetchElections }, dispatch)

interface AddEligibleVoterStateProps {
  elections: ImElectionsState
}

type AddEligibleVoterDispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<
  AddEligibleVoterStateProps,
  AddEligibleVoterDispatchProps,
  AddEligibleVoterOwnProps,
  RootReducer
>(
  state => ({ elections: state.elections }),
  mapDispatchToProps
)(AddEligibleVoter)
