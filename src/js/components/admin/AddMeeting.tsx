import React, { Component } from 'react'
import FieldGroup from '../common/FieldGroup'
import { logError } from '../../util/util'
import { Form } from 'react-bootstrap'
import { fromJS, List, Map } from 'immutable'
import { MeetingClient, Meetings } from '../../client/MeetingClient'
import { FromJS } from '../../util/typedMap'
import PageHeading from 'src/js/components/common/PageHeading'
import { Api } from 'src/js/client/ApiClient'

interface AddMeetingProps {
  memberId: number
  refresh: () => void
}

interface MeetingAttendeeForm {
  member_id: number
  meeting_id: number
}

interface AddMeetingState {
  meetings: Map<number, string>
  attendee: FromJS<MeetingAttendeeForm>
  inSubmission: boolean
}

export default class AddMeeting extends Component<
  AddMeetingProps,
  AddMeetingState
> {
  constructor(props) {
    super(props)
    this.state = {
      meetings: Map(),
      attendee: fromJS({ member_id: this.props.memberId, meeting_id: 0 }),
      inSubmission: false
    }
  }

  componentDidMount() {
    // TODO(jesse): hide the form behind an edit button to save bandwith for admins
    this.getMeetings()
  }

  updateAttendee = (formKey, value) => {
    if (this.state.inSubmission) {
      return
    }

    this.setState({ attendee: this.state.attendee.set(formKey, value) })
  }

  render() {
    return (
      <section className="add-meeting-attendance-section">
        <PageHeading level={3}>Add meeting attendance</PageHeading>
        <Form onSubmit={e => e.preventDefault()}>
          <FieldGroup
            formKey="meeting_id"
            label="Meeting to attend"
            componentClass="select"
            options={this.state.meetings.toObject()}
            placeholder="Select a meeting"
            value={this.state.attendee.get('meeting_id')}
            onFormValueChange={this.updateAttendee}
            required
          />
          <button
            type="submit"
            onClick={e => this.submitForm(e, this.addAttendee)}
            disabled={this.state.attendee.get('meeting_id') === 0}
          >
            Attend this meeting
          </button>
        </Form>
      </section>
    )
  }

  async getMeetings(): Promise<void> {
    try {
      const results = await Meetings.all()
      const meetingNamesById: List<[number, string]> = results.map(m => [
        m.get('id'),
        m.get('name')
      ])
      this.setState({ meetings: Map<number, string>(meetingNamesById) })
    } catch (err) {
      logError('Error loading meetings', err)
    }
  }

  addAttendee: () => Promise<void> = async () => {
    const meetings = new MeetingClient(Api) // TODO: Replace with redux
    const attendee: MeetingAttendeeForm = this.state.attendee.toJS()
    await meetings.addAttendee(attendee.meeting_id, attendee.member_id)
  }

  async submitForm(
    e: React.MouseEvent<HTMLButtonElement>,
    action: () => Promise<unknown>
  ): Promise<void> {
    e.preventDefault()
    if (this.state.inSubmission) {
      return
    }
    this.setState({ inSubmission: true })
    try {
      await action()
      this.props.refresh()
    } catch (err) {
      logError('Error adding attendee', err)
    } finally {
      this.setState({ inSubmission: false })
    }
  }
}
