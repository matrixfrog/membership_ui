import React, { Component } from 'react'
import FieldGroup from '../common/FieldGroup'
import { isAdmin } from '../../services/members'
import { bindActionCreators, Dispatch } from 'redux'
import { connect } from 'react-redux'
import { Col, Form, Row, Container } from 'react-bootstrap'
import { createMember as createMemberAction } from '../../redux/actions/memberActions'
import PageHeading from '../common/PageHeading'
import { RootReducer } from '../../redux/reducers/rootReducer'
import { RouteComponentProps } from 'react-router'
import { CreateMemberForm } from 'src/js/client/MemberClient'
import { fromJS } from 'immutable'
import { Helmet } from 'react-helmet'

interface AddMemberOwnProps {}

interface AddMemberState {
  newMember: CreateMemberForm
  inSubmission: boolean
}

type AddMemberStateProps = RootReducer

type AddMemberDispatchProps = ReturnType<typeof mapDispatchToProps>

// Params vs RouteParams https://github.com/ReactTraining/react-router/blob/v3/docs/API.md#routeparams
interface AddMemberRouteParamProps {}
interface AddMemberParamProps extends AddMemberRouteParamProps {}

type AddMemberProps = AddMemberOwnProps &
  AddMemberStateProps &
  AddMemberDispatchProps &
  RouteComponentProps<AddMemberParamProps, AddMemberRouteParamProps>

class AddMember extends Component<AddMemberProps, AddMemberState> {
  constructor(props) {
    super(props)
    this.state = {
      newMember: {
        first_name: '',
        last_name: '',
        email_address: '',
        give_chapter_member_role: true
      },
      inSubmission: false
    }
  }

  componentWillUpdate() {
    const { member, router } = this.props
    if (!isAdmin(member)) {
      router.push('/home')
    }
  }

  updateNewMember = <K extends keyof CreateMemberForm>(
    formKey: K,
    value: CreateMemberForm[K]
  ) => {
    if (this.state.inSubmission) {
      return
    }
    this.setState({
      newMember: {
        ...this.state.newMember,
        [formKey]: value
      }
    })
  }

  createMember: React.MouseEventHandler<HTMLButtonElement> = e => {
    e.stopPropagation()
    this.props.createMember(fromJS(this.state.newMember)).then(() => {
      history.back()
    })
  }

  render() {
    if (!isAdmin(this.props.member)) {
      return <div />
    }
    return (
      <Container>
        <Helmet>
          <title>Add new member</title>
        </Helmet>
        <Row>
          <Col sm={8} lg={6}>
            <PageHeading level={2}>Add Member</PageHeading>
            <Form onSubmit={e => e.preventDefault()}>
              <FieldGroup
                formKey="first_name"
                componentClass="input"
                type="text"
                label="First Name"
                value={this.state.newMember.first_name}
                onFormValueChange={this.updateNewMember}
                required
              />
              <FieldGroup
                formKey="last_name"
                componentClass="input"
                type="text"
                label="Last Name"
                value={this.state.newMember.last_name}
                onFormValueChange={this.updateNewMember}
                required
              />
              <FieldGroup
                formKey="email_address"
                componentClass="input"
                type="text"
                label="Email"
                value={this.state.newMember.email_address}
                onFormValueChange={this.updateNewMember}
                required
              />
              <FieldGroup
                componentClass="checkbox"
                formKey="give_chapter_member_role"
                label="Is member of DSA National"
                value={this.state.newMember.give_chapter_member_role || false}
                onFormValueChange={this.updateNewMember}
              />
              <button type="submit" onClick={this.createMember}>
                Add Member
              </button>
            </Form>
          </Col>
        </Row>
      </Container>
    )
  }
}

function mapDispatchToProps(dispatch: Dispatch) {
  return bindActionCreators(
    {
      createMember: createMemberAction
    },
    dispatch
  )
}

export default connect<
  AddMemberStateProps,
  AddMemberDispatchProps,
  {},
  RootReducer
>(
  state => state,
  mapDispatchToProps
)(AddMember)
