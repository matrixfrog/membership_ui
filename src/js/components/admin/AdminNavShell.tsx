import React from 'react'
import { RouteComponentProps } from 'react-router'
import { Nav, Navbar, NavDropdown } from 'react-bootstrap'
import { AdminComponents } from 'src/js/components/admin/Admin'
import { LinkContainer } from 'react-router-bootstrap'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { ImMemberState } from 'src/js/redux/reducers/memberReducers'
import { connect, MapStateToProps } from 'react-redux'
import { isAdmin } from 'src/js/services/members'
import { Helmet } from 'react-helmet'
import { PAGE_TITLE_TEMPLATE } from 'src/js/config'

interface AdminNavShellOwnProps {}

interface AdminNavShellStateProps {
  member: ImMemberState
}

interface ANSParamProps {}

interface ANSRouteParamProps {}

type AdminNavShellProps = AdminNavShellOwnProps &
  AdminNavShellStateProps &
  RouteComponentProps<ANSParamProps, ANSRouteParamProps>

const AdminNavShell: React.FC<AdminNavShellProps> = ({ children, member }) => {
  if (isAdmin(member)) {
    return (
      <>
        <Helmet titleTemplate={`🛡️ ${PAGE_TITLE_TEMPLATE}`} />
        <section className="admin-nav-shell">
          <AdminNavBar />
          {children}
        </section>
      </>
    )
  }

  return <section className="deactivated-admin-nav-shell">{children}</section>
}

const AdminNavBar: React.FC = () => (
  <Navbar className="admin-navbar" variant="dark" expand="lg" bg="dark">
    <Navbar.Collapse>
      <Nav className="admin-nav mr-auto">
        {AdminComponents.map(parent => {
          if (parent.children != null && parent.children.length > 0) {
            const dropdown = (
              <NavDropdown
                title={
                  <span className="admin-nav-entry">
                    <span className="admin-icon">{parent.icon}</span>{' '}
                    <span className="admin-label">{parent.label}</span>
                  </span>
                }
                id={`admin-nav-dropdown-${parent.label}`}
              >
                {parent.children.map(child => (
                  <LinkContainer to={child.link ?? ''}>
                    <NavDropdown.Item>
                      <span className="admin-nav-dropdown-entry">
                        <span className="admin-icon">{child.icon}</span>
                        {child.label}
                      </span>
                    </NavDropdown.Item>
                  </LinkContainer>
                ))}
              </NavDropdown>
            )

            if (parent.link != null) {
              return (
                <LinkContainer key={parent.label} to={parent.link}>
                  {dropdown}
                </LinkContainer>
              )
            } else {
              return dropdown
            }
          } else {
            if (parent.link != null) {
              return (
                <LinkContainer key={parent.label} to={parent.link}>
                  <Nav.Link>
                    <span className="admin-icon">{parent.icon}</span>{' '}
                    {parent.label}
                  </Nav.Link>
                </LinkContainer>
              )
            } else {
              return (
                <Nav.Item key={parent.label}>
                  <span className="admin-icon">{parent.icon}</span>{' '}
                  {parent.label}
                </Nav.Item>
              )
            }
          }
        })}
      </Nav>
      <Nav>
        <Navbar.Brand key="admin-marker" className="admin-bar-brand">
          Admin Mode
        </Navbar.Brand>
      </Nav>
    </Navbar.Collapse>
  </Navbar>
)

const mapStateToProps: MapStateToProps<
  AdminNavShellStateProps,
  {},
  RootReducer
> = state => ({
  member: state.member
})

export default connect<AdminNavShellStateProps, {}, {}, RootReducer>(
  mapStateToProps
)(AdminNavShell)
