import React from 'react'
import { Row, Col } from 'react-bootstrap'
import { AttendeeEligibilityRow, EmptyEligibilityRow } from '../MemberRow'
import { Link } from 'react-router'

const SearchResultRow = ({ member }) => {
  return (
    <Row style={{ marginTop: '13px' }}>
      <Col sm={6} md={4}>
        <Link to={`/members/${member.get('id')}`}>
          {`${member.get('name')}: ${member.get('email')}`}
        </Link>
      </Col>
      <Col sm={6} md={8}>
        {member.hasIn(['eligibility', 'is_eligible'], false) ? (
          <AttendeeEligibilityRow
            isPersonallyEligible={member.getIn(['eligibility', 'is_eligible'])}
            message={member.getIn(['eligibility', 'message'])}
          />
        ) : (
          <EmptyEligibilityRow />
        )}
      </Col>
    </Row>
  )
}

export default SearchResultRow
