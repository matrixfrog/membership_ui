import React from 'react'
import { Row, Col } from 'react-bootstrap'
import SearchResultRow from './SearchResultRow'
import { TODO } from '../../../typeMigrationShims'
import { ImEligibleMemberList } from 'src/js/client/MemberClient'

interface SearchResultsProps {
  isLoading: boolean
  results: ImEligibleMemberList
  hasMoreResults: boolean
  loadMore(): void
  cursor: number
}

const SearchResults: React.FC<SearchResultsProps> = ({
  isLoading,
  results,
  hasMoreResults,
  loadMore
}) => (
  <div className="admin-search-list">
    {results.map(member => (
      <SearchResultRow key={member.get('id')} member={member} />
    ))}
    {isLoading && (
      <Row>
        <Col xs={12}>Loading...</Col>
      </Row>
    )}
    {hasMoreResults && !isLoading && (
      <Row>
        <Col xs={12}>
          <button className="admin-search-load-more" onClick={loadMore}>
            Load more
          </button>
        </Col>
      </Row>
    )}
  </div>
)

export default SearchResults
