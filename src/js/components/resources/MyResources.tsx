import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Row, Col, Container } from 'react-bootstrap'
import { isMemberLoaded } from '../../services/members'
import Loading from '../common/Loading'
import googlecalendar from '../../../../images/googlecalendar.png'
import googlecalendarprivate from '../../../../images/googlecalendarprivate.png'
import googlegroups from '../../../../images/googlegroups.png'
import discourse from '../../../../images/discourse.png'
import slack from '../../../../images/slack.png'
import youtube from '../../../../images/youtube.png'
import PageHeading from '../common/PageHeading'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { Helmet } from 'react-helmet'

type MyResourcesStateProps = RootReducer
type MyResourcesProps = MyResourcesStateProps

class MyResources extends Component<MyResourcesProps> {
  render() {
    if (!isMemberLoaded(this.props.member)) {
      return <Loading />
    }

    return (
      <Container>
        <Helmet>
          <title>My Resources</title>
        </Helmet>
        <PageHeading level={1}>My Resources</PageHeading>

        <h3>Where can I find DSA SF's policies?</h3>

        <Container>
          <Row>
            <Col xs={8} sm={4} lg={3}>
              <a href="https://dsasf.org/bylaws" className="wrapper-link">
                <div className="panel">
                  <div className="panel-heading" />
                  <div className="panel-body text-center">
                    <h4>Bylaws</h4>
                  </div>
                </div>
              </a>
            </Col>
            <Col xs={8} sm={4} lg={3}>
              <a
                href="https://dsasf.org/code_of_conduct/"
                className="wrapper-link"
              >
                <div className="panel">
                  <div className="panel-heading" />
                  <div className="panel-body text-center">
                    <h4>Code of Conduct</h4>
                  </div>
                </div>
              </a>
            </Col>
            <Col xs={8} sm={4} lg={3}>
              <a href="https://dsasf.org/handbook" className="wrapper-link">
                <div className="panel">
                  <div className="panel-heading" />
                  <div className="panel-body text-center">
                    <h4>Handbook</h4>
                  </div>
                </div>
              </a>
            </Col>
          </Row>
        </Container>

        <h3>What online tools are available to members?</h3>

        <Container>
          <Row>
            <Col xs={5} sm={3} md={2} className="text-center">
              {this.renderExternalResource(
                'Public Calendar',
                'https://dsasf.org/calendar/public',
                googlecalendar
              )}
            </Col>
            <Col
              xs={{ span: 5, offset: 2 }}
              sm={{ span: 3, offset: 0 }}
              md={2}
              className="text-center"
            >
              {this.renderExternalResource(
                'Private Calendar',
                'https://dsasf.org/calendar/private',
                googlecalendarprivate
              )}
            </Col>
            <Col xs={5} sm={3} md={2} className="text-center">
              {this.renderExternalResource(
                'Google Group',
                'https://groups.google.com/forum/#!forum/dsasf-members',
                googlegroups
              )}
            </Col>
            <Col
              xs={{ span: 5, offset: 2 }}
              sm={{ span: 3, offset: 0 }}
              md={2}
              className="text-center"
            >
              {this.renderExternalResource(
                'Forum',
                'https://forum.dsasf.org',
                discourse
              )}
            </Col>
            <Col
              xs={5}
              sm={{ span: 3, offset: 3 }}
              md={{ span: 2, offset: 0 }}
              className="text-center"
            >
              {this.renderExternalResource(
                'Slack',
                'https://dsasfteam.slack.com',
                slack
              )}
            </Col>
            <Col
              xs={{ span: 5, offset: 2 }}
              sm={{ span: 3, offset: 0 }}
              md={2}
              className="text-center"
            >
              {this.renderExternalResource(
                'YouTube',
                'https://www.youtube.com/channel/UCcnao-2NMyRbTTvUAYx0Y2A',
                youtube
              )}
            </Col>
          </Row>
        </Container>

        <h3>How do I get reimbursed?</h3>

        <p>
          Committees have a monthly budget set by the Treasurer on Steering
          Committee. If you spend money on behalf of the chapter or a committee,
          you can apply for a refund. Make sure to select "Unpaid" in the
          dropdown box for "status" and you should receive a refund within about
          3 - 5 days.
        </p>

        <ul>
          <li>
            <a href="https://dsasf.org/money-please">dsasf.org/money-please</a>
          </li>
        </ul>

        <h3>What services do the committees provide?</h3>

        <h4>Conflict Resolution</h4>

        <ul>
          <li>
            <a href="https://dsasf.org/request-conflict-resolution/">
              Request conflict resolution
            </a>
          </li>
        </ul>

        <h4>Grievance Officers</h4>

        <ul>
          <li>
            Email <a href="mailto:grievance@dsasf.org">grievance@dsasf.org</a>{' '}
            to file a grievance.
          </li>
        </ul>

        <h4>Creative</h4>

        <ul>
          <li>
            <a href="https://dsasf.org/submit-design">Request design work</a>
          </li>
        </ul>

        <h4>Education</h4>

        <ul>
          <li>
            <a href="http://dsasf.org/red-talk/">
              Propose a topic for a Red talk
            </a>
          </li>
          <li>
            Library at the DSA SF office (on the top floor of 350 Alabama St.
            #9)
            <ul>
              <li>
                <a href="https://docs.google.com/document/d/13hteZ0mIgetz9W6WnzyXRDayricnzGq2gVCNEk7pxco/edit?usp=sharing">
                  How to check in/check out books from the library
                </a>
              </li>
              <li>
                <a href="https://dsasf.org/library-log">Browse the catalog</a>
              </li>
            </ul>
          </li>
        </ul>

        <h4>Events</h4>

        <ul>
          <li>
            To add an event to the chapter calendars, fill out the form at{' '}
            <a href="https://dsasf.org/submit-event">dsasf.org/submit-event</a>.
          </li>
          <li>
            To schedule a post on the chapter's social media accounts, use the
            form at{' '}
            <a href="https://dsasf.org/social-media">dsasf.org/social-media</a>.
          </li>
        </ul>

        <h4>Tech</h4>

        <ul>
          <li>
            Email{' '}
            <a href="mailto:eligibility@dsasf.org">eligibility@dsasf.org</a> for
            election eligibility support, or to request access to the Google
            Group, Facebook Group, and other chapter communications channels.
            When requesting access to the Facebook Group, please include a link
            to your profile.
          </li>
          <li>
            Email <a href="mailto:wordpress@dsasf.org">wordpress@dsasf.org</a>{' '}
            for help with <a href="https://dsasf.org">dsasf.org</a>, our chapter
            website.
          </li>
          <li>
            Email{' '}
            <a href="mailto:tech-support@dsasf.org">tech-support@dsasf.org</a>{' '}
            for general tech help.
          </li>
          <li>
            Email{' '}
            <a href="mailto:livemeeting@dsasf.org">livemeeting@dsasf.org</a> to
            get access to video of last month&#8217;s general chapter meeting.
          </li>
        </ul>
      </Container>
    )
  }

  renderExternalResource(name: string, url: string, imageFilename: string) {
    return (
      <a href={url} className="wrapper-link">
        <img className="img-responsive" src={imageFilename} alt={name} />
        {name}
      </a>
    )
  }
}

export default connect<MyResourcesStateProps, null, null, RootReducer>(
  state => state
)(MyResources)
