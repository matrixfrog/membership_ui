import React, { Component } from 'react'
import { connect } from 'react-redux'
import { FormControl } from 'react-bootstrap'
import { Collection, List, Map, Range } from 'immutable'
import seedrandom from 'seedrandom'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { ImElectionDetailsResponse } from 'src/js/client/ElectionClient'
import { isFunction } from 'lodash/fp'
import { ImCandidate } from 'src/js/client/CandidateClient'

interface PaperBallotOwnProps {
  editable?: boolean
  election: ImElectionDetailsResponse
  ballotKey: string
  onRankingChange?(ranking: Map<number, number>): void
}
type PaperBallotStateProps = RootReducer
type PaperBallotProps = PaperBallotStateProps & PaperBallotOwnProps

interface PaperBallotState {
  editable: boolean
  ranking: Map<number, number>
  warnings: Map<number, string>
}

class PaperBallot extends Component<PaperBallotProps, PaperBallotState> {
  electionId: number
  shuffledCandidates: List<ImCandidate>

  constructor(props) {
    super(props)
    this.state = {
      editable: !!props.editable,
      ranking: Map(),
      warnings: Map()
    }
    this.electionId = this.props.election.get('id')
    const candidates = this.props.election.get('candidates')
    const seed = this.electionId + '_' + String(this.props.ballotKey)
    const rng = seedrandom(seed)
    this.shuffledCandidates = candidates.sortBy(rng)
  }

  static validateBallot(
    ranking: Collection.Keyed<number, number>
  ): Map<number, string> {
    // Collect all candidates with the same rank
    // 1. Group candidate ids by rank on the form
    const candidateIdsByRank = ranking // : Map[str, str]
      .groupBy(rank => rank) // : Map[str, Map[str, str]]
      .map(ranking => ranking.keySeq()) // : Map[str, Seq[str]]
    // 2. Filter the candidate id groups that contain more than 1 candidate id with the same ranking
    // 3. flatten to a set of candidate ids that have errors
    return candidateIdsByRank
      .filter(cids => !cids.skip(1).isEmpty())
      .flatMap((cids, rank) => cids.map(cid => [cid, `Duplicate rank ${rank}`]))
      .toMap()
  }

  getCandidateList(candidates: List<ImCandidate>) {
    return Range(0, candidates.size, 2).map(ridx => {
      return (
        <tr key={`row-${ridx}`}>
          {Range(ridx, Math.min(ridx + 2, candidates.size)).map(cidx => {
            const candidate = candidates.get(cidx)!
            const id = candidate.get('id')
            const rank = this.state.ranking.get(id, '') // either a number or empty string
            if (!this.props.editable) {
              return (
                <td key={`col-${cidx}`} className="candidate">
                  <span
                    className={`rank-box ${
                      this.state.warnings.has(id) ? 'warning' : ''
                    }`}
                  >
                    <label>{rank}</label>
                  </span>
                  <span>{candidate.get('name')}</span>
                </td>
              )
            } else {
              return (
                <td
                  key={`election-${this.electionId}_candidate-${cidx}`}
                  className="candidate"
                >
                  <span
                    className={`rank-box ${
                      this.state.warnings.has(id) ? 'warning' : ''
                    }`}
                  >
                    <FormControl
                      type="text"
                      maxLength={5}
                      onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                        const newRank = e.target.value
                          ? parseInt(e.target.value, 10)
                          : null
                        const updatedRank = newRank
                          ? this.state.ranking.set(id, newRank)
                          : this.state.ranking.delete(id)
                        this.setState({
                          ranking: updatedRank,
                          warnings: PaperBallot.validateBallot(updatedRank)
                        })
                        if (isFunction(this.props.onRankingChange)) {
                          this.props.onRankingChange(updatedRank)
                        }
                      }}
                      value={rank.toString()}
                    />
                  </span>
                  <span>{candidate.get('name')}</span>
                </td>
              )
            }
          })}
        </tr>
      )
    })
  }

  render() {
    return (
      <div className="printPage">
        <h2>
          {' '}
          {this.props.election.get('name', 'Unnamed')} Election Ballot #
          {this.props.ballotKey}{' '}
        </h2>
        <p>
          Rank the candidates in the order of your choice. Rank as many or as
          few as you wish. Be sure not to use the same rank for more than one
          candidate.
        </p>
        <table className="ballotCandidates">
          <tbody>{this.getCandidateList(this.shuffledCandidates)}</tbody>
        </table>
      </div>
    )
  }
}

export default connect<
  PaperBallotStateProps,
  null,
  PaperBallotOwnProps,
  RootReducer
>(state => state)(PaperBallot)
