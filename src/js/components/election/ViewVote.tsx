import { Map } from 'immutable'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Col, Form, Container, Row } from 'react-bootstrap'
import { bindActionCreators, Dispatch } from 'redux'
import { isMemberLoaded } from '../../services/members'
import { Elections, ImGetVoteResponse } from '../../client/ElectionClient'
import {
  fetchElection,
  fetchElections
} from '../../redux/actions/electionActions'
import Loading from '../common/Loading'
import FieldGroup from '../common/FieldGroup'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { Helmet } from 'react-helmet'
import PageHeading from 'src/js/components/common/PageHeading'

type ViewVoteProps = ViewVoteStateProps & ViewVoteDispatchProps

interface ViewVoteState {
  electionId: number | null
  code: string | null
  result: ImGetVoteResponse | null
  searched: boolean
}

class ViewVote extends Component<ViewVoteProps, ViewVoteState> {
  constructor(props: ViewVoteProps) {
    super(props)
    this.state = {
      electionId: null,
      code: null,
      result: null,
      searched: false
    }
  }

  componentDidMount() {
    this.props.fetchElections()
  }

  render() {
    if (!isMemberLoaded(this.props.member)) {
      return <Loading />
    }

    const votes = this.props.member
      .getIn(['user', 'data', 'votes'])
      .filter(vote => vote.get('voted'))

    const body =
      votes.size === 0 ? (
        <p>You haven't voted in any elections before.</p>
      ) : (
        this.renderBody(votes)
      )
    return (
      <Container>
        <Helmet>
          <title>Check my vote</title>
        </Helmet>
        <PageHeading level={1}>Check my vote</PageHeading>
        {body}
      </Container>
    )
  }

  renderBody(votes) {
    const electionsById = votes.map(vote => [
      vote.get('election_id'),
      vote.get('election_name')
    ]) as Map<number, string>
    const disabled = this.state.electionId === null || this.state.code === null

    return (
      <Form inline onSubmit={e => e.preventDefault()}>
        <Row>
          <p>
            To check how you voted in an election, enter the 6-digit
            confirmation number. Note that once you've voted, your vote cannot
            be changed.
          </p>
        </Row>
        <Row>
          <Col xs={12} sm={8} md={6}>
            <FieldGroup
              formKey="election_id"
              componentClass="select"
              options={Map(electionsById).toObject()}
              value={this.state.electionId}
              placeholder="Select an election"
              label="Election"
              onFormValueChange={(formKey, value) => {
                this.setState({ electionId: parseInt(value, 10) })
              }}
            />
          </Col>
        </Row>
        <Row>
          <Col xs={12} sm={8} md={6}>
            <div className="form-group">
              <label htmlFor="code" className="col-sm-3 control-label">
                Confirmation Number
              </label>
              <Col sm={9}>
                <input
                  id="code"
                  type="text"
                  placeholder="000000"
                  maxLength={6}
                  onChange={e => {
                    this.setState({ code: e.target.value })
                  }}
                />
              </Col>
            </div>
          </Col>
        </Row>
        <Row>
          <button
            type="button"
            onClick={() => this.fetchVote()}
            disabled={disabled}
          >
            Check vote
          </button>
        </Row>
        {this.renderResult()}
      </Form>
    )
  }

  async fetchVote() {
    const { code, electionId } = this.state

    if (electionId != null && code != null) {
      this.setState({ result: null, searched: false })

      // Make an asynchronous call to fetch the candidates.
      this.props.fetchElection(electionId)

      try {
        const vote = await Elections.getVote(electionId, code)
        this.setState({ result: vote, searched: true })
      } catch (err) {
        this.setState({ result: null, searched: true })
      }
    }
  }

  renderResult() {
    const electionId = this.state.electionId
    if (electionId === null) {
      return null
    }
    const election = this.props.elections.getIn(['byId', electionId])
    const electionName = election.get('name')

    if (this.state.result == null && this.state.searched) {
      return (
        <div>
          <h3>Your vote was not found.</h3>
          <p>
            We couldn't find a vote on <strong>{electionName}</strong> with the
            confirmation code <strong>{this.state.code}</strong>.
          </p>
        </div>
      )
    } else if (this.state.result != null) {
      const rankings = this.state.result.get('rankings')
      const candidates = election.get('candidates')

      let candidateList
      if (candidates == null) {
        candidateList = <p>Loading...</p>
      } else if (rankings.size === 0) {
        candidateList = <p>You did not select any candidates.</p>
      } else if (rankings.size === 1) {
        const candidate = candidates.find(c => c.get('id') === rankings.first())
        candidateList = (
          <p>
            You selected <strong>{candidate.get('name')}</strong>.
          </p>
        )
      } else {
        candidateList = (
          <div>
            <p>
              These are your rankings for <strong>{electionName}</strong>:
            </p>
            <ol>
              {rankings.map(ranking => {
                const candidate = candidates.find(c => c.get('id') === ranking)
                return (
                  <li key={`candidate-${candidate.get('id')}`}>
                    {candidate.get('name')}
                  </li>
                )
              })}
            </ol>
          </div>
        )
      }

      return (
        <div>
          <h3>We got your vote!</h3>
          {candidateList}
        </div>
      )
    }
  }
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators({ fetchElection, fetchElections }, dispatch)

type ViewVoteStateProps = RootReducer
type ViewVoteDispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<
  ViewVoteStateProps,
  ViewVoteDispatchProps,
  null,
  RootReducer
>(
  state => state,
  mapDispatchToProps
)(ViewVote)
