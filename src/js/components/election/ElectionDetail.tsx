import dateFormat from 'dateformat'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link, RouteComponentProps } from 'react-router'
import { Container } from 'react-bootstrap'
import { List, Map } from 'immutable'
import { logError } from '../../util/util'
import { eligibilityEmail } from '../../services/emails'
import {
  Elections,
  ElectionDetailsResponse,
  ImElectionDetailsResponse,
  ImEligibleVotersById
} from '../../client/ElectionClient'
import {
  Members,
  ImMemberVote,
  ImMemberDetailed
} from '../../client/MemberClient'
import {
  eligibleVoteStatus,
  electionStatus,
  EligibleVoteStatus
} from '../../services/elections'
import { isAdmin } from '../../services/members'
import Candidate from './Candidate'
import ElectionResults from './ElectionResults'
import PageHeading from '../common/PageHeading'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { ImCandidate } from 'src/js/client/CandidateClient'
import { Helmet } from 'react-helmet'

dateFormat.masks.dsaElection = "dddd, mmmm dS 'at' h:MM TT"

export interface ElectionDetailParamProps {
  electionId: string
}
interface ElectionDetailRouteParamProps {}
type ElectionDetailStateProps = RootReducer
type ElectionDetailOtherProps = RouteComponentProps<
  ElectionDetailParamProps,
  ElectionDetailRouteParamProps
>
type ElectionDetailProps = ElectionDetailStateProps & ElectionDetailOtherProps

interface ElectionDetailState {
  member: ImMemberDetailed | null
  election: ImElectionDetailsResponse | null
  eligible: ImEligibleVotersById | null
  inSubmission: boolean
  results: {}
}

class ElectionDetail extends Component<
  ElectionDetailProps,
  ElectionDetailState
> {
  constructor(props: ElectionDetailProps) {
    super(props)
    this.state = {
      member: null,
      election: null,
      eligible: null,
      inSubmission: false,
      results: {}
    }
  }

  componentDidMount() {
    this.getMemberDetails()
    this.getElectionDetails()
    if (isAdmin(this.props.member)) {
      this.getEligibleVoters()
    }
  }

  render() {
    if (this.state.election != null) {
      const candidates = this.state.election
        .get('candidates')
        .map(candidate => (
          <Candidate
            key={candidate.get('id') || ''}
            imageUrl={candidate.get('image_url') || ''}
            name={candidate.get('name') || ''}
            height={60}
          />
        ))

      return (
        <Container>
          <Helmet>
            <title>{this.state.election.get('name')}</title>
          </Helmet>
          <PageHeading level={1}>{this.state.election.get('name')}</PageHeading>
          <h3>Election Details</h3>
          <p className="text-pre-wrap">
            {this.state.election.get('description')}
          </p>
          {this.renderElectionStatusSummary()}
          {this.renderVoterStatusSummary()}
          {this.renderEligibleVoterSummary()}
          {this.renderVoteDescriptionImage()}
          {this.renderCandidateCountSummary()}
          <h2>Candidates</h2>
          {candidates.toArray()}
          {this.renderAdminTools()}
        </Container>
      )
    } else {
      return null
    }
  }

  renderElectionStatusSummary() {
    if (this.state.election != null) {
      const status = electionStatus(this.state.election)
      const startTime = this.state.election.get('voting_begins_epoch_millis')
      const endTime = this.state.election.get('voting_ends_epoch_millis')

      let electionStatusText: React.ReactChild = status
      if (status === 'polls not open') {
        electionStatusText = (
          <span>Voting will begin {dateFormat(startTime, 'dsaElection')}.</span>
        )
      } else if (status === 'polls closed') {
        electionStatusText = (
          <span>
            Voting closed {dateFormat(endTime, 'dsaElection')}, and we're
            waiting for the final count.
          </span>
        )
      } else if (status === 'final') {
        electionStatusText = (
          <span>
            Voting closed {dateFormat(endTime, 'dsaElection')}, and the ballots
            have been counted.
          </span>
        )
      } else if (status === 'polls open') {
        electionStatusText = endTime ? (
          <span>Voting is open!</span>
        ) : (
          <span>
            Voting is open until {dateFormat(endTime, 'dsaElection')}.
          </span>
        )
      } else if (status === 'draft') {
        electionStatusText = <span>This election has not been published.</span>
      } else if (status === 'canceled') {
        electionStatusText = <span>This election was canceled.</span>
      }

      return (
        <p>
          <b>Election status:</b> {electionStatusText}
        </p>
      )
    } else {
      return <p>Loading election...</p>
    }
  }

  renderVoterStatusSummary() {
    const { electionId } = this.props.params

    if (this.state.election != null) {
      const vote = (this.state.member
        ? this.state.member.get('votes')
        : List<ImMemberVote>()
      ).find(
        vote => vote.get('election_id').toString() === electionId.toString()
      )
      const voteStatus: EligibleVoteStatus =
        vote != null
          ? eligibleVoteStatus(
              this.state.election.set('id', parseInt(electionId)),
              vote
            )
          : 'not found'

      const status = electionStatus(this.state.election)

      let eligibilityText: JSX.Element | React.ReactText = voteStatus
      switch (voteStatus) {
        case 'ineligible':
          if (status === 'draft' || status === 'polls not open') {
            eligibilityText = (
              <span>
                You have not been marked as eligible for this vote. Voting has
                not yet begun, so you may be added soon. If you have questions
                about this vote, contact {eligibilityEmail()}.
              </span>
            )
          } else if (status === 'polls open') {
            eligibilityText = (
              <span>
                You are not listed as eligible to vote. If you believe this is a
                mistake, contact {eligibilityEmail()}.
              </span>
            )
          } else {
            eligibilityText = 'You were ineligible for this vote.'
          }
        case 'already voted':
          if (status === 'polls closed' || status === 'final') {
            eligibilityText =
              'You were eligible for this vote, and your ballot was counted.'
          } else {
            eligibilityText =
              'You are eligible to vote, and your ballot has been counted.'
          }
        case 'eligible':
          if (status === 'draft' || status === 'polls not open') {
            eligibilityText = 'You will be eligible to vote.'
          } else if (status === 'polls open') {
            eligibilityText = 'You are eligible to vote.'
          } else {
            eligibilityText = 'You were eligible to vote.'
          }
      }

      return (
        <p>
          <b>Voter Status:</b> {eligibilityText}{' '}
          <b>
            {voteStatus === 'eligible' && status === 'polls open' && (
              <a href={`/vote/${electionId}`}>Vote Now!</a>
            )}
          </b>
        </p>
      )
    } else {
      return <p>Loading voter status...</p>
    }
  }

  renderEligibleVoterSummary() {
    if (this.state.eligible != null && this.state.eligible.size > 0) {
      return (
        <p>
          <b>Eligible Voters:</b> {this.state.eligible.size} members are
          eligible for this vote.
        </p>
      )
    } else {
      return null
    }
  }

  renderVoteDescriptionImage() {
    if (
      this.state.election == null ||
      this.state.election.get('description_img') == null
    ) {
      return null
    }

    return (
      <p>
        <img
          style={{
            maxWidth: '100%',
            maxHeight: '100%'
          }}
          src={this.state.election.get('description_img')}
        />
      </p>
    )
  }

  renderCandidateCountSummary() {
    if (this.state.election != null) {
      return (
        <p>
          <b>Number of positions:</b> This election is to select{' '}
          {this.state.election.get('number_winners')} out of{' '}
          {this.state.election.get('candidates').size} candidates.
        </p>
      )
    } else {
      return
    }
  }

  renderAdminTools() {
    if (!isAdmin(this.props.member)) {
      return null
    }

    const { electionId } = this.props.params

    return (
      <div>
        <h2>Manage Election</h2>
        <Link to={`/elections/${electionId}/edit`}>
          <button>Edit Election</button>
        </Link>
        <Link to={`/elections/${electionId}/print`}>
          <button>Print Ballots</button>
        </Link>
        <Link
          key={`enter-ballots-${electionId}`}
          to={`/admin/elections/${electionId}/vote`}
        >
          <button>Enter Ballots</button>
        </Link>
        <Link to={`/elections/${electionId}/signin`}>
          <button>Sign In Kiosk</button>
        </Link>
        <h2>Results</h2>
        <ElectionResults params={this.props.params} />
      </div>
    )
  }

  async getElectionDetails() {
    try {
      const results = await Elections.getElection(
        parseInt(this.props.params.electionId)
      )

      if (results != null) {
        this.setState({ election: results })
      }
    } catch (err) {
      return logError('Error fetching election', err)
    }
  }

  async getEligibleVoters() {
    try {
      const results = await Elections.getEligibleVoters(
        parseInt(this.props.params.electionId)
      )

      if (results != null) {
        this.setState({ eligible: results })
      }
    } catch (err) {
      return logError('Error fetching eligible voters', err)
    }
  }

  // TODO: Move this to redux
  async getMemberDetails() {
    try {
      const member = await Members.getCurrentUser()
      this.setState({ member })
    } catch (err) {
      return logError('Error loading member details', err)
    }
  }
}

export default connect<ElectionDetailStateProps, null, null, RootReducer>(
  state => state
)(ElectionDetail)
