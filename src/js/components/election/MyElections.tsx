import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Container } from 'react-bootstrap'
import { Link } from 'react-router'
import { bindActionCreators, Dispatch } from 'redux'
import { electionStatus } from '../../services/elections'
import { eligibilityEmail } from '../../services/emails'
import { isMember, isMemberLoaded } from '../../services/members'
import { fetchElections } from '../../redux/actions/electionActions'
import Loading from '../common/Loading'
import PastElections from './PastElections'
import UpcomingElections from './UpcomingElections'
import PageHeading from '../common/PageHeading'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { ImMemberVote } from 'src/js/client/MemberClient'
import { c } from 'src/js/config'
import { Helmet } from 'react-helmet'

type MyElectionsProps = MyElectionsStateProps & MyElectionsDispatchProps

class MyElections extends Component<MyElectionsProps> {
  componentDidMount() {
    this.props.fetchElections()
  }

  render() {
    const memberLoaded = isMemberLoaded(this.props.member)
    const votesLoaded =
      memberLoaded &&
      this.props.member
        .getIn(['user', 'data', 'votes'])
        .every(vote => this.getElection(vote.get('election_id')) !== null)
    if (!votesLoaded) {
      return <Loading />
    }

    return (
      <Container>
        <Helmet>
          <title>My Elections</title>
        </Helmet>
        <PageHeading level={1}>My Elections</PageHeading>
        <h3>Can I vote in any elections?</h3>
        {this.renderCurrentElections()}
        <h3>Will I be able to vote in future elections?</h3>
        {this.renderFutureEligibility()}
        <h3>What elections have already happened?</h3>
        {this.renderPastElections()}
        <h3>How do I see how I voted?</h3>
        {this.renderVoteVerification()}
      </Container>
    )
  }

  renderCurrentElections() {
    const memberData = this.props.member.getIn(['user', 'data'])

    const allVotes = memberData.get('votes')
    const activeVotes = allVotes.filter(vote =>
      this.isCurrentElection(vote.get('election_id'))
    )

    if (activeVotes.size === 0) {
      return (
        <p>
          There are currently no open elections that you are eligible to vote
          in. Check back when the election starts!
        </p>
      )
    } else {
      const voted = activeVotes
        .filter(vote => vote.get('voted'))
        .map(vote => this.renderCurrentElection(vote))
      const votedMessage =
        voted.size === 0 ? null : <p>Thanks for voting in {voted}.</p>

      const notVoted = activeVotes
        .filter(vote => !vote.get('voted'))
        .map(vote => this.renderCurrentElection(vote))
      const notVotedMessage =
        notVoted.size === 0 ? (
          <p>
            You have already voted in all currently open elections that you are
            eligible to vote in.
          </p>
        ) : (
          <p>You are eligible to vote on {notVoted.interpose(', ')}.</p>
        )

      return (
        <div>
          {votedMessage}
          {notVotedMessage}
        </div>
      )
    }
  }

  renderCurrentElection(vote: ImMemberVote) {
    const electionId = vote.get('election_id')

    return (
      <Link
        to={`/elections/${electionId}/`}
        key={`election-link-${electionId}`}
      >
        <strong>{vote.get('election_name')}</strong>
      </Link>
    )
  }

  renderFutureEligibility() {
    const memberData = this.props.member.getIn(['user', 'data'])

    let eligibilityMessage
    if (memberData.get('is_eligible')) {
      eligibilityMessage = (
        <p>
          <strong>Yes!</strong> You currently meet the eligibility requirements,
          so you'll be eligible to vote on future chapter business.
        </p>
      )
    } else if (isMember(this.props.member)) {
      eligibilityMessage = (
        <div>
          <p>
            No. Members who have either attended 2 of the previous 3 general
            meeting <i>or</i> are active in at least one committee or working
            group are eligible to vote.
          </p>
          <p>
            No committees have marked you as active, and you haven't attended
            enough recent meetings to be eligible. If you've been active in a
            committee or working group, go to{' '}
            <Link to="/my-committees">My Committees</Link> and request
            confirmation from a co-chair.
          </p>
        </div>
      )
    } else {
      eligibilityMessage = (
        <div>
          <p>
            No. Members who have either attended 2 of the previous 3 general
            meeting <i>or</i> are active in at least one committee or working
            group are eligible to vote.
          </p>
          <p>
            You are currently not listed as a member of our chapter. If you'd
            like to sign up, visit{' '}
            <a href={c('URL_CHAPTER_JOIN')}>our signup page</a> and forward your
            confirmation email to {eligibilityEmail()}.
          </p>
        </div>
      )
    }

    return (
      <div>
        {eligibilityMessage}
        <UpcomingElections />
      </div>
    )
  }

  renderPastElections() {
    return (
      <div>
        <PastElections />
        <p>
          To view all online votes that have been held at {c('CHAPTER_NAME')},
          check out the <Link to={`/elections`}>complete list</Link>.
        </p>
      </div>
    )
  }

  renderVoteVerification() {
    return (
      <p>
        If you have the 6-digit confirmation code from when you cast your vote,
        you can use it to{' '}
        <Link to="/my-elections/view-vote">see how you voted</Link>.
      </p>
    )
  }

  getElection(electionId: number) {
    return this.props.elections.getIn(['byId', electionId])
  }

  isCurrentElection(electionId: number) {
    const election = this.getElection(electionId)
    return election && electionStatus(election) === 'polls open'
  }
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators({ fetchElections }, dispatch)

type MyElectionsStateProps = RootReducer
type MyElectionsDispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<
  MyElectionsStateProps,
  MyElectionsDispatchProps,
  null,
  RootReducer
>(
  state => state,
  mapDispatchToProps
)(MyElections)
