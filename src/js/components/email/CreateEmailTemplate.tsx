import React, { Component } from 'react'
import { connect } from 'react-redux'
import { fromJS } from 'immutable'
import { bindActionCreators, Dispatch } from 'redux'
import _ from 'lodash'

import { saveEmailTemplate } from '../../redux/actions/emailTemplateActions'
import { fetchEmailTopics } from '../../redux/actions/emailTopicActions'
import PageHeading from '../common/PageHeading'
import {
  ImEmailTemplateForm,
  ImEmailTemplateById
} from 'src/js/client/EmailTemplateClient'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { RouteComponentProps } from 'react-router'
import { EmailTopic } from 'src/js/client/EmailTopicClient'
import { Container } from 'react-bootstrap'

type CreateEmailTemplateOtherProps = RouteComponentProps<{}, {}>
type CreateEmailTemplateProps = CreateEmailTemplateStateProps &
  CreateEmailTemplateDispatchProps &
  CreateEmailTemplateOtherProps

interface CreateEmailTemplateState {
  template: ImEmailTemplateForm
}

class CreateEmailTemplate extends Component<
  CreateEmailTemplateProps,
  CreateEmailTemplateState
> {
  selectedTopicRef = React.createRef<HTMLSelectElement>()

  constructor(props: CreateEmailTemplateProps) {
    super(props)
    this.state = {
      template: fromJS({
        name: '',
        subject: '',
        body: '',
        topic_id: -1
      })
    }
    this.props.fetchEmailTopics()
  }

  canSend(template: ImEmailTemplateForm): boolean {
    return (
      template.get('name') !== '' &&
      template.get('subject') !== '' &&
      template.get('body') !== '' &&
      template.get('topic_id') !== -1
    )
  }

  save() {
    const { template } = this.state
    if (this.canSend(template)) {
      this.props.saveEmailTemplate(template)
    }
  }

  handleNameChange: React.ChangeEventHandler<HTMLTextAreaElement> = event => {
    const { template } = this.state
    this.setState({
      template: template.set('name', event.target.value)
    })
  }

  handleSubjectChange: React.ChangeEventHandler<
    HTMLTextAreaElement
  > = event => {
    const { template } = this.state
    this.setState({
      template: template.set('subject', event.target.value)
    })
  }

  handleBodyChange: React.ChangeEventHandler<HTMLTextAreaElement> = event => {
    const { template } = this.state
    this.setState({
      template: template.set('body', event.target.value)
    })
  }

  handleTopicChange: React.ChangeEventHandler<HTMLSelectElement> = event => {
    const newTopic = parseInt(event.currentTarget.value)
    console.log(newTopic)
    this.setState(({ template }) => {
      return { template: template.set('topic_id', newTopic) }
    })
  }

  getSelectedTopic() {
    const select = this.selectedTopicRef.current
    return select?.options[select.selectedIndex].value
  }

  render() {
    const topics = this.getTopics()
    return (
      <Container>
        <PageHeading level={2}>New Template</PageHeading>
        Topic / Recipients:
        <select
          id="topicsSelect"
          ref={this.selectedTopicRef}
          style={{ marginLeft: 10 }}
          value={this.state.template.get('topic_id')}
          onChange={this.handleTopicChange}
        >
          {topics}
        </select>
        <br />
        <br />
        Title
        <br />
        <textarea
          name="title"
          rows={1}
          cols={60}
          onChange={this.handleNameChange}
        />
        <br />
        <br />
        Subject
        <br />
        <textarea
          name="subject"
          rows={1}
          cols={60}
          onChange={this.handleSubjectChange}
        />
        <br />
        <br />
        Body
        <br />
        <textarea
          name="body"
          rows={10}
          cols={60}
          onChange={this.handleBodyChange}
        />
        <br />
        <br />
        <button
          type="button"
          name="save"
          onClick={() => this.save()}
          disabled={!this.canSend(this.state.template)}
        >
          Save
        </button>
        <br />
        <br />
      </Container>
    )
  }

  getTopics() {
    const topics = this.props.topics.byId
    return _.map(topics, (topic: EmailTopic) => {
      return <option value={topic.id}>{topic.name}</option>
    }).concat([<option value="-1">None</option>])
  }
}

function getNewId(templates) {
  return templates.maxBy(t => t.get('id') || fromJS({ id: -1 })).get('id')
}

const mapStateToProps = (
  state: RootReducer,
  props: CreateEmailTemplateOtherProps
): CreateEmailTemplateStateProps => {
  if (state.templates.size > 0) {
    const templates = state.templates.get('byId') as ImEmailTemplateById | null // TODO coercion; fix later
    if (
      templates != null &&
      props.location.state != null &&
      templates.size > props.location.state.template_count
    ) {
      const highestId = getNewId(templates)
      const template = templates.get(highestId)
      props.router.replace({
        pathname: `/email/template/${template.get('id')}`,
        state: template
      })
    }
  }
  return state
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators({ fetchEmailTopics, saveEmailTemplate }, dispatch)

type CreateEmailTemplateStateProps = RootReducer
type CreateEmailTemplateDispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<
  CreateEmailTemplateStateProps,
  CreateEmailTemplateDispatchProps,
  CreateEmailTemplateOtherProps,
  RootReducer
>(
  mapStateToProps,
  mapDispatchToProps
)(CreateEmailTemplate)
