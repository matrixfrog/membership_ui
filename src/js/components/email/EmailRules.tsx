import React, { Component } from 'react'
import { bindActionCreators, Dispatch } from 'redux'
import { connect } from 'react-redux'
import { Col, FormControl, Row } from 'react-bootstrap'
import { isAdmin } from '../../services/members'
import * as emailRuleActions from '../../redux/actions/emailRuleActions'
import PageHeading from '../common/PageHeading'
import { RouteComponentProps } from 'react-router'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { ImEmail } from 'src/js/client/EmailAddressClient'
import { Helmet } from 'react-helmet'

interface EmailRulesParamProps {}
interface EmailRulesRouteParamProps {}
type EmailRulesProps = EmailRulesStateProps &
  EmailRulesDispatchProps &
  RouteComponentProps<EmailRulesParamProps, EmailRulesRouteParamProps>

interface EmailRulesState {
  inSubmission: boolean
}

class EmailRules extends Component<EmailRulesProps, EmailRulesState> {
  constructor(props: EmailRulesProps) {
    super(props)
    this.state = {
      inSubmission: false
    }
  }

  componentDidMount() {
    this.props.fetchEmailRules()
  }

  render() {
    if (!isAdmin(this.props.member)) {
      return <div />
    }
    const emails: JSX.Element[] = []
    this.props.emails.get('byId').forEach((email: ImEmail, emailIndex) => {
      const forwardingAddresses: JSX.Element[] = []
      let button: JSX.Element | null = null
      let emailListContent: JSX.Element | null = null
      if (!this.state.inSubmission) {
        if (email.get('status') === 'new') {
          button = (
            <button onClick={e => this.props.saveEmailRule(email)}>
              Add Email
            </button>
          )
        } else if (email.get('status') === 'modified') {
          button = (
            <button
              style={{ marginTop: '8px' }}
              onClick={e => this.props.saveEmailRule(email)}
            >
              Modify Email
            </button>
          )
        }
      }
      email
        .get('forwarding_addresses')
        .forEach((forwardingAddress, forwardingIndex) => {
          forwardingAddresses.push(
            <div
              style={{ marginTop: '8px' }}
              key={`forward-${emailIndex}-${forwardingIndex}`}
              className="form-inline"
            >
              <FormControl
                type="text"
                value={forwardingAddress}
                onChange={e =>
                  this.props.updateForwardingAddress(
                    emailIndex,
                    forwardingIndex,
                    e.target.value
                  )
                }
                aria-label="Which email should be forwarded to?"
              />
              <button
                onClick={e =>
                  this.props.deleteForwardingAddress(
                    emailIndex,
                    forwardingIndex
                  )
                }
                style={{ marginLeft: '8px' }}
                aria-label={`Delete ${forwardingAddress} from being forwarded to`}
              >
                x
              </button>
            </div>
          )
        })
      emailListContent = (
        <div key={`email-admin-${emailIndex}`}>
          <hr />
          <Row style={{ paddingTop: '8px' }}>
            <Col sm={6} lg={5}>
              <p>
                <strong>Incoming Email List Address:</strong>
              </p>
              <div style={{ margin: '8px 0' }} className="form-inline">
                <button
                  onClick={e => this.props.deleteEmailRule(email)}
                  aria-label={`Delete ${email.get(
                    'email_address'
                  )} from the list of incoming emails for this rule`}
                >
                  x
                </button>
                <FormControl
                  type="text"
                  value={email.get('email_address')}
                  onChange={e =>
                    this.props.updateEmailAddress(emailIndex, e.target.value)
                  }
                  aria-label="Incoming email"
                />
              </div>
            </Col>
            <Col sm={6} lg={5}>
              <p>
                <strong>Forwarding To Member Emails:</strong>
              </p>
              {forwardingAddresses}
              <button
                style={{ margin: '8px 0' }}
                onClick={e => this.props.addForwardingAddress(emailIndex)}
                aria-label="Add an email address to forward to"
              >
                +
              </button>
              {button && <hr />}
              {button}
            </Col>
          </Row>
        </div>
      )
      if (email.get('status') === 'new') {
        emails.unshift(emailListContent)
      } else {
        emails.push(emailListContent)
      }
    })
    return (
      <div
        className="email-rules-container"
        style={{ paddingLeft: '32px', paddingBottom: '48px' }}
      >
        <Helmet>
          <title>Emails</title>
        </Helmet>
        <PageHeading level={2}>Emails </PageHeading>
        <button
          style={{ marginTop: '16px' }}
          type="submit"
          onClick={e => {
            this.props.newEmailRule()
            this.props.addForwardingAddress('new')
          }}
        >
          Add New Email
        </button>
        {emails}
      </div>
    )
  }
}

const mapStateToProps = (state: RootReducer) => state

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(emailRuleActions, dispatch)

type EmailRulesStateProps = RootReducer
type EmailRulesDispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<
  EmailRulesStateProps,
  EmailRulesDispatchProps,
  null,
  RootReducer
>(
  mapStateToProps,
  mapDispatchToProps
)(EmailRules)
