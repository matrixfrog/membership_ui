import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link, RouteComponentProps } from 'react-router'
import { fetchEmailTemplates } from '../../redux/actions/emailTemplateActions'
import { List } from 'immutable'
import { isAdmin } from '../../services/members'
import PageHeading from '../common/PageHeading'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { Dispatch, bindActionCreators } from 'redux'
import {
  ImEmailTemplateById,
  ImEmailTemplate
} from 'src/js/client/EmailTemplateClient'
import { Helmet } from 'react-helmet'

interface EmailPageParamProps {}
interface EmailPageRouteParamProps {}
type EmailPageProps = EmailPageStateProps &
  EmailPageDispatchProps &
  RouteComponentProps<EmailPageParamProps, EmailPageRouteParamProps>

class EmailPage extends Component<EmailPageProps> {
  componentDidMount() {
    this.props.fetchEmailTemplates()
  }

  render() {
    const templates = this.props.templates.get('byId')

    if (templates != null) {
      const templateLinks = this.getTemplateLinks(templates)
      return (
        <div style={{ paddingLeft: '32px' }}>
          <Helmet>
            <title>Email Templates</title>
          </Helmet>
          <PageHeading level={2}>Email Templates</PageHeading>
          <ul>{templateLinks}</ul>
          <br />
          <br />
          <button
            type="button"
            name="newTemplate"
            onClick={() => this.createNewTemplate(templates.size)}
          >
            New Template
          </button>
        </div>
      )
    } else {
      return (
        <div>
          <span>Templates not loaded yet</span>
        </div>
      )
    }
  }

  getTemplateLinks(templates: ImEmailTemplateById) {
    return !isAdmin(this.props.member)
      ? List()
      : templates.valueSeq().map((tmpl: ImEmailTemplate) => {
          const linkTo = {
            pathname: `/email/template/${tmpl.get('id')}`,
            state: tmpl
          }
          return (
            <li key={tmpl.get('id')}>
              <Link to={linkTo}>{tmpl.get('name')}</Link>
            </li>
          )
        })
  }

  createNewTemplate(templateCount: number) {
    this.props.router.push({
      pathname: '/email/newTemplate',
      state: { template_count: templateCount }
    })
  }
}

const mapStateToProps = (state: RootReducer) => state

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators({ fetchEmailTemplates }, dispatch)

type EmailPageStateProps = RootReducer
type EmailPageDispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<
  EmailPageStateProps,
  EmailPageDispatchProps,
  null,
  RootReducer
>(
  mapStateToProps,
  mapDispatchToProps
)(EmailPage)
