import React from 'react'

import { EmailTopic } from 'src/js/client/EmailTopicClient'

type TopicSelectorProps = {
  currentTopic: number
  topics: Array<EmailTopic>
  onChange: (event: React.ChangeEvent<HTMLSelectElement>) => void
}

const TopicSelector: React.FC<TopicSelectorProps> = ({
  currentTopic,
  topics,
  onChange
}) => {
  const topicList = Object.values(topics)
  const topicOptions = topicList.map(topic => {
    return (
      <option value={topic.id} key={topic.id}>
        {topic.name}
      </option>
    )
  })
  return (
    <>
      <label htmlFor="topicsSelect">Topic / Recipients:</label>
      <select
        id="topicsSelect"
        style={{ marginLeft: 10 }}
        value={currentTopic}
        onChange={onChange}
      >
        {topicOptions}
      </select>
    </>
  )
}

export default TopicSelector
