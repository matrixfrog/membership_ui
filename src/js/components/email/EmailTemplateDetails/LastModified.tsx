import React from 'react'

type LastModifiedProps = {
  modifiedAt: number
}

const LastModified: React.FC<LastModifiedProps> = ({ modifiedAt }) => {
  const modifiedDate = new Date(modifiedAt).toDateString()
  return <div>Date modified: {modifiedDate}</div>
}

export default React.memo(LastModified)
