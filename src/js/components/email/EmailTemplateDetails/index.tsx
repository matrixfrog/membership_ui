import React, { useEffect, useState, useCallback } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { Container } from 'react-bootstrap'
import { Helmet } from 'react-helmet'

import { ImEmailTemplateForm } from 'src/js/client/EmailTemplateClient'

import {
  fetchEmailTemplate,
  saveEmailTemplate
} from '../../../redux/actions/emailTemplateActions'
import { fetchEmailAddresses } from '../../../redux/actions/emailAddressActions'
import { fetchEmailTopics } from '../../../redux/actions/emailTopicActions'
import { sendEmailTemplate } from '../../../redux/actions/emailTopicActions'
import { updateEmailTemplate } from '../../../redux/actions/emailTemplateActions'

import FromSelector from './FromSelector'
import TopicSelector from './TopicSelector'
import SubjectLine from './SubjectLine'
import EmailBody from './EmailBody'
import LastModified from './LastModified'

const EmailTemplateDetails = props => {
  useEffect(() => {
    props.fetchTemplate(parseInt(props.params.templateId))
    props.fetchEmailAddresses()
    props.fetchEmailTopics()
  }, [])
  const template = props.templates.get('template')
  const [isDirty, setIsDirty] = useState(false)
  const [fromEmail, setFromEmail] = useState('')
  const handleSave = () => {
    if (
      template != null &&
      template.get('name') !== '' &&
      template.get('subject') !== '' &&
      template.get('body') !== ''
    ) {
      setIsDirty(false)
      props.saveTemplate((template as any) as ImEmailTemplateForm)
    }
  }
  const handleSend = () => {
    const templateId = template.get('id')
    const topicId = template.get('topic_id')
    const address = ''
    if (templateId != null && topicId != null && address) {
      props.sendEmailTemplate(topicId, templateId, address)
    }
  }

  if (!template) {
    return <Container>Loading...</Container>
  }
  const templateName = template.name
  const fromAddresses = props.emailAddresses.get('byId').valueSeq()
  const topics = props.topics.byId
  return (
    <Container>
      <Helmet>{templateName}</Helmet>
      <h2>{templateName}</h2>
      <br />
      <FromSelector
        addresses={fromAddresses}
        currentAddress={fromEmail}
        onChange={event => {
          setFromEmail(event.target.value)
        }}
      />
      <br />
      <TopicSelector
        currentTopic={template.get('topic_id')}
        topics={topics}
        onChange={event => {
          props.updateEmailTemplate(
            template.set('topic_id', parseInt(event.target.value))
          )
          setIsDirty(true)
        }}
      />
      <br />
      <SubjectLine
        subject={template.get('subject')}
        onChange={event => {
          props.updateEmailTemplate(template.set('subject', event.target.value))
          setIsDirty(true)
        }}
      />
      <br />
      <EmailBody
        body={template.get('body')}
        onChange={event => {
          props.updateEmailTemplate(template.set('body', event.target.value))
          setIsDirty(true)
        }}
      />
      <br />
      <LastModified modifiedAt={template.get('last_updated')} />
      <br />
      <div style={{ marginBottom: '2rem' }}>
        <button onClick={handleSave}>Save</button>
        <button onClick={handleSend} disabled={isDirty || !fromEmail}>
          Send
        </button>
      </div>
    </Container>
  )
}

const mapStateToProps = (state: RootReducer) => {
  return state
}

const mapDispatchToProps = (dispatch: Dispatch) => {
  return bindActionCreators(
    {
      saveTemplate: saveEmailTemplate,
      fetchTemplate: fetchEmailTemplate,
      fetchEmailAddresses,
      fetchEmailTopics,
      sendEmailTemplate,
      updateEmailTemplate
    },
    dispatch
  )
}

type StateProps = RootReducer
type DispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<StateProps, DispatchProps, null, RootReducer>(
  mapStateToProps,
  mapDispatchToProps
)(EmailTemplateDetails)
