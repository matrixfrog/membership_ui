import React from 'react'

type EmailBodyProps = {
  onChange: (event: React.ChangeEvent<HTMLTextAreaElement>) => void
  body: string
}

const EmailBody: React.FC<EmailBodyProps> = ({ onChange, body }) => {
  return (
    <>
      <label htmlFor="body">Body</label>
      <br />
      <textarea
        id="body"
        name="body"
        rows={15}
        cols={40}
        value={body}
        onChange={onChange}
      />
    </>
  )
}

export default EmailBody
