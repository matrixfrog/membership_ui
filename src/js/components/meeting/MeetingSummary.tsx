import React from 'react'
import { Map } from 'immutable'
import dateFormat from 'dateformat'
import { ImMeeting } from 'src/js/client/MeetingClient'
import PageHeading from '../common/PageHeading'
import { c } from 'src/js/config'

dateFormat.masks.meetingSummary = 'mmmm d, yyyy h:MM TT'

const GROUP_NAME_SINGULAR = c('GROUP_NAME_SINGULAR')

interface MeetingSummaryProps {
  meeting: ImMeeting
  committeeName: string
  showAdmin?: boolean
}

export default (props: MeetingSummaryProps): React.ReactElement => {
  const meeting = props.meeting || Map()
  const meetingId = meeting.get('id')
  const meetingName = meeting.get('name')
  const committeeId = meeting.get('committee_id')
  const meetingType = committeeId
    ? props.committeeName
      ? `${GROUP_NAME_SINGULAR} / ${props.committeeName}`
      : GROUP_NAME_SINGULAR
    : 'Chapter Meeting'
  const landingPage = meeting.get('landing_url')
  const startTime = meeting.get('start_time')
  const endTime = meeting.get('end_time')

  return (
    <>
      <PageHeading level={1}>{meetingName}</PageHeading>
      <p>
        <b>Meeting Type:</b> {meetingType}
      </p>
      {startTime && (
        <p>
          <b>Start Time:</b> {dateFormat(startTime, 'meetingSummary')}
        </p>
      )}
      {endTime && (
        <p>
          <b>End Time:</b> {dateFormat(endTime, 'meetingSummary')}
        </p>
      )}
      <p>
        <b>Check-in:</b> <a href={`/meetings/${meetingId}/kiosk`}>Kiosk</a>
      </p>
      {landingPage && (
        <p>
          <b>Landing Page:</b> <a href={landingPage}>{landingPage}</a>
        </p>
      )}
      {props.showAdmin && (
        <p>
          <a href={`/meetings/${meetingId}/admin`}>Edit</a>
        </p>
      )}
    </>
  )
}
