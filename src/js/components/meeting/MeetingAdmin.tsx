import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  Col,
  Row,
  Container,
  TabContainer,
  Tabs,
  Tab,
  Button
} from 'react-bootstrap'
import { fromJS } from 'immutable'
import {
  Meetings,
  ImMeeting,
  UpdateAttendeeAttributes
} from '../../client/MeetingClient'
import MemberSearchField from '../common/MemberSearchField'
import { AttendeeEligibilityRow, EmptyEligibilityRow } from '../admin/MemberRow'
import VoteEligiblePopup from './VoteEligiblePopup'
import EditMeeting from './EditMeeting'
import MeetingKiosk from './MeetingKiosk'
import MeetingDetail from './MeetingDetail'
import {
  EligibleMemberListEntry,
  ImEligibleMemberList,
  ImEligibleMemberListEntry
} from 'src/js/client/MemberClient'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { RouteComponentProps } from 'react-router'
import Loading from 'src/js/components/common/Loading'
import PageHeading from 'src/js/components/common/PageHeading'
import { Helmet } from 'react-helmet'
import MeetingInvitations from './MeetingInvitations'
import EditMeetingAgenda from './EditMeetingAgenda'
import {
  isGeneralMeeting,
  countEligibleAttendees,
  countProxyVotes
} from 'src/js/services/meetings'

type MeetingAdminStateProps = RootReducer
interface MeetingAdminParamProps {
  meetingId: string
}
interface MeetingAdminRouteParamProps {}
type MeetingAdminProps = MeetingAdminStateProps &
  RouteComponentProps<MeetingAdminParamProps, MeetingAdminRouteParamProps>

interface MeetingAdminState {
  meeting: ImMeeting | null
  attendees: ImEligibleMemberList | null
  lastAddedAttendee: ImEligibleMemberListEntry | null
  tabKey: string
}

class MeetingAdmin extends Component<MeetingAdminProps, MeetingAdminState> {
  constructor(props) {
    super(props)
    this.state = {
      meeting: null,
      attendees: null,
      lastAddedAttendee: null,
      tabKey: 'details'
    }
  }

  componentDidMount() {
    this.getMeeting()
    this.getMeetingAttendees()
  }

  render() {
    const attendees = this.state.attendees

    if (this.state.meeting == null) {
      return <Loading />
    }

    return (
      <Container className="meeting-admin">
        <Helmet>
          <title>{this.state.meeting.get('name')} Admin</title>
        </Helmet>
        <PageHeading level={1}>
          {this.state.meeting.get('name')}{' '}
          <small>
            {this.state.meeting.get('code')
              ? '#' + this.state.meeting.get('code')
              : ''}
          </small>
        </PageHeading>

        <Row>
          <Col>
            <Tabs
              id="meeting-tabs"
              activeKey={this.state.tabKey}
              onSelect={this.handleChangeTab}
              className="meeting-tabs"
              unmountOnExit
            >
              <Tab eventKey="details" title="Meeting details">
                <EditMeeting meeting={this.state.meeting} />
              </Tab>
              <Tab eventKey="agenda" title="Agenda">
                <EditMeetingAgenda meeting={this.state.meeting} />
              </Tab>
              <Tab eventKey="attachments" title="Attachments">
                <MeetingDetail
                  meetingId={this.props.params.meetingId}
                  meeting={this.state.meeting}
                  hideSummary
                />
              </Tab>
              <Tab eventKey="kiosk" title="Kiosk">
                <MeetingKiosk
                  meetingId={this.props.params.meetingId}
                  meeting={this.state.meeting}
                />
              </Tab>
              <Tab eventKey="signin" title="Assisted sign-in">
                <h3>Sign In</h3>
                <div className="meeting-member-search-container">
                  <MemberSearchField
                    onMemberSelected={e => this.onMemberSelected(e)}
                  />
                  <VoteEligiblePopup
                    numVotes={
                      this.state.lastAddedAttendee
                        ? this.state.lastAddedAttendee
                            .get('eligibility')
                            .get('num_votes')
                        : 0
                    }
                    visible={this.state.lastAddedAttendee != null}
                  />
                </div>
                <AttendeesList
                  attendees={attendees}
                  meeting={this.state.meeting}
                />
              </Tab>
              <Tab eventKey="invitations" title="Invitations">
                <MeetingInvitations meeting={this.state.meeting} />
              </Tab>
            </Tabs>
          </Col>
        </Row>
      </Container>
    )
  }

  handleChangeTab = (tabKey: string) => {
    this.setState({ tabKey })
  }

  async onMemberSelected(member: EligibleMemberListEntry) {
    await Meetings.addAttendee(parseInt(this.props.params.meetingId), member.id)
    this.setState({ lastAddedAttendee: fromJS(member) })
    setTimeout(() => {
      this.setState({ lastAddedAttendee: null })
    }, 5000)
    this.getMeetingAttendees()
  }

  async getMeetingAttendees() {
    await this.getMeeting()

    const meetingId = parseInt(this.props.params.meetingId)
    const results = await Meetings.getAttendees(meetingId)
    this.setState({ attendees: results.reverse() })
  }

  async getMeeting() {
    const meeting = await Meetings.get(parseInt(this.props.params.meetingId))

    if (meeting != null) {
      this.setState({ meeting: meeting })
    }
  }
}

const updateAttendeeClicked = async (
  member: ImEligibleMemberListEntry,
  meeting: ImMeeting,
  attributes: UpdateAttendeeAttributes
) => {
  await Meetings.updateAttendee(meeting.get('id'), member.get('id'), attributes)
  this.getMeetingAttendees()
}

const removeAttendeeClicked = async (
  member: ImEligibleMemberListEntry,
  meeting: ImMeeting
) => {
  if (meeting != null) {
    const meetingName = meeting.get('name')
    if (
      confirm(
        `Are you sure you want to remove ${member.get(
          'name'
        )} from the list of attendees for ${meetingName}?`
      )
    ) {
      await Meetings.removeAttendee(meeting.get('id'), member.get('id'))
      this.getMeetingAttendees()
    }
  }
}

interface AttendeeEntryProps {
  attendee: ImEligibleMemberListEntry
  meeting: ImMeeting
}

const AttendeeEntry: React.FC<AttendeeEntryProps> = ({ attendee, meeting }) => (
  <div className="attendee-entry">
    <span>{`${attendee.get('name')}`}</span>
    <Button
      className="attendee-remove"
      onClick={e => removeAttendeeClicked(attendee, meeting)}
      variant="danger"
    >
      remove
    </Button>
    {meeting != null && (
      <Button
        className="attendee-update-eligibility-to-vote"
        onClick={e =>
          updateAttendeeClicked(attendee, meeting, {
            update_eligibility_to_vote: true
          })
        }
        variant="outline-info"
      >
        mark as eligible to vote
      </Button>
    )}
    {isGeneralMeeting(meeting) ? (
      attendee.hasIn(['eligibility']) ? (
        <AttendeeEligibilityRow
          isPersonallyEligible={attendee.getIn(['eligibility', 'is_eligible'])}
          numVotes={attendee.getIn(
            ['eligibility', 'num_votes'],
            // if num_votes was not passed, assume is_eligible counts as 1
            attendee.getIn(['eligibility', 'is_eligible']) ? 1 : 0
          )}
        />
      ) : (
        <EmptyEligibilityRow />
      )
    ) : null}
  </div>
)

interface AttendeesListProps {
  attendees: ImEligibleMemberList | null
  meeting: ImMeeting | null
}

const AttendeesList: React.FC<AttendeesListProps> = ({
  attendees,
  meeting
}) => {
  if (attendees == null || meeting == null || attendees.size === 0) {
    return null
  }

  return (
    <section className="attendees-list">
      <h3>{'Attendees ' + (attendees ? '(' + attendees.size + ')' : '')}</h3>
      {isGeneralMeeting(meeting) && attendees != null ? (
        <small>
          <i>Eligible voters: {countEligibleAttendees(attendees)}</i>
          <br />
          <i>
            Number of proxy votes: {countProxyVotes(attendees)} (already
            included in Eligible voter count)
          </i>
        </small>
      ) : null}
      {attendees.map(attendee => {
        return <AttendeeEntry attendee={attendee} meeting={meeting} />
      })}
    </section>
  )
}

export default connect<MeetingAdminStateProps, null, {}, RootReducer>(
  state => state
)(MeetingAdmin)
