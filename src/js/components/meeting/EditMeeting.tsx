import React, { Component } from 'react'
import { bindActionCreators, Dispatch } from 'redux'
import { connect } from 'react-redux'
import { Col, Form, FormControl, Row, Container } from 'react-bootstrap'
import { capitalize } from 'lodash/fp'
import { Map, fromJS, List } from 'immutable'
import DateTime from 'react-datetime'
import moment, { Moment } from 'moment'
import { c } from 'src/js/config'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import * as committees from 'src/js/redux/actions/committeeActions'
import {
  Meetings,
  ImMeeting,
  ImPartialMeeting
} from 'src/js/client/MeetingClient'
import { CommitteesById, ImCommittee } from 'src/js/client/CommitteeClient'
import { isAdmin } from 'src/js/services/members'

const DEFAULT_GENERAL_COMMITTEE = 'General (no committee)'

interface EditMeetingOwnProps {
  meeting: ImMeeting
}
type EditMeetingProps = EditMeetingOwnProps &
  RootReducer &
  EditMeetingDispatchProps
interface EditMeetingState {
  meeting: ImMeeting
  inSubmission: boolean
}

class EditMeeting extends Component<EditMeetingProps, EditMeetingState> {
  constructor(props) {
    super(props)
    this.state = {
      meeting: props.meeting,
      inSubmission: false
    }
  }

  componentWillMount() {
    this.props.fetchCommittees()
  }

  updateName(name: string) {
    if (this.state.inSubmission || this.state.meeting == null) {
      return
    }

    this.setState({
      meeting: this.state.meeting.set('name', name)
    })
  }

  mapCommitteeIdToSelectValue(newMeetingCommitteeId: number | null): number {
    if (newMeetingCommitteeId == null) {
      return 0
    } else {
      return newMeetingCommitteeId
    }
  }

  updateCommitteeId(committeeIdString: string) {
    if (this.state.inSubmission || this.state.meeting == null) {
      return
    }

    const parsed = parseInt(committeeIdString)
    const committeeId = parsed <= 0 ? null : parsed
    this.setState({
      meeting: this.state.meeting.set('committee_id', committeeId)
    })
  }

  updateLandingUrl(landingUrl) {
    if (this.state.inSubmission || this.state.meeting == null) {
      return
    }

    this.setState({
      meeting: this.state.meeting.set('landing_url', landingUrl)
    })
  }

  updateStartTime(startTime: string | Moment) {
    if (this.state.inSubmission || this.state.meeting == null) {
      return
    }

    if (typeof startTime === 'string') {
      this.setState({
        meeting: this.state.meeting.set(
          'start_time',
          moment(startTime).toDate()
        )
      })
    } else {
      this.setState({
        meeting: this.state.meeting.set('start_time', startTime.toDate())
      })
    }
  }

  updateEndTime(endTime: string | Moment) {
    if (this.state.inSubmission || this.state.meeting == null) {
      return
    }

    if (typeof endTime === 'string') {
      this.setState({
        meeting: this.state.meeting.set('end_time', moment(endTime).toDate())
      })
    } else {
      this.setState({
        meeting: this.state.meeting.set('end_time', endTime.toDate())
      })
    }
  }

  updateCode(codeString: string) {
    if (this.state.inSubmission || this.state.meeting == null) {
      return
    }

    const digitString = codeString.replace(/[^\d]/gi, '')
    const parsed = digitString ? parseInt(digitString) : 0
    const code = parsed <= 0 ? null : parsed
    this.setState({
      meeting: this.state.meeting.set('code', code)
    })
  }

  updatePublished(publishedStr: string) {
    if (this.state.inSubmission || this.state.meeting == null) {
      return
    }

    this.setState({
      meeting: this.state.meeting.set('published', publishedStr === 'true')
    })
  }

  async autogenerateCode(e) {
    e.preventDefault()

    if (this.state.inSubmission || this.state.meeting == null) {
      return
    }
    this.setState({
      inSubmission: true
    })

    try {
      const updatedCode = (
        await Meetings.autogenerateMeetingCode(this.state.meeting.get('id'))
      ).getIn(['meeting', 'code'])
      const updatedMeeting = this.state.meeting.set(
        'code',
        updatedCode
      ) as ImMeeting
      this.setState({ meeting: updatedMeeting })
    } finally {
      this.setState({ inSubmission: false })
    }
  }

  async submitMeeting(e) {
    e.preventDefault()

    if (this.state.inSubmission || this.state.meeting == null) {
      return
    }
    this.setState({
      inSubmission: true
    })

    try {
      const attributeNames = [
        'name',
        'committee_id',
        'landing_url',
        'start_time',
        'end_time',
        'code',
        'published'
      ]
      const updatedAttributes = this.state.meeting
        // filter out attribute names we don't want to send up
        .filter((_, key) => attributeNames.includes(key))
        // treat falsy values as null when sending up (except the "published" key)
        .map((val, key) =>
          key === 'published' ? val : val || null
        ) as ImPartialMeeting
      // send up ISO 8601 strings
      if (updatedAttributes.includes('start_time')) {
        updatedAttributes['start_time'] = updatedAttributes[
          'start_time'
        ].toISOString()
      }
      if (updatedAttributes.includes('end_time')) {
        updatedAttributes['end_time'] = updatedAttributes[
          'end_time'
        ].toISOString()
      }

      const meeting = await Meetings.updateMeeting(
        this.state.meeting.get('id'),
        updatedAttributes
      )
      const updatedMeeting = this.state.meeting.merge(meeting) as ImMeeting
      this.setState({ meeting: updatedMeeting })
    } finally {
      this.setState({ inSubmission: false })
    }
  }

  render() {
    const isGeneralAdmin = isAdmin(this.props.member)
    const committeeAdminIds = this.props.member
      .getIn(['user', 'data', 'roles'], List())
      .filter(role => role.get('role') === 'admin' && role.get('committee_id'))
      .map(role => role.get('committee_id'))
      .toSet()
    let committeesById = this.props.committees.get(
      'byId',
      fromJS<CommitteesById>({})
    )
    if (!isGeneralAdmin) {
      committeesById = committeesById.filter((committee: ImCommittee) =>
        committeeAdminIds.has(committee.get('id'))
      )
    }
    const committeeIdsByName = committeesById
      .mapEntries<number, string>(([cid, c]: [number, ImCommittee]) => [
        cid,
        c.get('name')
      ])
      .set(0, DEFAULT_GENERAL_COMMITTEE) // add the empty option
      .sort()

    return (
      <>
        <h3>Edit Meeting</h3>
        <Form
          className="edit-meeting-form"
          onSubmit={e => this.submitMeeting(e)}
        >
          <Form.Group as={Row} controlId="formName">
            <Form.Label column sm={2}>
              Meeting name
            </Form.Label>
            <Col sm={6}>
              <FormControl
                type="text"
                value={this.state.meeting.get('name') || undefined}
                onChange={e => this.updateName(e.target.value)}
              />
            </Col>
          </Form.Group>
          <Form.Group as={Row} controlId="formCommittee">
            <Form.Label column sm={2}>
              {capitalize(c('GROUP_NAME_SINGULAR'))}
            </Form.Label>
            <Col sm={6}>
              <FormControl
                as="select"
                value={(this.state.meeting.get('committee_id') || 0).toString()}
                onChange={e =>
                  this.updateCommitteeId((e.target as HTMLSelectElement).value)
                }
              >
                {committeeIdsByName
                  .map((committeeName, committeeId) => (
                    <option value={committeeId} key={committeeId}>
                      {committeeName}
                    </option>
                  ))
                  .toArray()}
              </FormControl>
            </Col>
          </Form.Group>
          <Form.Group as={Row} controlId="formLandingUrl">
            <Form.Label column sm={2}>
              Landing URL
            </Form.Label>
            <Col sm={6}>
              <FormControl
                type="text"
                value={this.state.meeting.get('landing_url') || undefined}
                onChange={e => this.updateLandingUrl(e.target.value)}
              />
            </Col>
          </Form.Group>
          <Form.Group as={Row}>
            <Form.Label column sm={2}>
              Start Time
            </Form.Label>
            <Col sm={6}>
              <DateTime
                value={this.state.meeting.get('start_time') || undefined}
                onChange={m => this.updateStartTime(m)}
                timeConstraints={{
                  minutes: { min: 0, max: 59, step: 15 }
                }}
              />
            </Col>
          </Form.Group>
          <Form.Group column as={Row}>
            <Form.Label column sm={2}>
              End Time
            </Form.Label>
            <Col sm={6}>
              <DateTime
                value={this.state.meeting.get('end_time') || undefined}
                onChange={m => this.updateEndTime(m)}
                timeConstraints={{
                  minutes: { min: 0, max: 59, step: 15 }
                }}
              />
            </Col>
          </Form.Group>
          <Form.Group as={Row} controlId="formCode">
            <Form.Label column sm={2}>
              Meeting code
            </Form.Label>
            <Col sm={5}>
              <FormControl
                type="text"
                value={this.state.meeting.get('code')?.toString() || ''}
                onChange={e => this.updateCode(e.target.value)}
                minLength={4}
                maxLength={4}
              />
            </Col>
            <Col sm={1}>
              <button
                disabled={this.state.inSubmission || this.state.meeting == null}
                onClick={e => this.autogenerateCode(e)}
              >
                ♻
              </button>
            </Col>
          </Form.Group>
          <Form.Group as={Row} controlId="formPublished">
            <Form.Label column sm={2}>
              Published?
            </Form.Label>
            <Col sm={6}>
              <select
                value={'' + this.state.meeting.get('published', 'true')}
                onChange={e => this.updatePublished(e.target.value)}
              >
                <option value="true">Published</option>
                <option value="false">Unpublished</option>
              </select>
            </Col>
          </Form.Group>
          <button type="submit" onClick={e => this.submitMeeting(e)}>
            Save changes
          </button>
        </Form>
      </>
    )
  }
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      fetchCommittees: committees.fetchCommittees
    },
    dispatch
  )

type EditMeetingDispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<RootReducer, EditMeetingDispatchProps, {}, RootReducer>(
  state => state,
  mapDispatchToProps
)(EditMeeting)
