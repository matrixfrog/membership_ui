import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Container, ListGroup, ListGroupItem } from 'react-bootstrap'
import { adminEmail } from '../../services/emails'
import { isMemberLoaded } from '../../services/members'
import Loading from '../common/Loading'
import {
  Meetings,
  ProxyTokenState,
  ProxyTokenAction
} from '../../client/MeetingClient'
import { logError } from '../../util/util'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { RouteComponentProps } from 'react-router'
import { Helmet } from 'react-helmet'

type ProxyTokenConsumeStateProps = RootReducer
interface ProxyTokenConsumeParamProps {}
interface ProxyTokenConsumeRouteParamProps {
  meetingId: number
  proxyTokenId: number
}
type ProxyTokenConsumeProps = ProxyTokenConsumeStateProps &
  RouteComponentProps<
    ProxyTokenConsumeParamProps,
    ProxyTokenConsumeRouteParamProps
  >

interface ProxyTokenConsumeState {
  meetingName: string | null
  nominatorId: number | null
  nominatorName: string | null
  loading: boolean
  stateOnLoad: ProxyTokenState | null
  stateAfterAction: ProxyTokenState | null
  errorMessage: string | null
  recentlyActedMemberId: number | null
}

class ProxyTokenConsume extends Component<
  ProxyTokenConsumeProps,
  ProxyTokenConsumeState
> {
  copyableLinkRef = React.createRef<HTMLInputElement>()

  constructor(props) {
    super(props)
    this.state = {
      meetingName: null,
      nominatorId: null,
      nominatorName: null,
      loading: false,
      stateOnLoad: null,
      stateAfterAction: null,
      errorMessage: null,
      recentlyActedMemberId: null
    }
  }

  componentDidMount() {
    this.getMeeting()
    this.getProxyTokenState()
  }

  async getMeeting() {
    const meeting = await Meetings.get(this.props.params.meetingId)

    if (meeting != null) {
      this.setState({ meetingName: meeting.get('name') })
    } else {
      throw new Error("Couldn't load meeting")
    }
  }

  async getProxyTokenState() {
    const { meetingId, proxyTokenId } = this.props.routeParams
    try {
      this.setState({ loading: true })
      const result = await Meetings.getProxyTokenState(meetingId, proxyTokenId)

      if (result != null) {
        this.setState({
          loading: false,
          stateOnLoad: result.get('state'),
          nominatorId: result.get('nominator_id'),
          nominatorName: result.get('nominator_name'),
          recentlyActedMemberId: result.get('recently_acted_member_id')
        })
      } else {
        throw new Error("Couldn't load proxy token")
      }
    } catch (err) {
      this.setState({
        loading: false,
        errorMessage:
          'We could not find that proxy voter nomination. Please contact the person who sent you this link and let them know.'
      })
    }
  }

  acceptOrRejectNomination = async (action: ProxyTokenAction) => {
    const { meetingId, proxyTokenId } = this.props.routeParams
    try {
      this.setState({ loading: true })
      const result = await Meetings.acceptOrRejectProxyToken(
        meetingId,
        proxyTokenId,
        action
      )
      this.setState({
        loading: false,
        stateAfterAction: result.get('state'),
        errorMessage: null
      })
    } catch (err) {
      let errorMessage = `There was an error ${action}ing the proxy voter nomination for meeting ${meetingId}`
      try {
        errorMessage = err.error.response.body.err
      } catch (responseParseError) {}
      this.setState({
        loading: false,
        errorMessage
      })
    }
  }

  acceptNomination = async () => {
    return await this.acceptOrRejectNomination('accept')
  }

  rejectNomination = async () => {
    return await this.acceptOrRejectNomination('reject')
  }

  friendlyStateName(unfriendlyStateName) {
    return unfriendlyStateName === 'ACCEPTED'
      ? 'accepted'
      : unfriendlyStateName === 'REJECTED'
      ? 'rejected'
      : null
  }

  renderViewingOwn = () => {
    const { meetingName, stateOnLoad } = this.state
    const friendlyStateOnLoad = this.friendlyStateName(stateOnLoad)

    return friendlyStateOnLoad === 'accepted' ? (
      <p>
        Your nomination to have a proxy voter at {meetingName} has already been
        accepted!
      </p>
    ) : (
      <div>
        <p>
          You have made this link so you can nominate someone to vote on your
          behalf at {meetingName}.
        </p>
        <ListGroup>
          <ListGroupItem>
            <strong>1. Send the link</strong> below, along with instructions on
            how to cast your vote, to an eligible voter whom you want to vote on
            your behalf.
          </ListGroupItem>
          <ListGroupItem>
            <strong>2. When they visit the link</strong>, they can accept or
            reject your nomination. We'll let you know how they respond.
          </ListGroupItem>
          <ListGroupItem>
            <strong>3. When they check in</strong> to the meeting, they'll be
            able vote on your behalf, as long as they accepted your proxy
            nomination. You'll still have an opportunity to participate in any
            online votes, or your proxy can request a paper ballot.
          </ListGroupItem>
        </ListGroup>
        <div style={{ display: 'flex' }}>
          <input
            style={{ flex: '1 0 auto' }}
            type="text"
            value={window.location.href}
            onFocus={() => {
              this.copyableLinkRef.current?.select()
            }}
            ref={this.copyableLinkRef}
          />
          <button
            style={{ flex: '0 0 auto' }}
            onClick={() => {
              this.copyableLinkRef.current?.select()
              document.execCommand('copy')
            }}
          >
            Copy Link
          </button>
        </div>
      </div>
    )
  }

  renderViewingOthers = () => {
    const {
      meetingName,
      nominatorName,
      recentlyActedMemberId,
      stateOnLoad,
      stateAfterAction,
      errorMessage
    } = this.state
    const friendlyStateOnLoad = this.friendlyStateName(stateOnLoad)
    const friendlyStateAfterAction = this.friendlyStateName(stateAfterAction)
    const myMemberId = this.props.member.getIn(['user', 'data', 'id'])
    const currentMemberMostRecentlyActed = recentlyActedMemberId === myMemberId

    return friendlyStateOnLoad === 'accepted' ? (
      currentMemberMostRecentlyActed ? (
        <p>
          You have already accepted this nomination! You will be the proxy voter
          for {nominatorName} at {meetingName}.
        </p>
      ) : (
        <p>
          Sorry, but this nomination has already been accepted by someone else.
          Please contact {nominatorName} and confirm with them what's going on.
        </p>
      )
    ) : friendlyStateAfterAction ? (
      <p>
        You have successfully {friendlyStateAfterAction} the invitation to vote
        on behalf of {nominatorName}.{' '}
        {friendlyStateAfterAction === 'accepted'
          ? 'We will inform them, along with the people doing sign-in at the meeting.'
          : 'We will inform them so they can nominate someone else instead.'}
      </p>
    ) : (
      <div>
        {friendlyStateOnLoad === 'rejected' &&
        currentMemberMostRecentlyActed ? (
          <p>
            <strong>You have previously rejected</strong> this nomination. Let
            us know if you want to change that.
          </p>
        ) : (
          <p>
            <strong>You have been nominated</strong> to vote on {nominatorName}
            's behalf at {meetingName}.
          </p>
        )}
        <p>
          If you'll be attending the meeting and have instructions on how to
          cast their vote, go ahead and accept the proxy nomination. If they
          should nominate someone else, let them know by rejecting the
          nomination.
        </p>
        <p>
          <button onClick={this.acceptNomination}>Accept Nomination</button>{' '}
          <button onClick={this.rejectNomination}>Reject Nomination</button>
          {errorMessage ? <div>{errorMessage}</div> : null}
        </p>
        <p>
          <strong>Questions?</strong> Contact us at {adminEmail()} before
          responding to the nomination. Don't worry, this link is valid until{' '}
          {meetingName} ends.
        </p>
      </div>
    )
  }

  render() {
    const { loading, errorMessage, meetingName, nominatorId } = this.state
    const isLoading =
      loading || !meetingName || !isMemberLoaded(this.props.member)
    const myMemberId = this.props.member.getIn(['user', 'data', 'id'])
    const isViewingOwn = myMemberId === nominatorId

    return (
      <Container>
        <Helmet>
          <title>Proxy nomination</title>
        </Helmet>
        <h1>Nomination to be a Proxy Voter</h1>
        {isLoading ? (
          <Loading />
        ) : nominatorId ? (
          isViewingOwn ? (
            this.renderViewingOwn()
          ) : (
            this.renderViewingOthers()
          )
        ) : (
          <div>Error: {errorMessage}</div>
        )}
      </Container>
    )
  }
}

export default connect<ProxyTokenConsumeStateProps, null, {}, RootReducer>(
  state => state
)(ProxyTokenConsume)
