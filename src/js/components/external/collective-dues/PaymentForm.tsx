import { CardElement } from '@stripe/react-stripe-js'
import React from 'react'
import { FiLock } from 'react-icons/fi'
import { DuesRecurrence } from 'src/js/client/PaymentsClient'
import PageHeading from 'src/js/components/common/PageHeading'
import {
  ABBR_TIME_UNIT,
  DuesSubscriptionPaymentInfo,
  formatCurrency,
  TIME_UNIT
} from 'src/js/components/external/collective-dues/utils'
import { c } from 'src/js/config'

interface PaymentFormProps {
  paymentInfo: DuesSubscriptionPaymentInfo
}

const PaymentForm: React.FC<PaymentFormProps> = ({
  paymentInfo: { recurrence, nationalAmount, localAmount, totalAmount }
}) => (
  <section className="collective-dues-step payment-info-step">
    <PageHeading level={3}>Payment info</PageHeading>
    <CardElement
      className="stripe-card-element"
      options={{
        style: {
          base: { fontFamily: '"Work Sans", sans-serif', lineHeight: '1.5' }
        },
        hidePostalCode: true
      }}
    />
    <div className="payment-notice">
      <FiLock /> Securely processed by{' '}
      <a href="https://stripe.com/privacy">Stripe</a>. {c('CHAPTER_NAME_SHORT')}{' '}
      does not store your payment info.
    </div>
    <hr />
    <PaymentSummary
      recurrence={recurrence}
      nationalAmount={nationalAmount}
      localAmount={localAmount}
      totalAmount={totalAmount}
    />
  </section>
)

interface PaymentSummaryProps {
  recurrence: DuesRecurrence
  nationalAmount: number
  localAmount: number
  totalAmount: number
}

const PaymentSummary: React.FC<PaymentSummaryProps> = ({
  recurrence,
  nationalAmount,
  localAmount,
  totalAmount
}) => (
  <section className="payment-summary">
    {recurrence !== 'one-time' ? (
      <>
        <p>
          You are about to join {c('CHAPTER_NAME')} and enroll in{' '}
          <strong>{recurrence}</strong>-recurring collective dues.
        </p>
        <p>
          Your membership will be valid for one {TIME_UNIT[recurrence]} starting
          today and will renew each {TIME_UNIT[recurrence]} until canceled.
        </p>
        <p>
          To cancel your membership, please contact{' '}
          <a href={`mailto:${c('COLLECTIVE_DUES_CANCEL_EMAIL')}`}>
            {c('COLLECTIVE_DUES_CANCEL_EMAIL')}
          </a>
          . We're working on tools to enable you to manage your membership
          online.
        </p>
        <div className="split-breakdown">
          <div className="split-breakdown-item">
            <dt>{c('CHAPTER_NAME_SHORT')} (local chapter)</dt>
            <dd>
              <span className="split-breakdown-amount">
                ${formatCurrency(localAmount)}
              </span>
              <span className="split-breakdown-units">
                /
                <span className="split-breakdown-recurrence">
                  {ABBR_TIME_UNIT[recurrence]}
                </span>
              </span>
            </dd>
          </div>
          <div className="split-breakdown-item">
            <dt>National DSA</dt>
            <dd>
              <span className="split-breakdown-amount">
                ${formatCurrency(nationalAmount)}
              </span>
              <span className="split-breakdown-units">
                /
                <span className="split-breakdown-recurrence">
                  {ABBR_TIME_UNIT[recurrence]}
                </span>
              </span>
            </dd>
          </div>
        </div>
      </>
    ) : (
      <>
        <p>
          You are about to join {c('CHAPTER_NAME')} make a{' '}
          <strong>one-time</strong> dues contribution of{' '}
          <strong>${formatCurrency(totalAmount)}</strong>.
        </p>
        <p>Your membership will be valid for one year starting today.</p>
        <p>
          You may choose to renew your membership at any time. If you choose to
          renew before your membership expires, we will extend your membership
          for the renewal amount.
        </p>
        <div className="split-breakdown">
          <div className="split-breakdown-item">
            <dt>{c('CHAPTER_NAME_SHORT')} (local chapter)</dt>
            <dd>${formatCurrency(localAmount)}</dd>
          </div>
          <div className="split-breakdown-item">
            <dt>National DSA</dt>
            <dd>${formatCurrency(nationalAmount)}</dd>
          </div>
        </div>
      </>
    )}
    <div className="total-amount">
      <dt>Total payment today</dt>
      <dd>${formatCurrency(totalAmount)}</dd>
    </div>
  </section>
)

export default PaymentForm
