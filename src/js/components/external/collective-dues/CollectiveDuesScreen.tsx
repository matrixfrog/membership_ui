import React from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import { Helmet } from 'react-helmet'
import PageHeading from 'src/js/components/common/PageHeading'
import CollectiveDuesForm from 'src/js/components/external/collective-dues/CollectiveDuesForm'
import { c } from 'src/js/config'

export const CollectiveDuesScreen: React.FC = () => (
  <Container className="external-screen">
    <Helmet titleTemplate="">
      <title>Join {c('CHAPTER_NAME')}</title>
    </Helmet>
    <main className="collective-dues-screen">
      <Row>
        <Col md={6} className="description-column">
          <header>
            <PageHeading level={1}>Join {c('CHAPTER_NAME')}</PageHeading>
          </header>
          <aside>
            <p>
              Thank you for your interest in the {c('CHAPTER_AREA_NAME')}{' '}
              chapter of the Democratic Socialists of America!
            </p>
            <p>
              Our local chapter is made up of ordinary people fighting for
              socialism in the heart of {c('CHAPTER_AREA_NAME')}. We are a
              democratic, all-volunteer organization funded entirely by member
              dues and donations.
            </p>
            <p>
              We invite you to join us in the struggle. As a member, you can
              vote in our local meetings and run for leadership positions in the
              chapter.
            </p>
            <p>
              To join, fill out this form. If you're able to afford dues, select
              your dues amount and your preferred split between our local
              chapter and National DSA. If you can't afford dues at this time,
              that’s okay! We'll sponsor your membership using dues paid by your
              comrades.
            </p>
            <p>
              <strong>
                If you’re not in or near {c('CHAPTER_AREA_NAME')}:
              </strong>{' '}
              please{' '}
              <a href="https://dsausa.org/join">
                join DSA through the national organization
              </a>{' '}
              and they will connect you to your local chapter.
            </p>
          </aside>
        </Col>
        <Col md={6} className="form-column">
          <CollectiveDuesForm />
          <footer>
            <p>
              {c('CHAPTER_NAME')}, {new Date().getFullYear()}
              {'  '}🌹{'  '}Volunteer labor donated
            </p>
          </footer>
        </Col>
      </Row>
    </main>
    <footer></footer>
  </Container>
)
