import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'
import { Navbar, NavbarBrand, Nav } from 'react-bootstrap'
import { fetchMember } from '../../redux/actions/memberActions'
import { isAdmin } from '../../services/members'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { c, USE_ELECTIONS } from 'src/js/config'
import { capitalize, compact } from 'lodash/fp'
import { NavLinkEntry } from 'src/js/components/admin/Admin'
import logo from '../../../../images/dsa-logo.png'

const navMap: NavLinkEntry[] = compact([
  {
    link: '/home',
    label: 'Home'
  },
  {
    link: '/my-membership',
    label: 'Membership'
  },
  USE_ELECTIONS && {
    link: '/my-elections',
    label: 'Elections'
  },
  {
    link: '/my-committees',
    label: capitalize(c('GROUP_NAME_PLURAL'))
  },
  {
    link: '/my-meetings',
    label: 'Meetings'
  },
  {
    link: '/my-contact-info',
    label: 'Contact Info'
  },
  {
    link: '/my-resources',
    label: 'Resources'
  }
])

type NavigationProps = NavigationStateProps & NavigationDispatchProps

class Navigation extends Component<NavigationProps> {
  componentWillReceiveProps(nextProps) {
    if (localStorage == null) {
      return
    }
    const authUser = localStorage.user
    if (authUser == null || authUser.email == null) {
      return
    }
    const dbMember = nextProps.member.getIn(['user', 'data'], null)
    if (
      (dbMember === null || dbMember.get('email_address') !== authUser.email) &&
      !nextProps.member.getIn(['user', 'loading']) &&
      nextProps.member.getIn(['user', 'error']) === null
    ) {
      this.props.fetchMember()
    }
  }

  render() {
    const links = navMap.map(({ link, label }) => (
      <Nav.Item key={link} className="dsaNavItem">
        <Nav.Link href={link}>{label}</Nav.Link>
      </Nav.Item>
    ))
    return (
      <Navbar collapseOnSelect tabIndex={-1} expand="lg" className="mainNav">
        <NavbarBrand href="/home">
          <img
            src={logo}
            alt=""
            className="d-inline-block align-top"
            style={{ maxHeight: '55px' }}
          />
        </NavbarBrand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse>
          <Nav className="mr-auto">
            {links}
            {isAdmin(this.props.member) && (
              <Nav.Item key="admin-link" className="dsaNavItem">
                <Nav.Link href="/admin">Admin</Nav.Link>
              </Nav.Item>
            )}
          </Nav>
          <Nav>
            <Nav.Item className="dsaNavItem">
              <Nav.Link href="/logout">Logout</Nav.Link>
            </Nav.Item>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    )
  }
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      fetchMember
    },
    dispatch
  )

type NavigationDispatchProps = ReturnType<typeof mapDispatchToProps>
type NavigationStateProps = RootReducer

export default connect<
  NavigationStateProps,
  NavigationDispatchProps,
  {},
  RootReducer
>(
  state => state,
  mapDispatchToProps
)(Navigation)
