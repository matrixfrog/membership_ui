import { History } from 'history'
import React, { Component } from 'react'
import { Helmet } from 'react-helmet'
import { connect } from 'react-redux'
import { IndexRedirect, Route, Router } from 'react-router'
import { HistoryUnsubscribe } from 'react-router-redux'
import { bindActionCreators, Dispatch } from 'redux'
import AddMember from 'src/js/components/admin/AddMember'
import Admin from 'src/js/components/admin/Admin'
import AdminNavShell from 'src/js/components/admin/AdminNavShell'
import ImportRoster from 'src/js/components/admin/ImportRoster'
import MemberList from 'src/js/components/admin/MemberList'
import App from 'src/js/components/App'
import RequireAuth from 'src/js/components/auth/requireAuth'
import Signin from 'src/js/components/auth/signin'
import SigningOut from 'src/js/components/auth/SigningOut'
import Callback from 'src/js/components/callback/Callback'
import Committee from 'src/js/components/committee/Committee'
import Committees from 'src/js/components/committee/Committees'
import MyCommittees from 'src/js/components/committee/MyCommittees'
import NotFound from 'src/js/components/common/NotFound'
import CreateElection from 'src/js/components/election/CreateElection'
import ElectionDetail from 'src/js/components/election/ElectionDetail'
import ElectionEdit from 'src/js/components/election/ElectionEdit'
import Elections from 'src/js/components/election/Elections'
import EnterVote from 'src/js/components/election/EnterVote'
import MyElections from 'src/js/components/election/MyElections'
import PrintBallots from 'src/js/components/election/PrintBallots'
import { RankedChoiceVisualization } from 'src/js/components/election/RankedChoiceVisualization'
import SignInKiosk from 'src/js/components/election/SignInKiosk'
import ViewVote from 'src/js/components/election/ViewVote'
import Vote from 'src/js/components/election/Vote'
import CreateEmailTemplate from 'src/js/components/email/CreateEmailTemplate'
import EmailPage from 'src/js/components/email/EmailPage'
import EmailRules from 'src/js/components/email/EmailRules'
import EmailTemplateDetails from 'src/js/components/email/EmailTemplateDetails'
import CollectiveDuesScreen from 'src/js/components/external/collective-dues'
import VerifyEmail from 'src/js/components/external/verify-email'
import NewsletterSignupScreen from 'src/js/components/external/newsletter-signup'
import CreateMeeting from 'src/js/components/meeting/CreateMeeting'
import MeetingAdmin from 'src/js/components/meeting/MeetingAdmin'
import MeetingDetailPage from 'src/js/components/meeting/MeetingDetailPage'
import MeetingKioskPage from 'src/js/components/meeting/MeetingKioskPage'
import MeetingRsvp from 'src/js/components/meeting/MeetingRsvp'
import Meetings from 'src/js/components/meeting/Meetings'
import MyMeetings from 'src/js/components/meeting/MyMeetings'
import ProxyTokenConsume from 'src/js/components/meeting/ProxyTokenConsume'
import ProxyTokenCreate from 'src/js/components/meeting/ProxyTokenCreate'
import MainPage from 'src/js/components/membership/MainPage'
import MemberPage from 'src/js/components/membership/MemberPage'
import MyContactInfo from 'src/js/components/membership/MyContactInfo'
import MyMembership from 'src/js/components/membership/MyMembership'
import MyResources from 'src/js/components/resources/MyResources'
import {
  PAGE_HEADING_HTML_ID,
  PAGE_TITLE_TEMPLATE,
  USE_ANONYMOUS_SIGNUPS,
  USE_COLLECTIVE_DUES,
  USE_ELECTIONS,
  c
} from 'src/js/config'
import { fullySignOut } from 'src/js/redux/actions/auth/index'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import focusOnElement from 'src/js/util/focusOnElement'

interface RoutesOwnProps {
  history: History<any> & HistoryUnsubscribe
}

type RoutesProps = RoutesStateProps & RoutesDispatchProps & RoutesOwnProps

class Routes extends Component<RoutesProps> {
  constructor(props) {
    super(props)

    this.props.history.listen(({ action }) => {
      // Only change focus when moving to a new URL (not on redirects).
      if (action === 'PUSH') {
        focusOnElement({
          id: PAGE_HEADING_HTML_ID
        })
      }
    })
  }

  shouldComponentUpdate() {
    return false
  }

  render() {
    const electionRoutes = (
      <>
        <Route path="my-elections" component={RequireAuth(MyElections)} />
        <Route
          path="elections/:electionId"
          component={RequireAuth(ElectionDetail)}
        />
        <Route
          path="my-elections/view-vote"
          component={RequireAuth(ViewVote)}
        />
        <Route
          path="elections/:electionId/results"
          component={RequireAuth(RankedChoiceVisualization)}
        />
        <Route
          path="elections/:electionId/results/fullscreen"
          component={RequireAuth(RankedChoiceVisualization)}
        />
        <Route
          path="admin/elections/:electionId/vote"
          component={RequireAuth(EnterVote)}
        />
        <Route
          path="elections/:electionId/print"
          component={RequireAuth(PrintBallots)}
        />
        <Route path="vote/:electionId" component={RequireAuth(Vote)} />
      </>
    )

    const electionAdminRoutes = (
      <>
        <Route path="elections" component={RequireAuth(Elections)} />
        <Route
          path="elections/create"
          component={RequireAuth(CreateElection)}
        />
        <Route
          path="elections/:electionId/edit"
          component={RequireAuth(ElectionEdit)}
        />
        <Route
          path="elections/:electionId/signin"
          component={RequireAuth(SignInKiosk)}
        />
      </>
    )

    return (
      <>
        <Helmet
          titleTemplate={PAGE_TITLE_TEMPLATE}
          defaultTitle="DSA Member Portal"
        >
          <link
            rel="icon"
            type="image/png"
            sizes="32x32"
            href={c('URL_CHAPTER_FAVICON_32')}
          />
          <link
            rel="icon"
            type="image/png"
            sizes="192x192"
            href={c('URL_CHAPTER_FAVICON_192')}
          />
        </Helmet>
        <Router history={this.props.history}>
          {/* External, unauthenticated routes */}
          {USE_ANONYMOUS_SIGNUPS ? (
            <Route path="/newbies">
              <Route path="signup" component={NewsletterSignupScreen} />
              {USE_COLLECTIVE_DUES ? (
                <Route
                  path="collective-dues"
                  component={CollectiveDuesScreen}
                />
              ) : null}
            </Route>
          ) : null}

          <Route path="/verify-email(/:token)" component={VerifyEmail} />

          <Route path="/" component={App}>
            <IndexRedirect to="home" />
            <Route path="process" component={Callback} />
            <Route path="login" component={Signin} />
            <Route
              path="logout"
              onEnter={() => {
                this.props.fullySignOut()
              }}
              component={SigningOut}
            />

            {/* Admin and admin-bar routes */}
            <Route path="admin" component={RequireAuth(Admin)} />
            <Route component={AdminNavShell}>
              <Route path="members" component={RequireAuth(MemberList)} />
              <Route path="members/create" component={RequireAuth(AddMember)} />
              <Route path="import" component={RequireAuth(ImportRoster)} />
              <Route
                path="members/:memberId"
                component={RequireAuth(MemberPage)}
              />
              <Route path="meetings" component={RequireAuth(Meetings)} />
              <Route
                path="meetings/create"
                component={RequireAuth(CreateMeeting)}
              />
              <Route path="committees" component={RequireAuth(Committees)} />
              <Route path="email/rules" component={RequireAuth(EmailRules)} />
              <Route path="email" component={RequireAuth(EmailPage)} />
              <Route
                path="email/newTemplate"
                component={RequireAuth(CreateEmailTemplate)}
              />
              <Route
                path="email/template/:templateId"
                component={RequireAuth(EmailTemplateDetails)}
              />
              {USE_ELECTIONS ? electionAdminRoutes : null}
            </Route>

            {/* Authenticated routes */}
            <Route>
              <Route path="home" component={RequireAuth(MainPage)} />
              <Route path="member" component={RequireAuth(MemberPage)} />
              <Route
                path="meetings/:meetingId/kiosk"
                component={RequireAuth(MeetingKioskPage)}
              />
              <Route
                path="meetings/:meetingId/proxy-token"
                component={RequireAuth(ProxyTokenCreate)}
              />
              <Route
                path="meetings/:meetingId/proxy-token/:proxyTokenId"
                component={RequireAuth(ProxyTokenConsume)}
              />
              <Route
                path="meetings/:meetingId/details"
                component={RequireAuth(MeetingDetailPage)}
              />
              <Route
                path="meetings/:meetingId/rsvp/:token"
                component={RequireAuth(MeetingRsvp)}
              />
              <Route
                path="meetings/:meetingId/admin"
                component={RequireAuth(MeetingAdmin)}
              />
              <Route
                path="committees/:committeeId"
                component={RequireAuth(Committee)}
              />
              <Route
                path="my-membership"
                component={RequireAuth(MyMembership)}
              />
              <Route
                path="my-committees"
                component={RequireAuth(MyCommittees)}
              />
              <Route path="my-meetings" component={RequireAuth(MyMeetings)} />
              <Route
                path="my-contact-info"
                component={RequireAuth(MyContactInfo)}
              />
              <Route path="my-resources" component={RequireAuth(MyResources)} />
              {USE_ELECTIONS ? electionRoutes : null}
            </Route>
            <Route path="*" component={NotFound} />
          </Route>
        </Router>
      </>
    )
  }
}

const mapStateToProps = (state: RootReducer) => state
const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators({ fullySignOut }, dispatch)

type RoutesStateProps = RootReducer
type RoutesDispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<
  RoutesStateProps,
  RoutesDispatchProps,
  RoutesOwnProps,
  RootReducer
>(
  mapStateToProps,
  mapDispatchToProps
)(Routes)
