import { Api, ApiClient } from '../client/ApiClient'
import { List } from 'immutable'
import { FromJS } from 'src/js/util/typedMap'

interface CommitteeLeader {
  id: number
  committee_id: number
  member_id: number
  member_name: string
  role_title: string
  term_start_date: string
  term_end_date: string
}
export type ImCommitteeLeader = FromJS<CommitteeLeader>

export interface AddLeadershipRoleArgs {
  committeeId: number
  roleTitle: string
  termLimitMonths: number
}

export interface ReplaceLeaderArgs {
  committeeId: number
  leaderId: number
  memberId: number | null
  termStartDate?: string
  termEndDate?: string
}

export class CommitteeLeadershipClient {
  api: ApiClient

  constructor(api: ApiClient) {
    this.api = api
  }

  async list(committeeId: number): Promise<List<ImCommitteeLeader>> {
    const result = await this.api
      .url(`/committee/${committeeId}/leaders`)
      .get()
      .execute<List<ImCommitteeLeader>>()

    if (result != null) {
      return result
    } else {
      throw new Error('Got empty response when fetching committee leaders')
    }
  }

  async addLeadershipRole(
    args: AddLeadershipRoleArgs
  ): Promise<ImCommitteeLeader> {
    const { committeeId, roleTitle, termLimitMonths } = args
    const payload = { role_title: roleTitle }
    if (termLimitMonths) {
      payload['term_limit_months'] = termLimitMonths
    }
    const result = await this.api
      .url(`/committee/${committeeId}/leaders`)
      .post(payload)
      .execute<ImCommitteeLeader>()

    if (result != null) {
      return result
    } else {
      throw new Error('Got empty response when adding leadership role')
    }
  }

  async replaceLeader(args: ReplaceLeaderArgs): Promise<ImCommitteeLeader> {
    const { committeeId, leaderId, memberId, termStartDate, termEndDate } = args
    const payload = { member_id: memberId }
    if (termStartDate) {
      payload['term_start_date'] = termStartDate
    }
    if (termEndDate) {
      payload['term_end_date'] = termEndDate
    }
    const result = await this.api
      .url(`/committee/${committeeId}/leaders/${leaderId}`)
      .patch(payload)
      .execute<ImCommitteeLeader>()

    if (result != null) {
      return result
    } else {
      throw new Error('Got empty response when replacing a leader')
    }
  }

  async listRoleTitles(): Promise<List<string>> {
    const result = await this.api
      .url('/committee-leadership-roles')
      .get()
      .execute<List<string>>()

    if (result != null) {
      return result
    } else {
      throw new Error(
        'Got empty response when fetching committee leadership role titles'
      )
    }
  }
}

export const CommitteeLeadership = new CommitteeLeadershipClient(Api)

export default CommitteeLeadership
