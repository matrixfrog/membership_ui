import { Api, ApiClient, ApiStatus } from '../client/ApiClient'
import { fromJS, Map } from 'immutable'
import { FromJS, typedKeyBy } from 'src/js/util/typedMap'

interface Committee {
  id: number
  name: string
  provisional: boolean
  viewable: boolean
  email: string | null
}
export type ImCommittee = FromJS<Committee>

interface CommitteeMember {
  id: number
  name: string
}
export type ImCommitteeMember = FromJS<CommitteeMember>

interface CommitteeDetailed extends Committee {
  admins: CommitteeMember[]
  members: CommitteeMember[]
  active_members: CommitteeMember[]
  inactive: boolean
}
export type ImCommitteeDetailed = FromJS<CommitteeDetailed>

export interface CommitteesById {
  [id: number]: CommitteeDetailed
}
export type ImCommitteesById = Map<number, ImCommitteeDetailed>

export interface CreateCommitteeProps {
  name: string
  admin_list: string[]
  provisional: boolean
}

interface CreateCommitteeResponse extends ApiStatus {
  created: CommitteeDetailed
}
export type ImCreateCommitteeResponse = FromJS<CreateCommitteeResponse>

export class CommitteeClient {
  api: ApiClient

  constructor(api: ApiClient) {
    this.api = api
  }

  async all(): Promise<ImCommitteesById> {
    const result = await this.api
      .url(`/committee/list`)
      .get()
      // HACK: These types are somewhat incorrect.
      //
      // The Committee reducer hydrates committees from their basic
      // contents to their detailed contents when a user visits the
      // Committee page. However, there isn't a good way of representing
      // this hydration with current types since we use Immutable.js and
      // don't have the ability to discriminate with string types or
      // with T | null things (the mutable->immutable typing mangles these
      // nullable union types).
      //
      // Since the current Committee component has fallbacks to empty
      // lists for all of these detailed data, I'm going to treat this as
      // a list of detailed committee responses for now.
      .execute<FromJS<CommitteeDetailed[]>>()

    if (result != null) {
      return typedKeyBy(result, committee => committee.get('id'))
    } else {
      throw new Error('Got empty response when fetching committees')
    }
  }

  async get(committeeId: number): Promise<ImCommitteeDetailed | null> {
    const response = await this.api
      .url(`/committee/${committeeId}`)
      .get()
      .execute<ImCommitteeDetailed>()

    if (response != null) {
      return response
    } else {
      return null
    }
  }

  async create({
    name,
    admin_list,
    provisional
  }: CreateCommitteeProps): Promise<ImCreateCommitteeResponse> {
    const response = await this.api
      .url(`/committee`)
      .post({
        admin_list: admin_list,
        name: name,
        provisional: provisional
      })
      .execute<ImCreateCommitteeResponse>()

    if (response != null) {
      return response
    } else {
      throw new Error('Got an empty response when creating a committee')
    }
  }

  async requestMembership(committeeId: number): Promise<void> {
    const response = await this.api
      .url(`/committee/${committeeId}/member_request`)
      .post()
      .execute()

    if (response == null) {
      throw new Error(
        'Got an empty response when requesting membership in a committee'
      )
    }
  }

  async editInactiveStatus(
    committeeId: number,
    inactive: boolean
  ): Promise<void> {
    const response = await this.api
      .url(`/committee/${committeeId}`)
      .patch({ inactive })
      .execute()

    if (response == null) {
      throw new Error(
        'Got an empty response when attempting to edit committee inactive status'
      )
    }
  }
}

export const Committees = new CommitteeClient(Api)

export default Committees
