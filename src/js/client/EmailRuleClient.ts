import { Api, ApiClient } from '../client/ApiClient'
import {
  ImEmailsListResponse,
  EmailsListResponse,
  ImEmail,
  ImEmailUpdateResponse,
  EmailUpdateResponse
} from './EmailAddressClient'
import { fromJS } from 'immutable'

export class EmailRuleClient {
  api: ApiClient

  constructor(api: ApiClient) {
    this.api = api
  }

  async all(): Promise<ImEmailsListResponse | null> {
    const response = await this.api
      .url('/emails')
      .get()
      .execute<ImEmailsListResponse>()

    if (response != null) {
      return response
    } else {
      return null
    }
  }

  async create(email: ImEmail): Promise<ImEmailUpdateResponse> {
    const response = await this.api
      .url('/emails')
      .post(email)
      .execute<ImEmailUpdateResponse>()

    if (response != null) {
      return response
    } else {
      throw new Error(
        'Got empty response from server when creating an email address'
      )
    }
  }

  async update(email: ImEmail): Promise<ImEmailUpdateResponse> {
    const response = await this.api
      .url(`/emails/${email.get('id')}`)
      .put(email)
      .execute<ImEmailUpdateResponse>()

    if (response != null) {
      return response
    } else {
      throw new Error(
        'Got empty response from server when updating an email address'
      )
    }
  }

  async delete(email: ImEmail): Promise<void> {
    const response = await this.api
      .url(`/emails/${email.get('id')}`)
      .remove()
      .execute()

    if (response == null) {
      throw new Error('Got empty response ')
    }
  }
}

export const EmailRules = new EmailRuleClient(Api)
