import { Api, ApiClient } from '../client/ApiClient'
import { TypedMap, FromJS } from 'src/js/util/typedMap'
import { fromJS } from 'immutable'

export interface Candidate {
  id: number
  name: string
  image_url: string
}

export type ImCandidate = FromJS<Candidate>

export class CandidateClient {
  api: ApiClient

  constructor(api: ApiClient) {
    this.api = api
  }

  async create(
    electionId: number,
    candidate: Partial<ImCandidate>
  ): Promise<ImCandidate> {
    const response = await this.api
      .url(`/election/${electionId}/candidates`)
      .post(candidate)
      .execute<ImCandidate>()

    if (response != null) {
      return response
    } else {
      throw new Error(
        'Got an empty response from the server when creating a candidate'
      )
    }
  }

  async update(
    electionId: number,
    candidateId: number,
    candidate: Partial<ImCandidate>
  ): Promise<ImCandidate> {
    const response = await this.api
      .url(`/election/${electionId}/candidates/${candidateId}`)
      .patch(candidate)
      .execute<ImCandidate>()

    if (response != null) {
      return response
    } else {
      throw new Error(
        'Got an empty response from the server when updating a candidate'
      )
    }
  }

  async delete(electionId: number, candidateId: number): Promise<void> {
    const response = await this.api
      .url(`/election/${electionId}/candidates/${candidateId}`)
      .remove()
      .execute()

    if (response == null) {
      throw new Error(
        'Got an empty response from the server when deleting a candidate'
      )
    }
  }
}

export const Candidates = new CandidateClient(Api)

export default Candidates
