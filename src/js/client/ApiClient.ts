import {
  HTTP_DELETE,
  HTTP_GET,
  HTTP_PATCH,
  HTTP_POST,
  HTTP_PUT,
  logError
} from '../util/util'
import { fromJS, Set, Map, isImmutable, Collection } from 'immutable'
import { FromJS, ImmutableType } from '../util/typedMap'
import { MEMBERSHIP_API_URL, USE_AUTH } from '../config'
import request, { SuperAgentRequest, ResponseError } from 'superagent'
import { auth } from 'src/js/redux/actions/auth/universalAuth'
import { assign, isObject, isPlainObject, merge } from 'lodash/fp'
import { set } from 'lodash'

// TODO: Add equality for ApiRequest
// TODO: Add tests
type ApiMethod = 'GET' | 'POST' | 'DELETE' | 'PUT' | 'PATCH'

interface ApiParams {
  [key: string]: string | number | boolean
}

interface ApiHeaders {
  [key: string]: string
}

interface ApiRequestProps {
  baseUrl: string
  headers: ApiHeaders
  method: ApiMethod | null
  path: string
  params: ApiParams
  body: {}
  attachments: { [param: string]: File }
  respErrorHandler: ApiResponseErrorHandler
  requestValidator: ApiRequestValidator
  visualization: boolean
}

export interface ApiResponseErrorHandlerProps extends ApiRequestValidatorProps {
  error: ResponseError
}

type ApiResponseErrorHandler = (props: ApiResponseErrorHandlerProps) => boolean

interface ApiRequestValidatorProps {
  method: ApiMethod
  path: string
  params: ApiParams
  url: string
  body: {} | null
  attachments: { [param: string]: File }
  response: null
}

type ApiRequestValidator = (props: ApiRequestValidatorProps) => boolean

/**
 * An immutable-like request object that captures all information before issuing the request for
 * logging / debugging / sharing / testing purposes.
 *
 * Usage:
 *
 * const client = new ApiClient({baseUrl: 'http://localhost:8080'})
 * const api = client.url('/my/route')
 * const r1 = api.get() // GET http://localhost:8080/my/route
 * r1.executeOr({}) // makes request, handles error by logging and returns a Promise containing {}
 * r1.execute() // issues request again, handles error by logging and returns a failed Promise
 * const r2 = api.post({name: 'example'}) // POST http://localhost:8080/my/route {"name":"example"}
 * ...
 */
export class ApiRequest {
  static bodyJson: string
  baseUrl: string
  _headers: ApiHeaders
  method: ApiMethod | null
  path: string
  params: {}
  _body: {}
  attachments: { [param: string]: File }
  respErrorHandler: ApiResponseErrorHandler
  requestValidator: ApiRequestValidator
  url: string

  static validatePathOrLogError({
    method,
    path
  }: {
    method: ApiMethod
    path: string
  }) {
    if (!path) {
      logError(`Invalid path: '${path}' (${method})`)
      return false
    }
    return true
  }

  static validateHttpMethodOrLogError({
    method,
    path
  }: {
    method: ApiMethod
    path: string
  }) {
    switch (method.toUpperCase()) {
      case HTTP_GET:
      case HTTP_PUT:
      case HTTP_POST:
      case HTTP_PATCH:
      case HTTP_DELETE:
        return true
      default:
        logError(`Unrecognized http method: '${method}' (/${path})`)
        return false
    }
  }

  static validateResponseOrLogError({
    method,
    path,
    error
  }: {
    method: ApiMethod
    path: string
    error: ResponseError
  }): boolean {
    const { status } = error

    const body = this.bodyJson
    const bodyMessage =
      body == null
        ? 'with empty body'
        : `with body: ${JSON.stringify(body, null, 2)}`
    const debugRequest = `from request to ${method} ${path} ${bodyMessage}`

    if (status == null) {
      logError(`Unrecognized or unexpected status code ${debugRequest}`, error)
      return false
    }

    if (status >= 200 && status < 300) {
      return true
    } else {
      if (status >= 400 && status < 500) {
        logError(`BAD REQUEST ${debugRequest}`, error)
      } else if (status >= 500) {
        logError(`SERVER ERROR ${debugRequest}`, error)
      }

      return false
    }
  }

  static defaultRespErrorHandler: ApiResponseErrorHandler = ({
    method,
    path,
    error
  }) => {
    return (
      ApiRequest.validatePathOrLogError({ method, path }) &&
      ApiRequest.validateResponseOrLogError({ method, path, error })
    )
  }

  static defaultRequestValidator: ApiRequestValidator = ({ method, path }) => {
    return ApiRequest.validateHttpMethodOrLogError({ method, path })
  }

  constructor({
    baseUrl,
    headers = {},
    method,
    path,
    params = {},
    body = {},
    attachments = {},
    respErrorHandler = ApiRequest.defaultRespErrorHandler,
    requestValidator = ApiRequest.defaultRequestValidator
  }: Partial<ApiRequestProps>) {
    if (!baseUrl) {
      throw new Error(`ApiRequest.baseUrl must not be empty string or null`)
    }
    if (!path || !path.startsWith('/')) {
      throw new Error(
        `ApiRequest.path '${path}' must start with a forward slash '/'`
      )
    }
    this.baseUrl = baseUrl
    this._headers = headers
    this.method = method ? (method.toUpperCase() as ApiMethod) : null
    this.path = path
    this.params = params
    this._body = fromJS(body)
    this.attachments = attachments
    this.respErrorHandler = respErrorHandler
    this.requestValidator = requestValidator

    this.url = this.baseUrl + this.path
  }

  async execute<T extends ImmutableType>(
    context: Partial<ApiRequestValidatorProps> = {}
  ): Promise<T | null> {
    const response = await this.executeRaw<T>(context)
    return response != null ? fromJS<T>(response) : null
  }

  async executeOr<T extends ImmutableType, OrElse = T>(
    orElse: OrElse,
    context: Partial<ApiRequestValidatorProps> = {}
  ): Promise<T | OrElse> {
    try {
      const response = await this.execute<T>(context)
      return response != null ? response : orElse
    } catch (e) {
      return orElse
    }
  }

  executeRaw = async <T>(
    context: Partial<ApiRequestValidatorProps> = {}
  ): Promise<T | null> => {
    if (this.method === null) {
      throw new Error(
        'ApiClient.execute called before .get, .post, .put, or .delete'
      )
    }
    const body = this.bodyJson
    const headers = this.headers
    const props: ApiRequestValidatorProps = {
      method: this.method,
      path: this.path,
      params: this.params,
      url: this.url,
      body,
      attachments: this.attachments,
      response: null
    }
    const invalidKeys = Set(Object.keys(props)).intersect(Object.keys(context))
    if (invalidKeys.size > 0) {
      throw new Error(
        `Cannot pass the following keys in the ApiRequest context: ${invalidKeys.toArray()}`
      )
    }
    const descriptor = assign(props, context)
    this.requestValidator(descriptor)
    let apiRequest: SuperAgentRequest = this.setRequestMethod(
      this.method,
      this.url
    )

    if (this.params) {
      apiRequest = apiRequest.query(this.params)
    }
    if (Object.keys(headers).length > 0) {
      apiRequest = apiRequest.set(headers)
    }
    if (body != null && Object.keys(body).length > 0) {
      apiRequest = apiRequest.type('application/json').send(body)
    }
    Object.keys(props.attachments).forEach(function(param) {
      apiRequest = apiRequest.attach(param, props.attachments[param])
    })

    const requestWithAuth = addAuthSessionRetry(addAuthHeaders(apiRequest))

    try {
      const result = await requestWithAuth
      const rawResult: T | null = result.body != null ? result.body : null
      if (rawResult != null) {
        return rawResult
      } else {
        return null
      }
    } catch (error) {
      if (error.status !== 401) {
        const errorDescriptor: ApiResponseErrorHandlerProps = {
          ...descriptor,
          error
        }
        this.respErrorHandler(errorDescriptor)
        throw errorDescriptor
      } else {
        return null
      }
    }
  }

  copy({
    baseUrl = this.baseUrl,
    headers = this._headers,
    method = this.method,
    path = this.path,
    params = this.params,
    body = this._body,
    attachments = this.attachments,
    respErrorHandler = this.respErrorHandler,
    requestValidator = this.requestValidator
  }: Partial<ApiRequestProps>) {
    return new ApiRequest({
      baseUrl,
      headers,
      method,
      path,
      params,
      body,
      attachments,
      respErrorHandler,
      requestValidator
    })
  }

  toString() {
    const header = `${this.method} ${this.baseUrl}${this.url}`
    const body = this.bodyJson
    let prettyJsonBody
    if (body) {
      prettyJsonBody = JSON.stringify(body, null, 2)
    }
    switch (this.method) {
      case 'POST':
      case 'PUT':
        return `${header}\n\n${prettyJsonBody}`
      default:
        return header
    }
  }

  get bodyJson() {
    if (!isObject(this._body)) return null
    else return isImmutable(this._body) ? this._body.toJS() : this._body
  }

  get headers() {
    if (!isObject(this._headers)) return {}
    else
      return isImmutable(this._headers) ? this._headers.toJS() : this._headers
  }

  /**
   * Provide a function that is called after the request completes.
   *
   * The second argument is the default handler for this API so that you can call it to have the
   * default logging of errors or whatever is configured for this API.
   *
   * @param recoverFn ((Descriptor => *), Descriptor) => * a function that combines
   *                  the previous handler with the API result descriptor. You should call
   *                  the default handler if you want to chain effects, such as logging.
   *                  NOTE: Returning a Promise will not wait until the promise completes
   *                  before completing the Promise returned by execute()
   * @returns {*} Ignored outside of error handlers,
   *              but default handlers return boolean so you can && short-circuit.
   */
  composeErrorHandler(recoverFn) {
    const compositeErrorHandler = descriptor =>
      recoverFn(this.respErrorHandler, descriptor)
    return this.copy({ respErrorHandler: compositeErrorHandler })
  }

  query(params) {
    if (isPlainObject(params)) {
      return this.copy({ params })
    } else {
      throw new Error(`Invalid query string params: ${params}`)
    }
  }

  get(): ApiRequest {
    if (this.method === HTTP_GET) return this
    else return this.copy({ method: HTTP_GET })
  }

  put(body?: {}): ApiRequest {
    return this.copy({ method: HTTP_PUT, body })
  }

  post(body?: {}): ApiRequest {
    return this.copy({ method: HTTP_POST, body })
  }

  patch(body: {}): ApiRequest {
    return this.copy({ method: HTTP_PATCH, body })
  }

  remove(): ApiRequest {
    if (this.method === HTTP_DELETE) return this
    else return this.copy({ method: HTTP_DELETE })
  }

  delete(body: {}): ApiRequest {
    return this.copy({ method: HTTP_DELETE, body })
  }

  attach(param: string, file: File) {
    const attachments = set(this.attachments, [param], file)
    return this.copy({ attachments })
  }

  setRequestMethod = (method: ApiMethod, url: string): SuperAgentRequest => {
    switch (method) {
      case 'GET':
        return request.get(url)
      case 'DELETE':
        return request.delete(url)
      case 'PUT':
        return request.put(url)
      case 'POST':
        return request.post(url)
      case 'PATCH':
        return request.patch(url)
      default:
        throw new Error(`Unsupported method: ${method}`)
    }
  }
}

export function addAuthSessionRetry(
  request: SuperAgentRequest
): SuperAgentRequest {
  return request.retry(1, async err => {
    if (err != null && err.status === 401) {
      // Auth0 session expired
      try {
        await auth.silentAuth()
        return true
      } catch (err) {
        throw err
      }
    } else {
      return false
    }
  })
}

export type ApiStatusType =
  | 'success'
  | 'failed'
  | 'match'
  | 'mismatch'
  | 'new'
  | 'error'

export interface ApiStatus {
  status: ApiStatusType
}

export type ImApiStatus = FromJS<ApiStatus>

/**
 * An immutable {@link ApiRequest} builder with default arguments.
 */
export class ApiClient {
  defaultArgs: {}

  constructor(defaultArgs: Partial<ApiRequestProps> = {}) {
    this.defaultArgs = defaultArgs
  }

  // TODO: deconstruct path query arguments?
  url(path: string, overrides: Partial<ApiRequestProps> = {}) {
    const args = merge(this.defaultArgs, overrides)
    return new ApiRequest({ path, ...args })
  }
}

// Add basic auth headers to API request based on redux store
function addAuthHeaders(apiRequest: SuperAgentRequest): SuperAgentRequest {
  if (USE_AUTH) {
    const token = localStorage.getItem('id_token')
    if (token) {
      return apiRequest.set('Authorization', `Bearer ${token}`)
    }
  }
  return apiRequest
}

export const Api = new ApiClient({ baseUrl: MEMBERSHIP_API_URL })
