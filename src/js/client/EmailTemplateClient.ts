import { Api, ApiClient } from '../client/ApiClient'
import { FromJS } from '../util/typedMap'
import { List } from 'immutable'

export interface EmailTemplate extends EmailTemplateForm {
  id: number
  last_updated: string
}
export type ImEmailTemplate = FromJS<EmailTemplate>

export interface EmailTemplateById {
  [templateId: string]: EmailTemplate
}
export type ImEmailTemplateById = FromJS<EmailTemplateById>

interface EmailTemplateForm {
  id?: number
  name: string
  topic_id: number
  subject: string
  body: string
}
export type ImEmailTemplateForm = FromJS<EmailTemplateForm>

type EmailTemplateListResponse = EmailTemplate[]
export type ImEmailTemplateListResponse = FromJS<EmailTemplateListResponse>

export class EmailTemplateClient {
  api: ApiClient

  constructor(api: ApiClient) {
    this.api = api
  }

  async all(): Promise<ImEmailTemplateListResponse> {
    const response = await this.api
      .url('/email_templates')
      .get()
      .executeOr<ImEmailTemplateListResponse>(List())

    return response
  }

  async template(id: number): Promise<ImEmailTemplate | null> {
    const response = await this.api
      .url(`/email_template/${id}`)
      .get()
      .execute<ImEmailTemplate>()

    if (response != null) {
      return response
    } else {
      return null
    }
  }

  async save(template: ImEmailTemplateForm): Promise<ImEmailTemplate> {
    const templateId = template.get('id') || null

    const req =
      templateId != null
        ? this.api.url(`/email_template/${templateId}`, { method: 'PUT' })
        : this.api.url(`/email_template`, { method: 'POST' })

    const response = await req
      .copy({ body: template.toJS() })
      .execute<ImEmailTemplate>()

    if (response != null) {
      return response
    } else {
      throw new Error('Got empty response when saving email template')
    }
  }
}

export const EmailTemplates = new EmailTemplateClient(Api)
