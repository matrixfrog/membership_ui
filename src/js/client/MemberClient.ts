import { Api, ApiClient, ApiStatus, ImApiStatus } from '../client/ApiClient'
import { Map, fromJS, List } from 'immutable'
import { FromJS } from '../util/typedMap'
import { TODO } from '../typeMigrationShims'
import { ElectionStatus } from './ElectionClient'
import { Brand } from 'src/js/util/typeUtils'

export interface MemberInfo {
  first_name: string
  last_name: string
  biography: string
  pronouns?: MemberPronounBundle
  email_address?: string
  additional_email_addresses?: MemberEmailAddress[]
  phone_numbers?: MemberPhoneNumber[]
}
export type ImMemberInfo = FromJS<MemberInfo>

export type MemberPronounBundle = Brand<string, 'member-pronoun-bundle'>
export type MemberPronouns = Brand<string, 'member-pronouns'>

export interface MemberEmailAddress {
  email_address: string
  name: string
  preferred: boolean
  verified: boolean
}
export type ImMemberEmailAddress = FromJS<MemberEmailAddress>
export type ImMemberEmailAddressList = FromJS<MemberEmailAddress[]>

export interface MemberPhoneNumber {
  phone_number: string
  name: string
}
export type ImMemberPhoneNumber = FromJS<MemberPhoneNumber>
export type ImMemberPhoneNumberList = FromJS<MemberPhoneNumber[]>

interface MemberRole {
  role: string
  committee_id: number
  committee_name: string

  /**
   * @deprecated
   */
  committee: string
  date_created: Date
}
export type ImMemberRole = FromJS<MemberRole>

interface MemberBasics {
  id: number
  info: MemberInfo
  roles: MemberRole[]
}

interface MemberAttendance {
  meeting_id: number
  name: string
}

interface MemberVote {
  election_id: number
  election_name: string
  election_status: ElectionStatus
  voted: boolean
}
export type ImMemberVote = FromJS<MemberVote>
export type ImMemberVoteList = FromJS<MemberVote[]>

interface FullMembership extends Membership {
  active: boolean
  ak_id: string
  do_not_call: boolean
  first_name: string
  middle_name: string
  last_name: string
  city: string
  zipcode: string
  join_date: Date
}

interface Membership {
  status: string
  address: string
  phone_numbers: string[]
  dues_paid_until: Date
  dues_paid_overridden_on: Date | null
}
export type ImMembership = FromJS<Membership>

export interface MemberDetailed extends MemberBasics {
  do_not_call: boolean
  do_not_email: boolean
  is_eligible: boolean
  dues_are_paid: boolean
  meets_attendance_criteria: boolean
  active_in_committee: boolean
  is_member: boolean
  meetings: MemberAttendance[]
  votes: MemberVote[]
  notes: string
  membership?: Membership
}
export type ImMemberDetailed = FromJS<MemberDetailed>

export interface MemberListEntry {
  id: number
  name: string
  membership: FullMembership
}

export interface EligibleMemberListEntry extends MemberListEntry {
  eligibility: EligibilityAttributes
  email: string
}
export type ImEligibleMemberListEntry = FromJS<EligibleMemberListEntry>

export type EligibleMemberList = EligibleMemberListEntry[]
export type ImEligibleMemberList = FromJS<EligibleMemberList>

interface EligibilityAttributes {
  is_eligible: boolean
  message: string
  meets_attendance_criteria: boolean
  active_in_committee: boolean
  num_votes: number
}

export interface RosterResults {
  members_created: number
  members_updated: number
  memberships_created: number
  memberships_updated: number
  member_roles_added: number
  identities_created: number
  phone_numbers_created: number
  errors: number
  processed: number
  total: number
}
export type ImRosterResults = FromJS<RosterResults>

interface ImportResponse extends ApiStatus {
  data: RosterResults
}
type ImImportResponse = FromJS<ImportResponse>

export interface CreateMemberForm {
  first_name: string
  last_name: string
  email_address: string
  give_chapter_member_role?: boolean
}
export type ImCreateMemberForm = FromJS<CreateMemberForm>

interface CreateMemberResponse extends ApiStatus {
  data: {
    member: MemberBasics
    verify_url: string
    email_sent: boolean
    role_created: boolean
  }
}
type ImCreateMemberResponse = FromJS<CreateMemberResponse>

export interface UpgradeMemberResponse extends ApiStatus {
  data: UpgradeMemberResponseData
}
export interface UpgradeMemberResponseData {
  auth0_account_created: boolean
  email_sent: boolean
  role_created: boolean
}
export type ImUpgradeMemberResponse = FromJS<UpgradeMemberResponse>

interface UpdateMemberResponse extends ApiStatus {
  data: MemberDetailed
}
type ImUpdateMemberResponse = FromJS<UpdateMemberResponse>

export interface MemberSearchResponse {
  members: EligibleMemberList
  cursor: number
  has_more: boolean
}
export type ImMemberSearchResponse = FromJS<MemberSearchResponse>

export interface MemberNotes {
  notes: string
}
export type ImMemberNotes = FromJS<MemberNotes>

/**
 * TODO: This is a very limited subset based on what the backend allows us to update at the moment
 */
export type UpdateMemberAttributeKeys = 'do_not_call' | 'do_not_email'
export type UpdateMemberAttributes = Pick<
  MemberDetailed,
  UpdateMemberAttributeKeys
>

export interface AdminUpdateMemberAttributes {
  first_name: string
  last_name: string
  pronouns: MemberPronounBundle
  biography: string
  do_not_call: boolean
  do_not_email: boolean
  dues_paid_overridden_on: Date
}

export class MemberClient {
  api: ApiClient

  constructor(api: ApiClient) {
    this.api = api
  }

  async getCurrentUser(): Promise<ImMemberDetailed | null> {
    const response = await this.api
      .url(`/member/details`)
      .get()
      .execute<ImMemberDetailed>()

    if (response != null) {
      return response
    } else {
      return null
    }
  }

  format_member_details_response(
    response: ImMemberDetailed
  ): ImMemberDetailed | null {
    const membership = response.get('membership') as ImMembership | undefined
    let cleaned_membership = membership?.set(
      'dues_paid_until',
      new Date(membership?.get('dues_paid_until'))
    )

    let overridden_on = membership?.get('dues_paid_overridden_on')
    if (overridden_on != null && cleaned_membership != null) {
      overridden_on = new Date(overridden_on)
      cleaned_membership = cleaned_membership.set(
        'dues_paid_overridden_on',
        overridden_on
      )
    }

    return response.set('membership', cleaned_membership)
  }

  async details(memberId: string): Promise<ImMemberDetailed | null> {
    const response = await this.api
      .url(`/admin/member/details`)
      .query({ member_id: memberId })
      .get()
      .execute<ImMemberDetailed>()

    if (response != null) {
      return this.format_member_details_response(response)
    } else {
      return null
    }
  }

  async updateNotes(
    memberId: number,
    notes: string
  ): Promise<ImMemberNotes | null> {
    const updateBody: MemberNotes = { notes }

    const response = await this.api
      .url(`/admin/member/notes`)
      .query({ member_id: memberId })
      .put(updateBody)
      .execute<ImMemberNotes>()

    if (response != null) {
      return response
    } else {
      return null
    }
  }

  async addPhoneNumber(
    memberId: number,
    phoneNumber: Partial<MemberPhoneNumber>
  ): Promise<ImMemberPhoneNumberList | null> {
    const response = await this.api
      .url(`/admin/member/phone_numbers`)
      .query({ member_id: memberId })
      .post(phoneNumber)
      .execute<ImMemberPhoneNumberList>()

    if (response != null) {
      return response
    } else {
      return null
    }
  }

  async deletePhoneNumber(
    memberId: number,
    phoneNumber: string
  ): Promise<ImMemberPhoneNumberList | null> {
    const response = await this.api
      .url(`/admin/member/phone_numbers`)
      .query({ member_id: memberId })
      .delete({ phone_number: phoneNumber })
      .execute<ImMemberPhoneNumberList>()

    if (response != null) {
      return response
    } else {
      return null
    }
  }

  async addEmailAddress(
    memberId: number,
    emailAddress: Partial<MemberEmailAddress>
  ): Promise<ImMemberEmailAddressList | null> {
    const response = await this.api
      .url(`/admin/member/email_addresses`)
      .query({ member_id: memberId })
      .post(emailAddress)
      .execute<ImMemberEmailAddressList>()

    if (response != null) {
      return response
    } else {
      return null
    }
  }

  async deleteEmailAddress(
    memberId: number,
    emailAddress: string
  ): Promise<ImMemberEmailAddressList | null> {
    const response = await this.api
      .url(`/admin/member/email_addresses`)
      .query({ member_id: memberId })
      .delete({ email_address: emailAddress })
      .execute<ImMemberEmailAddressList>()

    if (response != null) {
      return response
    } else {
      return null
    }
  }

  async upgradeToMember(
    memberId: number
  ): Promise<ImUpgradeMemberResponse | null> {
    return await this.api
      .url(`/member/upgrade/${memberId}`)
      .put()
      .execute()
  }

  async suspendMember(memberId: number): Promise<void> {
    await this.api
      .url(`/member/suspend/${memberId}`)
      .post()
      .execute()
  }

  async exitMember(memberId: number): Promise<void> {
    await this.api
      .url(`/member/exit/${memberId}`)
      .post()
      .execute()
  }

  async cancelDuesOverride(memberId: number): Promise<void> {
    await this.api
      .url(`/member/downgrade/${memberId}`)
      .post()
      .execute()
  }

  async all(): Promise<ImEligibleMemberList> {
    const response = await this.api
      .url(`/member/list`)
      .get()
      .executeOr<ImEligibleMemberList>(List())

    return response
  }

  async importRoster(file: File): Promise<ImImportResponse> {
    const response = await this.api
      .url(`/import`)
      .put()
      .attach('file', file)
      .execute<ImImportResponse>()

    if (response != null) {
      return response
    } else {
      throw new Error('Got empty response when importing roster')
    }
  }

  async create(member: ImCreateMemberForm): Promise<ImCreateMemberResponse> {
    const response = await this.api
      .url(`/member`)
      .post(member)
      .execute<ImCreateMemberResponse>()

    if (response != null) {
      return response
    } else {
      throw new Error('Got empty response from server when creating member')
    }
  }

  async updateAdmin(
    memberId: number,
    attributes: Partial<AdminUpdateMemberAttributes>
  ): Promise<ImUpdateMemberResponse> {
    const response = await this.api
      .url(`/admin/member`)
      .query({ member_id: memberId })
      .patch(attributes)
      .execute<ImUpdateMemberResponse>()

    if (response != null) {
      return response
    } else {
      throw new Error('Got empty response from server when updating member')
    }
  }

  async update(
    // Only handle contact preference updates right now. Use MemberDetailed
    // or update the Pick property list when API supports more things
    attributes: Partial<UpdateMemberAttributes>
  ): Promise<ImUpdateMemberResponse> {
    const response = await this.api
      .url(`/member`)
      .patch(attributes)
      .execute<ImUpdateMemberResponse>()

    if (response != null) {
      return response
    } else {
      throw new Error('Got empty response from server when updating member')
    }
  }

  async search(
    query: string,
    pageSize = 10,
    cursor = 0
  ): Promise<ImMemberSearchResponse> {
    const response = await this.api
      .url(`/member/search`)
      .query({
        query: query,
        page_size: pageSize,
        cursor
      })
      .get()
      .execute<ImMemberSearchResponse>()

    if (response != null) {
      const responseWithFixedDates = response.set(
        'members',
        response.get('members').map(entry => {
          const joinDate = entry.getIn(['membership', 'join_date'])
          const duesPaidUntil = entry.getIn(['membership', 'dues_paid_until'])
          const dateImported = entry.getIn(['membership', '_date_imported'])
          return entry
            .setIn(
              ['membership', 'join_date'],
              joinDate != null ? new Date(joinDate) : null
            )
            .setIn(
              ['membership', 'dues_paid_until'],
              duesPaidUntil != null ? new Date(duesPaidUntil) : null
            )
            .setIn(
              ['membership', '_date_imported'],
              dateImported != null ? new Date(duesPaidUntil) : null
            )
        })
      )
      return responseWithFixedDates
    } else {
      throw new Error(
        'Got empty response from the server when performing a member search'
      )
    }
  }

  async addRole(
    memberId: number,
    role: string,
    committeeId: number | null
  ): Promise<ImApiStatus> {
    const response = await this.api
      .url(`/member/role`)
      .post({
        member_id: memberId,
        role: role,
        committee_id: committeeId
      })
      .execute<ImApiStatus>()

    if (response != null) {
      return response
    } else {
      throw new Error(
        'Got an empty response from the server when adding a role to a member'
      )
    }
  }

  async removeRole(
    memberId: number,
    role: string,
    committeeId: number | null
  ): Promise<ImApiStatus> {
    const response = await this.api
      .url(`/member/role`)
      .copy({
        body: fromJS({
          member_id: memberId,
          role: role,
          committee_id: committeeId
        })
      })
      .remove()
      .execute<ImApiStatus>()

    if (response != null) {
      return response
    } else {
      throw new Error(
        'Got an empty response from the server when removing a role from a member'
      )
    }
  }

  async merge(
    memberId: number,
    memberToRemoveId: number
  ): Promise<ImApiStatus> {
    const response = await this.api
      .url(`/member/${memberId}/merge`)
      .query({
        member_to_remove_id: memberToRemoveId,
        force: true
      })
      .post()
      .execute<ImApiStatus>()
    if (response != null) {
      return response
    } else {
      throw new Error(
        'Got an empty response from the server when merging members'
      )
    }
  }
}

export const Members = new MemberClient(Api)
