import { Api, ApiClient, ApiStatus, ImApiStatus } from '../client/ApiClient'
import { fromJS, List } from 'immutable'
import { FromJS } from '../util/typedMap'
import {
  EligibleMemberList,
  ImEligibleMemberList,
  MemberInfo
} from './MemberClient'

interface MeetingBase {
  id: number
  name: string
  committee_id: number | null
  code: number | null
  landing_url: string | null
  owner: string | null
  published: boolean
}

interface MeetingResponse extends MeetingBase {
  start_time: string | null
  end_time: string | null
}
type ImMeetingResponse = FromJS<MeetingResponse>
type ImMeetingResponseList = FromJS<MeetingResponse[]>

export interface Meeting extends MeetingBase {
  start_time: Date | null
  end_time: Date | null
}
export type ImMeeting = FromJS<Meeting>
export type ImMeetingsList = FromJS<Meeting[]>
export type ImPartialMeeting = FromJS<Partial<Meeting>>

export interface MeetingsById {
  [id: number]: Meeting
}
export type ImMeetingsById = FromJS<MeetingsById>

interface UpdateMeetingResponse extends ApiStatus {
  meeting: MeetingResponse
}
export type ImUpdateMeetingResponse = FromJS<UpdateMeetingResponse>

export interface UpdateAttendeeAttributes {
  update_eligibility_to_vote: boolean
}

export type ProxyTokenState = 'UNCLAIMED' | 'ACCEPTED' | 'REJECTED'
export type ProxyTokenAction = 'accept' | 'reject'
interface CreateProxyTokenResponse {
  proxy_token_id: number
  state: ProxyTokenState
}
export type ImCreateProxyTokenResponse = FromJS<CreateProxyTokenResponse>

interface ProxyTokenResponseShared {
  proxy_token_id: number
  nominator_id: number
  nominator_name: string
  state: ProxyTokenState
}

interface ProxyTokenResponse extends ProxyTokenResponseShared {
  recently_acted_member_id: number
}
export type ImProxyTokenResponse = FromJS<ProxyTokenResponse>

interface ProxyTokenActionResponse extends ProxyTokenResponseShared {
  email_sent: boolean
}
export type ImProxyTokenActionResponse = FromJS<ProxyTokenActionResponse>

export interface VerifyTokenResponse {
  status: string
  member: MemberInfo
}

export type MeetingInvitationStatus = 'NO_RESPONSE' | 'ACCEPTED' | 'DECLINED'

interface MeetingInvitation {
  id: number
  meeting_id: number
  status: MeetingInvitationStatus
  created_at: string
  name: string
  email: string
}
export type ImMeetingInvitation = FromJS<MeetingInvitation>

interface MeetingInvitationResponse {
  status: string
  invitation: MeetingInvitation
}
export type ImMeetingInvitationResponse = FromJS<MeetingInvitationResponse>

interface MeetingInvitationBulkInviteCommitteeMembersResponse {
  status: string
  invitations: Array<MeetingInvitation>
}
export type ImMeetingInvitationBulkInviteCommitteeMembersResponse = FromJS<
  MeetingInvitationBulkInviteCommitteeMembersResponse
>

interface MeetingAgenda {
  id: number
  meeting_id: number
  text: string
  updated_at: string
}
export type ImMeetingAgenda = FromJS<MeetingAgenda>
type ImMeetingAgendaResponse = ImMeetingAgenda

interface MeetingAgendaEditResponse {
  status: string
  agenda: MeetingAgenda
}
type ImMeetingAgendaEditResponse = FromJS<MeetingAgendaEditResponse>

export class MeetingClient {
  api: ApiClient

  constructor(api: ApiClient) {
    this.api = api
  }

  isValidMeetingCode(code: string): boolean {
    let codeInt
    try {
      codeInt = parseInt(code, 10)
    } catch (e) {
      codeInt = 0
    }
    return codeInt >= 1000 && codeInt <= 9999
  }

  sanitizeMeetingCode(code: string): number | null {
    if (code.length == 0) {
      return null
    } else {
      return parseInt(code)
    }
  }

  async create(meeting: ImMeeting): Promise<ImUpdateMeetingResponse> {
    const startTime = meeting.get('start_time')
    const endTime = meeting.get('end_time')
    const startTimeIsoString =
      startTime != null ? startTime.toISOString() : null
    const endTimeIsoString = endTime != null ? endTime.toISOString() : null
    const committeeId = meeting.get('committee_id') || null
    const landingUrl = meeting.get('landing_url') || null

    const response = await this.api
      .url(`/meeting`)
      .post({
        name: meeting.get('name'),
        committee_id: committeeId,
        landing_url: landingUrl,
        start_time: startTimeIsoString,
        end_time: endTimeIsoString,
        published: meeting.get('published')
      })
      .execute<ImUpdateMeetingResponse>()

    if (response != null) {
      return response
    } else {
      throw new Error('Got empty response when creating meeting')
    }
  }

  async all(): Promise<ImMeetingsList> {
    const response = await this.api
      .url(`/meeting/list`)
      .get()
      .executeOr<ImMeetingResponseList>(List())

    return response.map(meeting => this.parseMeetingDates(meeting))
  }

  async get(meetingId: number): Promise<ImMeeting | null> {
    const response = await this.api
      .url(`/meetings/${meetingId}`)
      .get()
      .execute<ImMeetingResponse>()

    if (!response) return null

    return this.parseMeetingDates(response)
  }

  async getAttendees(meetingId: number): Promise<ImEligibleMemberList> {
    const response = await this.api
      .url(`/meetings/${meetingId}/attendees`)
      .get()
      .executeOr<ImEligibleMemberList>(List())

    return response
  }

  async addAttendee(meetingId: number, memberId: number): Promise<ImApiStatus> {
    const response = await this.api
      .url(`/member/attendee`)
      .post({ meeting_id: meetingId, member_id: memberId })
      .execute<ImApiStatus>()

    if (response != null) {
      return response
    } else {
      throw new Error('Got empty response from server when adding attendee')
    }
  }

  async updateAttendee(
    meetingId: number,
    memberId: number,
    attributes: UpdateAttendeeAttributes
  ): Promise<ImApiStatus> {
    const response = await this.api
      .url(`/meetings/${meetingId}/attendees/${memberId}`)
      .patch(attributes)
      .execute<ImApiStatus>()

    if (response != null) {
      return response
    } else {
      throw new Error(
        'Got empty response from server when updating attendee voting status'
      )
    }
  }

  async removeAttendee(
    meetingId: number,
    memberId: number
  ): Promise<ImApiStatus> {
    const response = await this.api
      .url(`/meetings/${meetingId}/attendees/${memberId}`)
      .remove()
      .execute<ImApiStatus>()

    if (response != null) {
      return response
    } else {
      throw new Error('Got empty response from server when removing attendee')
    }
  }

  async autogenerateMeetingCode(
    meetingId: number
  ): Promise<ImUpdateMeetingResponse> {
    if (meetingId <= 0) {
      throw new Error(`Invalid meeting_id: ${meetingId}`)
    }

    const response = await this.api
      .url(`/meeting`)
      .patch({
        meeting_id: meetingId,
        code: 'autogenerate'
      })
      .execute<ImUpdateMeetingResponse>()

    if (response != null) {
      return response
    } else {
      throw new Error(
        'Got empty response from server when autogenerating meeting code'
      )
    }
  }

  async setMeetingCode(
    meetingId: number,
    code: string
  ): Promise<ImUpdateMeetingResponse> {
    if (meetingId <= 0) {
      throw new Error(`Invalid meeting_id: ${meetingId}`)
    }

    if (code.length > 0 && !this.isValidMeetingCode(code)) {
      throw new Error(`Invalid meeting code: ${code}`)
    }

    const validatedCode = this.sanitizeMeetingCode(code)

    const response = await this.api
      .url(`/meeting`)
      .patch({
        meeting_id: meetingId,
        code: validatedCode
      })
      .execute<ImUpdateMeetingResponse>()

    if (response != null) {
      return response
    } else {
      throw new Error(
        'Got empty response from server when setting meeting code'
      )
    }
  }

  parseMeetingDates(imResponse: ImMeetingResponse): ImMeeting {
    // Conversion to accept new date type conversion below
    let meeting: ImMeeting = (imResponse as any) as ImMeeting

    // parse received ISO 8601 strings
    const startTimeString = meeting.get('start_time')
    if (startTimeString != null) {
      meeting = meeting.set('start_time', new Date(startTimeString))
    }

    const endTimeString = meeting.get('end_time')
    if (endTimeString != null) {
      meeting = meeting.set('end_time', new Date(endTimeString))
    }

    return meeting
  }

  async updateMeeting(
    meetingId: number,
    attributes: ImPartialMeeting
  ): Promise<ImMeeting> {
    if (meetingId <= 0) {
      throw new Error(`Invalid meeting_id: ${meetingId}`)
    }
    const payload = {
      ...attributes.toJSON(),
      id: meetingId,
      meeting_id: meetingId
    }

    const response = await this.api
      .url(`/meeting`)
      .patch(payload)
      .execute<ImUpdateMeetingResponse>()

    if (response != null) {
      return this.parseMeetingDates(response.get('meeting'))
    } else {
      throw new Error('Got empty response from server when updating meeting')
    }
  }

  async createProxyToken(
    meetingId: number
  ): Promise<ImCreateProxyTokenResponse> {
    if (!meetingId) {
      throw new Error(`Invalid meeting_id: ${meetingId}`)
    }

    const response = await this.api
      .url(`/meetings/${meetingId}/proxy-token`)
      .post()
      .execute<ImCreateProxyTokenResponse>()

    if (response != null) {
      return response
    } else {
      throw new Error(
        'Got empty response from server when creating proxy token'
      )
    }
  }

  async getProxyTokenState(
    meetingId: number,
    proxyTokenId: number
  ): Promise<ImProxyTokenResponse | null> {
    if (meetingId <= 0) {
      throw new Error(`Invalid meeting_id: ${meetingId}`)
    }
    if (proxyTokenId <= 0) {
      throw new Error(`Invalid proxyTokenId: ${proxyTokenId}`)
    }

    const response = await this.api
      .url(
        `/meetings/${meetingId}/proxy-token/${encodeURIComponent(proxyTokenId)}`
      )
      .get()
      .execute<ImProxyTokenResponse>()

    if (response != null) {
      return response
    } else {
      return null
    }
  }

  async acceptOrRejectProxyToken(
    meetingId: number,
    proxyTokenId: number,
    action: ProxyTokenAction
  ): Promise<ImProxyTokenActionResponse> {
    if (meetingId <= 0) {
      throw new Error(`Invalid meetingId: ${meetingId}`)
    }

    if (proxyTokenId <= 0) {
      throw new Error(`Invalid proxyTokenId: ${proxyTokenId}`)
    }

    const response = await this.api
      .url(
        `/meetings/${meetingId}/proxy-token/${encodeURIComponent(
          proxyTokenId
        )}/${action}`
      )
      .post()
      .execute<ImProxyTokenActionResponse>()

    if (response != null) {
      return response
    } else {
      throw new Error(
        'Got empty response from server when taking action on proxy token'
      )
    }
  }

  async verifyToken(token: string): Promise<VerifyTokenResponse | null> {
    if (token === '') {
      throw new Error('Token must not be blank')
    }

    const response = await this.api
      .url(`/verify-email`)
      .post({ token })
      .executeRaw<VerifyTokenResponse>()

    return response
  }

  async getInvitatitons(
    meetingId: number
  ): Promise<List<ImMeetingInvitation> | null> {
    if (meetingId <= 0) {
      throw new Error(`Invalid meeting id: ${meetingId}`)
    }

    const response = await this.api
      .url(`/meetings/${meetingId}/invitations`)
      .get()
      .execute<List<ImMeetingInvitation>>()

    if (response != null) {
      return response
    } else {
      return null
    }
  }

  async sendInvitation(
    meetingId: number,
    memberId: number
  ): Promise<ImMeetingInvitation | null> {
    if (meetingId <= 0) {
      throw new Error(`Invalid meeting id: ${meetingId}`)
    }
    if (memberId <= 0) {
      throw new Error(`Invalid member id: ${memberId}`)
    }

    const response = await this.api
      .url(`/meetings/${meetingId}/invitation`)
      .post({ member_id: memberId })
      .execute<ImMeetingInvitationResponse>()

    if (response != null && response.get('status') === 'success') {
      return response.get('invitation')
    } else {
      return null
    }
  }

  async bulkInviteCommitteeMembers(
    meetingId: number
  ): Promise<List<ImMeetingInvitation> | null> {
    if (meetingId <= 0) {
      throw new Error(`Invalid meeting id: ${meetingId}`)
    }

    const response = await this.api
      .url(`/meetings/${meetingId}/invite-all-committee-members`)
      .post()
      .execute<ImMeetingInvitationBulkInviteCommitteeMembersResponse>()

    if (response != null && response.get('status') === 'success') {
      return response.get('invitations')
    } else {
      return null
    }
  }

  async getInvitation(meetingId: number): Promise<ImMeetingInvitation | null> {
    if (meetingId <= 0) {
      throw new Error(`Invalid meeting_id: ${meetingId}`)
    }

    const response = await this.api
      .url(`/meetings/${meetingId}/invitation`)
      .get()
      .execute<ImMeetingInvitation>()

    if (response != null) {
      return response
    } else {
      return null
    }
  }

  async sendRsvp(
    meetingId: number,
    canAttend: boolean
  ): Promise<ImMeetingInvitation | null> {
    if (meetingId <= 0) {
      throw new Error(`Invalid meeting id: ${meetingId}`)
    }

    const rsvpString: MeetingInvitationStatus = canAttend
      ? 'ACCEPTED'
      : 'DECLINED'

    const response = await this.api
      .url(`/meetings/${meetingId}/invitation/${rsvpString}`)
      .post()
      .execute<ImMeetingInvitationResponse>()

    if (response != null && response.get('status') === 'success') {
      return response.get('invitation')
    } else {
      return null
    }
  }

  async getMyInvitations(): Promise<List<ImMeetingInvitation> | null> {
    const response = await this.api
      .url('/invitations')
      .get()
      .execute<List<ImMeetingInvitation>>()

    if (response != null) {
      return response
    } else {
      return null
    }
  }

  async getAgenda(meetingId: number): Promise<ImMeetingAgenda | null> {
    if (meetingId <= 0) {
      throw new Error(`Invalid meetingId: ${meetingId}`)
    }

    const response = await this.api
      .url(`/meetings/${meetingId}/agenda`)
      .get()
      .execute<ImMeetingAgendaResponse>()

    if (response != null) {
      return response
    } else {
      throw new Error(`Error loading meeting agenda ${meetingId}`)
    }
  }

  async editAgenda(
    meetingId: number,
    text: string,
    checkoutTimestamp: string
  ): Promise<ImMeetingAgenda | null> {
    if (meetingId <= 0) {
      throw new Error(`Invalid meetingId: ${meetingId}`)
    }

    const response = await this.api
      .url(`/meetings/${meetingId}/agenda`)
      .post({ text: text, checkout_timestamp: checkoutTimestamp })
      .execute<ImMeetingAgendaEditResponse>()

    if (response != null && response.get('status') === 'success') {
      return response.get('agenda')
    } else {
      throw new Error(`Error editing meeting agenda ${meetingId}`)
    }
  }
}

export const Meetings = new MeetingClient(Api)

export default Meetings
