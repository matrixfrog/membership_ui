import { createStore, applyMiddleware, compose, Store } from 'redux'
import thunk from 'redux-thunk'
import { browserHistory } from 'react-router'
import { routerMiddleware } from 'react-router-redux'

import rootReducer, { RootReducer } from '../reducers/rootReducer'

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const enhancer = composeEnhancers(
  applyMiddleware(thunk, routerMiddleware(browserHistory))
)

export default function configureStore(initialState?): Store<RootReducer> {
  const store = createStore(rootReducer, initialState, enhancer)
  return store
}
