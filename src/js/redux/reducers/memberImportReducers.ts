import { IMPORT_MEMBERS } from '../constants/actionTypes'
import { Map, fromJS } from 'immutable'
import { AnyAction } from 'redux'
import { Reducer } from 'react'
import { ImRosterResults } from 'src/js/client/MemberClient'
import { FromJS } from 'src/js/util/typedMap'

interface MemberImportState {
  state: 'initiated' | 'success' | 'failure' | null
  results?: ImRosterResults
}

export type ImMemberImportState = FromJS<MemberImportState>
export type MemberImportAction = AnyAction

export const INITIAL_STATE: ImMemberImportState = fromJS<MemberImportState>({
  state: null
})

const memberImport: Reducer<ImMemberImportState, MemberImportAction> = (
  state = INITIAL_STATE,
  action
) => {
  switch (action.type) {
    case IMPORT_MEMBERS.SUBMIT:
      return fromJS<MemberImportState>({ state: 'initiated' })
    case IMPORT_MEMBERS.SUCCESS:
      return fromJS<MemberImportState>({
        state: 'success',
        results: action.payload
      })
    case IMPORT_MEMBERS.FAILED:
      return fromJS<MemberImportState>({ state: 'failure' })
    default:
      return state
  }
}

export default memberImport
