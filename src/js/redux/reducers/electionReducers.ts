import { ELECTIONS } from '../constants/actionTypes'
import { fromJS, Map } from 'immutable'
import { Reducer } from 'react'
import { TODOAction } from '../../typeMigrationShims'
import {
  ImElectionsList,
  ImElectionResponse,
  ImElectionDetailsResponse
} from 'src/js/client/ElectionClient'
import { FromJS } from 'src/js/util/typedMap'

interface ElectionById {
  [id: number]: ImElectionResponse
}
export type ImElectionById = FromJS<ElectionById>

interface ElectionsState {
  byId: ElectionById
}
export type ImElectionsState = FromJS<ElectionsState>
export type ElectionsAction = TODOAction

const INITIAL_STATE: ImElectionsState = fromJS({
  byId: {}
})

const elections: Reducer<ImElectionsState, ElectionsAction> = (
  state = INITIAL_STATE,
  action
) => {
  switch (action.type) {
    case ELECTIONS.FETCH_ELECTIONS: {
      const payload: ImElectionsList | null = action.payload

      if (payload != null) {
        const payloadById: Iterable<[
          number,
          ImElectionResponse
        ]> = payload.map(election => [election.get('id'), election])
        const elections = Map(payloadById) as ImElectionById
        return state.mergeDeepIn(['byId'], elections)
      } else {
        return state
      }
    }
    case ELECTIONS.FETCH_ELECTION:
      const payload = action.payload as ImElectionDetailsResponse | null
      if (payload != null) {
        const electionId = payload.get('id')
        return state.mergeDeepIn(['byId', electionId], action.payload)
      } else {
        return state
      }
    default:
      return state
  }
}

export default elections
