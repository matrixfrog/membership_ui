import { combineReducers, Reducer } from 'redux'
import { routerReducer as routing } from 'react-router-redux'

import auth from './auth'
import assets from './assetReducers'
import { Api } from '../../client/ApiClient'
import committees from './committeeReducers'
import elections from './electionReducers'
import member from './memberReducers'
import memberImport from './memberImportReducers'
import meetings from './meetingReducers'
import templates from './emailTemplates'
import topics from './emailTopics'
import emailAddresses from './emailAddressReducers'
import emails from './emailRuleReducers'
import universalAuth from './universalAuthReducer'
import callback from './callback'
import memberSearch from './memberSearchReducer'

const reducers = {
  universalAuth,
  callback,
  assets,
  auth,
  client: () => Api,
  committees,
  elections,
  emails,
  templates,
  member,
  memberImport,
  meetings,
  topics,
  emailAddresses,
  memberSearch,
  routing
}

type Reducers = { [key: string]: Reducer<any> }
type ReducersReturnType<O extends Reducers> = {
  [K in keyof O]: ReturnType<O[K]>
}

export type RootReducer = ReducersReturnType<typeof reducers>

const rootReducer = combineReducers<RootReducer>(reducers)

export default rootReducer
