import { MEETINGS } from '../constants/actionTypes'
import { fromJS, List, Map } from 'immutable'
import { AnyAction } from 'redux'
import { Reducer } from 'react'
import { ImMeetingsList, MeetingsById } from 'src/js/client/MeetingClient'
import { ResolvedAsset } from 'src/js/client/AssetClient'
import { FromJS } from 'src/js/util/typedMap'

interface MeetingsState {
  byId: MeetingsById
  form: {
    code: {
      inSubmission: boolean
    }
    create: {
      inSubmission: boolean
    }
    upload: {
      asset: ResolvedAsset | null
      inSubmission: boolean
    }
  }
}

export type ImMeetingsState = FromJS<MeetingsState>
export type MeetingsAction = AnyAction

const INITIAL_STATE: ImMeetingsState = fromJS<MeetingsState>({
  byId: {},
  form: {
    code: {
      inSubmission: false
    },
    create: {
      inSubmission: false
    },
    upload: {
      asset: null,
      inSubmission: false
    }
  }
})

const meetings: Reducer<ImMeetingsState, MeetingsAction> = (
  state = INITIAL_STATE,
  action
) => {
  switch (action.type) {
    case MEETINGS.CREATE.SUBMIT:
      return state.setIn(['form', 'create', 'inSubmission'], true)
    case MEETINGS.CREATE.SUCCESS:
      return updateMeetingsById(state, List.of(action.payload)).setIn(
        ['form', 'create'],
        INITIAL_STATE.getIn(['form', 'create'])
      )
    case MEETINGS.CREATE.FAILED:
      return state.setIn(['form', 'create', 'inSubmission'], false)
    case MEETINGS.UPDATE_LIST:
      return updateMeetingsById(state, action.payload)
    case MEETINGS.CODE.SUBMIT:
      return state.setIn(['form', 'code', 'inSubmission'], true)
    case MEETINGS.CODE.SUCCESS:
      const { code, meetingId } = action.payload
      return setMeetingCode(state, meetingId, code)
    case MEETINGS.CODE.FAILED:
      return state.setIn(['form', 'code', 'inSubmission'], false)
    case MEETINGS.ADD_ASSET.SUBMIT:
      return state.setIn(['form', 'upload', 'inSubmission'], true)
    case MEETINGS.ADD_ASSET.SUCCESS:
      return state.setIn(
        ['form', 'upload'],
        Map({
          asset: null,
          inSubmission: false
        })
      )
    case MEETINGS.ADD_ASSET.FAILED:
      return state.setIn(['form', 'upload', 'inSubmission'], false)
    default:
      return state
  }
}

function updateMeetingsById(state: ImMeetingsState, meetings: ImMeetingsList) {
  const meetingsWithCodeClaimed = meetings
    .toMap()
    .mapEntries(([_, m]) => [m.get('id'), m])
  return state.mergeDeepIn(['byId'], meetingsWithCodeClaimed)
}

function setMeetingCode(state: ImMeetingsState, meetingId, code) {
  const currentMeeting = state.getIn(['byId', meetingId]).set('code', code)
  return updateMeetingsById(state, List.of(currentMeeting))
}

export default meetings
