import { fromJS, Map } from 'immutable'
import * as types from './../constants/actionTypes'
import { Reducer } from 'react'
import { AnyAction } from 'redux'
import { FromJS } from 'src/js/util/typedMap'
import { EmailById } from 'src/js/client/EmailAddressClient'

interface EmailAddressesState {
  byId: EmailById
}

export type ImEmailAddressesState = FromJS<EmailAddressesState>
export type EmailAddressesAction = AnyAction

const INITIAL_STATE: ImEmailAddressesState = fromJS({
  byId: {}
})

const emailAddresses: Reducer<ImEmailAddressesState, EmailAddressesAction> = (
  state = INITIAL_STATE,
  action
) => {
  switch (action.type) {
    case types.FETCH_EMAIL_ADDRESSES:
      return state.set(
        'byId',
        action.payload
          .toMap()
          .mapEntries(([_, address]) => [address.get('id'), address])
      )
    default:
      return state
  }
}

export default emailAddresses
