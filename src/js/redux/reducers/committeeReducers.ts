import { fromJS, Map } from 'immutable'
import { COMMITTEES } from '../constants/actionTypes'
import { showNotification } from '../../util/util'
import { AnyAction } from 'redux'
import { TODO } from '../../typeMigrationShims'
import { Reducer } from 'react'
import { FromJS } from 'src/js/util/typedMap'
import {
  CommitteesById,
  ImCommitteesById,
  ImCommitteeDetailed
} from 'src/js/client/CommitteeClient'

interface Committee {
  id: string
  name: string
  inactive: boolean
}

export type ImCommittee = FromJS<Committee>

interface CommitteesState {
  byId: CommitteesById
  form: {
    create: {
      name: string
      inSubmission: boolean
    }
  }
}
export type ImCommitteesState = FromJS<CommitteesState>
export type CommitteesAction = AnyAction

const INITIAL_STATE: ImCommitteesState = fromJS<CommitteesState>({
  byId: {},
  form: {
    create: {
      name: '',
      inSubmission: false
    }
  }
})

const committees: Reducer<ImCommitteesState, CommitteesAction> = (
  state = INITIAL_STATE,
  action
) => {
  switch (action.type) {
    case COMMITTEES.UPDATE_LIST:
      return addCommitteesById(state, action.payload)
    case COMMITTEES.FETCH_COMMITTEE:
      return state.setIn(['byId', action.payload.get('id')], action.payload)
    case COMMITTEES.CREATE.SUBMIT:
      return state.setIn(['form', 'create', 'inSubmission'], true)
    case COMMITTEES.CREATE.SUCCESS:
      const committeeId: number | null = action.payload.get('id')
      if (committeeId == null) {
        throw new Error(`Missing committee.id from ${action.payload.toJS()}`)
      }
      return addCommitteesById(
        state,
        Map<number, ImCommitteeDetailed>([[committeeId, action.payload]])
      ).setIn(['form', 'create', 'inSubmission'], false)
    case COMMITTEES.CREATE.FAILED:
      return state.setIn(['form', 'create', 'inSubmission'], false)
    case COMMITTEES.REQUESTING_COMMITTEE_ACCESS:
      showNotification('Sent!', 'Committee request sent.')
    default:
      return state
  }
}

function addCommitteesById(
  state: ImCommitteesState,
  committees: ImCommitteesById
) {
  const meetingsWithCodeClaimed = committees
    .toMap()
    .mapEntries(([_, m]) => [m.get('id'), m])
  return state.mergeDeepIn(['byId'], meetingsWithCodeClaimed)
}

export default committees
