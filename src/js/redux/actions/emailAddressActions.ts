import * as types from './../constants/actionTypes'
import { EmailAddresses } from '../../client/EmailAddressClient'
import { TODOAsyncThunkAction } from 'src/js/typeMigrationShims'

export function fetchEmailAddresses(): TODOAsyncThunkAction {
  return async dispatch => {
    const addresses = await EmailAddresses.all()
    dispatch({
      type: types.FETCH_EMAIL_ADDRESSES,
      payload: addresses
    })
  }
}
