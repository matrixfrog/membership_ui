import {
  CommitteeClient,
  CreateCommitteeProps
} from '../../client/CommitteeClient'
import { Members } from '../../client/MemberClient'
import { COMMITTEES } from '../constants/actionTypes'
import { ThunkAction } from 'redux-thunk'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { TODOAction, TODOAsyncThunkAction } from 'src/js/typeMigrationShims'

// thunk that requests data and handles success and error cases
export function createCommittee(
  committee: CreateCommitteeProps
): TODOAsyncThunkAction {
  return async (dispatch, getState) => {
    dispatch({
      type: COMMITTEES.CREATE.SUBMIT,
      payload: committee
    })
    const result = await new CommitteeClient(getState().client).create(
      committee
    )
    if (result.get('status') === 'success') {
      const created = result.get('created')

      dispatch({
        type: COMMITTEES.CREATE.SUCCESS,
        payload: created
      })
    } else {
      dispatch({
        type: COMMITTEES.CREATE.FAILED
      })
    }
  }
}

export function fetchCommittees(): TODOAsyncThunkAction {
  return async (dispatch, getState) => {
    const committees = await new CommitteeClient(getState().client).all()
    dispatch({
      type: COMMITTEES.UPDATE_LIST,
      payload: committees
    })
  }
}

export function fetchCommittee(committeeId: number): TODOAsyncThunkAction {
  return async (dispatch, getState) => {
    const committee = await new CommitteeClient(getState().client).get(
      committeeId
    )
    dispatch({
      type: COMMITTEES.FETCH_COMMITTEE,
      payload: committee
    })
  }
}

export function addAdmin(
  memberId: number,
  committeeId: number
): TODOAsyncThunkAction {
  return async (dispatch, getState) => {
    await Members.addRole(memberId, 'admin', committeeId)
    const committee = await new CommitteeClient(getState().client).get(
      committeeId
    )
    dispatch({
      type: COMMITTEES.FETCH_COMMITTEE,
      payload: committee
    })
  }
}

export function removeAdmin(
  memberId: number,
  committeeId: number
): TODOAsyncThunkAction {
  return async (dispatch, getState) => {
    await Members.removeRole(memberId, 'admin', committeeId)
    const committee = await new CommitteeClient(getState().client).get(
      committeeId
    )
    dispatch({
      type: COMMITTEES.FETCH_COMMITTEE,
      payload: committee
    })
  }
}

export function markMemberInactive(
  memberId: number,
  committeeId: number
): TODOAsyncThunkAction {
  return async (dispatch, getState) => {
    await Members.removeRole(memberId, 'active', committeeId)
    const committee = await new CommitteeClient(getState().client).get(
      committeeId
    )
    dispatch({
      type: COMMITTEES.FETCH_COMMITTEE,
      payload: committee
    })
  }
}

export function requestCommitteeMembership(
  committeeId: number
): TODOAsyncThunkAction {
  return async (dispatch, getState) => {
    await new CommitteeClient(getState().client).requestMembership(committeeId)
    dispatch({
      type: COMMITTEES.REQUESTING_COMMITTEE_ACCESS
    })
  }
}
