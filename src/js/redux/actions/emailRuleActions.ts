import { EMAIL } from './../constants/actionTypes'
import { EmailRules } from '../../client/EmailRuleClient'
import { ThunkAction } from 'redux-thunk'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import {
  TODOAsyncThunkAction,
  TODOThunkAction,
  TODO
} from 'src/js/typeMigrationShims'
import { ImEmail } from 'src/js/client/EmailAddressClient'

type EmailId = number | 'new'

export function fetchEmailRules(): TODOAsyncThunkAction {
  return async dispatch => {
    const emails = await EmailRules.all()
    dispatch({
      type: EMAIL.RULES.UPDATE_LIST,
      payload: emails
    })
  }
}

export function saveEmailRule(email: ImEmail): TODOAsyncThunkAction {
  return async dispatch => {
    const isNewRecord = email.get('status', undefined) === 'new'
    const response = isNewRecord
      ? await EmailRules.create(email)
      : await EmailRules.update(email)

    dispatch({
      type: EMAIL.RULES.SAVE.SUCCESS,
      email: response.get('email'),
      new_record: isNewRecord
    })
  }
}

export function newEmailRule(): TODOThunkAction {
  return dispatch => dispatch({ type: EMAIL.RULES.CREATE })
}

export function updateEmailAddress(
  emailId: EmailId,
  value: string
): TODOThunkAction {
  return dispatch =>
    dispatch({
      type: EMAIL.RULES.UPDATE_ADDRESS,
      id: emailId,
      email_address: value
    })
}

export function deleteEmailRule(email: ImEmail): TODOAsyncThunkAction {
  return async dispatch => {
    if (email.get('status', undefined) !== 'new') {
      await EmailRules.delete(email)
    }

    dispatch({
      type: EMAIL.RULES.DELETE.SUCCESS,
      id: email.get('id', undefined)
    })
  }
}

export function addForwardingAddress(emailId: EmailId): TODOThunkAction {
  return dispatch =>
    dispatch({
      type: EMAIL.FORWARD.ADD,
      id: emailId
    })
}

export function deleteForwardingAddress(
  emailId: EmailId,
  forwardingIndex: TODO
): TODOThunkAction {
  return dispatch =>
    dispatch({
      type: EMAIL.FORWARD.REMOVE,
      id: emailId,
      forwardingIndex: forwardingIndex
    })
}

export function updateForwardingAddress(
  emailId: EmailId,
  forwardingIndex: TODO,
  address: string
): TODOThunkAction {
  return dispatch =>
    dispatch({
      type: EMAIL.FORWARD.UPDATE,
      id: emailId,
      forwardingIndex: forwardingIndex,
      forwardingAddress: address
    })
}
