import { AssetClient, ImAssetsById } from '../../client/AssetClient'
import { ASSETS } from '../constants/actionTypes'
import { fromJS } from 'immutable'
import { Dispatch } from 'redux'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { ThunkAction } from 'redux-thunk'
import { TODOAction, TODOAsyncThunkAction } from 'src/js/typeMigrationShims'

interface SearchAssetProps {
  labels: string[]
}
export function searchAssets({
  labels
}: SearchAssetProps): ThunkAction<
  Promise<ImAssetsById | null>,
  RootReducer,
  null,
  TODOAction
> {
  return async (dispatch: Dispatch, getState) => {
    const state = getState()
    const client = new AssetClient(state.client)
    const assets = await client.search({ labels })
    dispatch({
      type: ASSETS.UPDATE_LIST,
      payload: assets
    })
    return assets
  }
}

interface DeleteAssetProps {
  id: number
}
export function deleteAsset({ id }: DeleteAssetProps): TODOAsyncThunkAction {
  return async (dispatch: Dispatch, getState) => {
    const state = getState()
    const client = new AssetClient(state.client)
    await client.deleteById(id) // throws exception on failure
    dispatch({
      type: ASSETS.DELETE_MULTIPLE,
      payload: fromJS([{ id }])
    })
  }
}
