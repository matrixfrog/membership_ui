import { fromJS, Map } from 'immutable'

import { membershipApi } from '../../services/membership'
import { HTTP_GET, logError, HttpMethod } from '../../util/util'
import { TODO } from 'src/js/typeMigrationShims'

export const FETCH_DATA_REQUEST = 'FETCH_DATA_REQUEST'
export const FETCH_DATA_SUCCESS = 'FETCH_DATA_SUCCESS'
export const FETCH_DATA_ERROR = 'FETCH_DATA_ERROR'
export const SET_DATA = 'SET_DATA'

interface FetchDataProps {
  apiService(method, route, params): Promise<any>
  method: HttpMethod
  route: string
  paramsOverride: boolean
  store: string
  keyPath: string[]
  responseParser(response: TODO): TODO
}

/**
 * Generic fetch data action that can be used to fetch data from an API &
 * dispatch generic actions handled by the fetchDataHandler higher-order reducer
 * @deprecated
 */
export function fetchData({
  apiService = membershipApi,
  method = HTTP_GET,
  route = '/',
  paramsOverride = false,
  store = '',
  keyPath = [],
  responseParser = response => fromJS(response)
}: Partial<FetchDataProps>) {
  return async (dispatch, getState) => {
    dispatch(fetchDataRequest(store, keyPath))
    try {
      let params
      if (paramsOverride !== false) {
        params = paramsOverride
      } else {
        params = getState()
          [store].getIn(keyPath.concat('params'), Map())
          .toJS()
      }
      const response = await apiService(method, route, params)
      const data = responseParser(response)
      dispatch(fetchDataSuccess(store, keyPath, data))
    } catch (err) {
      dispatch(fetchDataError(store, keyPath, err))
      logError(`Error fetching ${route}`, err)
    }
  }
}

// Action indicates we are fetching data from API
function fetchDataRequest(store, keyPath) {
  return {
    type: FETCH_DATA_REQUEST,
    store,
    keyPath
  }
}

// Action indicates we have retrieved data from API
function fetchDataSuccess(store, keyPath, data) {
  return {
    type: FETCH_DATA_SUCCESS,
    store,
    keyPath,
    data
  }
}

// Action logs error when fetching data from API
function fetchDataError(store, keyPath, err) {
  return {
    type: FETCH_DATA_ERROR,
    store,
    keyPath,
    err
  }
}

export function setData(store, keyPath, value) {
  return {
    type: SET_DATA,
    store,
    keyPath,
    value
  }
}
