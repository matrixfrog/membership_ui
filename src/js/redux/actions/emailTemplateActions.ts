import * as types from './../constants/actionTypes'
import {
  EmailTemplates,
  ImEmailTemplate,
  ImEmailTemplateForm
} from '../../client/EmailTemplateClient'
import { List } from 'immutable'
import { fromJS } from 'immutable'
import {
  TODOAsyncThunkAction,
  TODOThunkAction
} from 'src/js/typeMigrationShims'

export function updateEmailTemplate(template) {
  return {
    type: types.UPDATE_EMAIL_TEMPLATE,
    payload: template
  }
}

export function fetchEmailTemplates(): TODOAsyncThunkAction {
  return async dispatch => {
    const templates = (await EmailTemplates.all()) ?? List()
    dispatch({
      type: types.FETCH_EMAIL_TEMPLATES_SUCCEEDED,
      payload: templates
    })
  }
}

export function fetchEmailTemplate(id: number): TODOAsyncThunkAction {
  return async dispatch => {
    const template = await EmailTemplates.template(id)
    dispatch({
      type: types.FETCH_EMAIL_TEMPLATE_SUCCEEDED,
      payload: fromJS(template)
    })
  }
}

export function saveEmailTemplate(
  template: ImEmailTemplateForm
): TODOThunkAction {
  return dispatch => {
    EmailTemplates.save(template)
      .then(rsp => {
        dispatch({
          type: types.SAVE_EMAIL_TEMPLATE_SUCCEEDED,
          payload: rsp
        })
      })
      .catch(err => {
        dispatch({
          type: types.SAVE_EMAIL_TEMPLATE_FAILED,
          payload: err
        })
      })
    dispatch({
      type: types.SAVE_EMAIL_TEMPLATE,
      payload: template
    })
  }
}
