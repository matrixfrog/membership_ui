import { logError } from '../../util/util'
import { Elections } from '../../client/ElectionClient'
import { ELECTIONS } from '../constants/actionTypes'
import { TODOThunkAction } from 'src/js/typeMigrationShims'

export function fetchElections(): TODOThunkAction {
  return dispatch => {
    Elections.getElections()
      .then(data => {
        dispatch({
          type: ELECTIONS.FETCH_ELECTIONS,
          payload: data
        })
      })
      .catch(err => {
        logError(err.toString(), err)
      })
  }
}

export function fetchElection(electionId: number): TODOThunkAction {
  return dispatch => {
    Elections.getElection(electionId)
      .then(data => {
        if (data != null) {
          dispatch({
            type: ELECTIONS.FETCH_ELECTION,
            payload: data.set('id', electionId)
          })
        } else {
          throw new Error(
            `Got empty election data for election ID ${electionId}`
          )
        }
      })
      .catch(err => {
        logError(err.toString(), err)
      })
  }
}
