/**
 * Attempt to focus an element with the specified ID. If the element isn't
 * available yet this function will retry until it hits the maxAttempts value.
 *
 * @param {Object} params
 * @param {String} params.id            HTML ID of element
 * @param {Number} params.delay         ms to wait before focusing
 * @param {Number} params.retryInterval ms to wait between each attempt to focus
 * @param {Number} params.maxAttempts
 */
export default function focusOnElement({
  id,
  delay = 125,
  retryInterval = 500,
  maxAttempts = 30
}) {
  function focus(attempts = 0) {
    if (attempts >= maxAttempts) {
      return
    }

    const el: HTMLElement | null = document.querySelector(`#${id}`)

    if (el != null) {
      el.focus()
    } else {
      setTimeout(() => focus(attempts + 1), retryInterval)
    }
  }

  setTimeout(() => focus(0), delay)
}
