export const stringOf = (a, defaultValue = '') =>
  typeof a === 'string' ? a : defaultValue

export const objectOf = (a, defaultValue = {}) =>
  typeof a === 'object' && a !== null ? a : defaultValue

export const arrayOf = (a, defaultValue = []) =>
  Array.isArray(a) ? a : defaultValue

export const numberOf = (a, defaultValue) =>
  typeof a === 'number' ? a : defaultValue

export const maybeNumberOf = a => (typeof a === 'number' ? a : null)
