import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { Store } from 'redux'
import { System } from 'react-notification-system'

interface ServiceLocator {
  store: Store<RootReducer> | null
  notificationSystem: React.RefObject<System> | null
}

export const serviceLocator: ServiceLocator = {
  notificationSystem: null,
  store: null
}
