import { fromJS, Map } from 'immutable'
import { mapValues } from 'lodash'

type ActionsFromSchema<T> = { [P in keyof T]: string }

/**
 * Creates an key space for action type names based on the given prefix and schema.
 *
 * ```
 *   actions("prefix", {
 *     example: "documentation describing the type of action that is being reduced"
 *   }) == Map({
 *     example: "prefix.example"
 *   })
 * ```
 *
 * @param prefix {string | [string]} the root of the object
 * @param schema {object} the json schema, any string values will be replaced with the full path to that key
 *                        starting with the prefix.
 * @param init {object} an initial object to mutate
 * @returns {object} object of fully pathed key names
 */
export function actions<T>(
  prefix: string | string[],
  schema: T,
  init: any = {}
): T {
  if (typeof schema !== 'object' || Object.keys(schema).length < 1) {
    throw new Error('actions must be given a non-empty object')
  }
  const prefixArr = Array.isArray(prefix) ? prefix : [prefix]
  return Object.keys(schema).reduce((result, key) => {
    const field = schema[key]
    const path = [...prefixArr, key]
    if (typeof field === 'string') {
      result[key] = path.join('.')
    } else {
      result[key] = actions(path, field)
    }
    return result
  }, init)
}

export function Docs(schema) {
  const docs = schema.__schema__ ? schema.__schema__ : fromJS(schema)
  return key => {
    console.log(`key=${key}`)
    return docs.getIn(key.split('.'))
  }
}

export function Actions<T extends {}>(
  schema: T
): { [P in keyof T]: T[keyof T] } & { __schema__: Map<string, any> } {
  const __schema__ = fromJS(schema) as Map<string, any>
  const contents = mapValues(schema, (root, key) => actions(key, root))
  return {
    __schema__,
    ...contents
  }
}
