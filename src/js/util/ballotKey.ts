/**
 * Only allow users to type in numbers towards a valid BallotKey
 */
export function sanitizeIntermediateBallotInput(
  ballotKey: string
): string | null {
  const trimmed = ballotKey.trim()
  const isPositiveNumber = /^[1-9]?[0-9]*$/.test(trimmed)
  if (isPositiveNumber) {
    return trimmed
  } else {
    return null
  }
}

/**
 * Fully validate and sanitize a BallotKey
 */
export function sanitizeBallotKey(ballotKey: string): string | null {
  const trimmed = ballotKey.trim()
  const isPositiveNumber = /^[1-9]?[0-9]*$/.test(trimmed)
  const isFiveDigits = trimmed.length === 5

  if (isPositiveNumber && isFiveDigits) {
    return trimmed
  } else {
    return null
  }
}
