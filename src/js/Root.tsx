import React, { Component } from 'react'
import { connect, Provider } from 'react-redux'
import { History } from 'history'
import { HistoryUnsubscribe } from 'react-router-redux'
import { hot } from 'react-hot-loader/root'
import { fetchMember } from './redux/actions/memberActions'
import { USE_AUTH } from './config'

import Routes from './Routes'
import { RootReducer } from './redux/reducers/rootReducer'
import { Dispatch, Store, bindActionCreators } from 'redux'
import { QueryClient, QueryClientProvider } from 'react-query'

interface RootOwnProps {
  store: Store<RootReducer>
  history: History<any> & HistoryUnsubscribe
}

type RootProps = RootStateProps & RootDispatchProps & RootOwnProps

const queryClient = new QueryClient()

class Root extends Component<RootProps> {
  constructor(props: RootProps) {
    super(props)
  }

  componentDidMount() {
    document.addEventListener('sessionStarted', this.props.fetchMember, false)
    if (!USE_AUTH || localStorage.id_token != null) {
      this.props.fetchMember()
    }
  }

  render() {
    const { store, history } = this.props
    return (
      <Provider store={store}>
        <QueryClientProvider client={queryClient}>
          <Routes history={history} />
        </QueryClientProvider>
      </Provider>
    )
  }
}

const mapStateToProps = (state: RootReducer) => state
const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators({ fetchMember }, dispatch)

type RootStateProps = RootReducer
type RootDispatchProps = ReturnType<typeof mapDispatchToProps>

let exported = connect<
  RootStateProps,
  RootDispatchProps,
  RootOwnProps,
  RootReducer
>(
  mapStateToProps,
  mapDispatchToProps
)(Root)

if (process.env.NODE_ENV !== 'production') {
  exported = hot(exported)
}

export default exported
