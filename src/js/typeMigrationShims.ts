import { AnyAction } from 'redux'
import { ThunkAction } from 'redux-thunk'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'

/**
 * Placeholder type. Don't assign things to `any`. Instead, assign them to this type.
 *
 * This way you can see where TODOs still linger.
 *
 * Remove when done, or to break compilation if things are still TODO
 */
export type TODO = any
export type TODOAction = AnyAction
export type TODOAsyncThunkAction<T = void> = ThunkAction<
  Promise<T>,
  RootReducer,
  null,
  TODOAction
>
export type TODOThunkAction = ThunkAction<void, RootReducer, null, TODOAction>
