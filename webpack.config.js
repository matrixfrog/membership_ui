const webpack = require('webpack')
const DashboardPlugin = require('webpack-dashboard/plugin')
const path = require('path')
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin')
const packageJson = require('./package.json')
const DotenvPlugin = require('dotenv-webpack')

const dotenvConfig = process.env.DOTENV_CONFIG
const dotenvOpts = dotenvConfig && { path: dotEnvConfig }

require('dotenv').config(dotenvOpts)

const DEV_SERVER_PORT = process.env.DEV_SERVER_PORT || 3000
const DEV_SERVER_HOST = process.env.DEV_SERVER_HOST || 'localhost'
const DEFAULT_BABEL_CONFIG = {
  presets: [
    [
      '@babel/preset-env',
      {
        targets: {
          browsers: 'last 2 versions'
        }
      }
    ],
    '@babel/preset-typescript',
    '@babel/preset-react'
  ],
  plugins: [
    [
      '@babel/plugin-proposal-decorators',
      {
        legacy: true
      }
    ],
    [
      '@babel/plugin-proposal-class-properties',
      {
        loose: true
      }
    ],
    'react-hot-loader/babel'
  ]
}

function renderBabelConfig() {
  const packageBabelConfig = packageJson.babel
  const options = {
    cacheDirectory: true,
    babelrc: false
  }

  if (packageBabelConfig != null) {
    const plugins = packageBabelConfig.plugins || options.plugins || []

    return {
      ...options,
      ...packageBabelConfig,
      plugins: [...plugins, 'react-hot-loader/babel']
    }
  } else {
    return {
      ...options,
      ...DEFAULT_BABEL_CONFIG
    }
  }
}

module.exports = {
  mode: 'development',
  devtool: '#cheap-module-source-map',
  entry: [
    `webpack-dev-server/client?http://localhost:${DEV_SERVER_PORT}`,
    './src/js/index.tsx'
  ],
  devServer: { host: DEV_SERVER_HOST, port: DEV_SERVER_PORT, hot: true },
  output: {
    path: path.join(__dirname, 'dist'),
    publicPath: '/',
    filename: './bundle.js',
    chunkFilename: './[name].bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.(scss|css)$/,
        use: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.(j|t)s[x]?$/,
        include: path.resolve(__dirname, 'src'),
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: renderBabelConfig()
        }
      },
      {
        test: /\.(png|jpg)$/,
        loader: 'url-loader?limit=8192'
      },
      {
        test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url-loader?limit=10000&mimetype=application/font-woff'
      },
      {
        test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url-loader?limit=10000&mimetype=application/octet-stream'
      },
      {
        test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'file-loader'
      },
      {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url-loader?limit=10000&mimetype=image/svg+xml'
      }
    ]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new DashboardPlugin(),
    new ForkTsCheckerWebpackPlugin({
      useTypescriptIncrementalApi: true,
      measureCompilationTime: true,
      silent: process.argv.includes('--json')
    }),
    new webpack.NamedModulesPlugin(),
    new DotenvPlugin({
      safe: true
    }),
    new webpack.EnvironmentPlugin({
      NODE_ENV: 'development'
    })
  ],
  resolve: {
    modules: ['node_modules'],
    alias: {
      'react-dom': '@hot-loader/react-dom'
    },
    extensions: ['.js', '.jsx', '.ts', '.tsx']
  }
}
