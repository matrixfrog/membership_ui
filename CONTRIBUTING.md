# Contributing

## Common issues

### Operating on typed Immutable.js collections

If you try to map/filter/iterate over a typed Immutable.js collection (list, map, etc.), you may run into an error like the following:

```ts
this.props.meetings.get('byId').filter(meeting => {
  if (!meeting) return false
  // Property 'get' does not exist on type 'string | number | true | AllowedMap | AllowedList | TypedMap<any>'.
  return meeting.get('start_time') !== null
})
```

To fix this, you can cast the argument to the predicate to a more specific type. In the example above, that would be `ImMeeting`:

```ts
import { ImMeeting } from 'src/js/client/MeetingClient'

this.props.meetings.get('byId').filter((meeting: ImMeeting) => {
  if (!meeting) return false
  // meeting.get(...) now typechecks successfully
  return meeting.get('start_time') !== null
})
```

To determine the appropriate type to cast to, check the type of the collection you're working with.
